<?php

// foreach (getallheaders() as $name => $value) {
//     echo "$name: $value\n";
// }
include("../secret.php");

function get_data($url) {
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	curl_setopt($ch,CURLOPT_FAILONERROR,true);
        $data = curl_exec($ch);
	 if(curl_error($ch))
	 {
	echo curl_error($ch);
         header("HTTP/1.1 401 Unathorized");
	 }
        curl_close($ch);
        return $data;
}
function get_data_with_header($url) {
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
}


$timeout = 900;

function post_data($url,$json) {
$ip=$_SERVER['REMOTE_ADDR'];
// $header[0]  = "Accept: text/xml,application/xml,application/xhtml+xml,"; 
// $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
$header[] = "Content-Type: application/json";
$header[] = "Content-Length: " . strlen($json);
$header[] = "Cache-Control: max-age=0"; 
$header[] = "Connection: keep-alive"; 
$header[] = "Keep-Alive: 300"; 
$header[] = "Custom: ".$ip;
//$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"; 
//$header[] = "Accept-Language: en-us,en;q=0.5"; 
//$header[] = "Pragma: "; // browsers = blank
$header[] = "X_FORWARDED_FOR: " . $ip;
$header[] = "REMOTE_ADDR: " . $ip;
//$header[] = "Host: example.com";
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); 
// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($json),'REMOTE_ADDR: $ip', 'HTTP_X_FORWARDED_FOR: $ip'));
// curl_setopt( $ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));
curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
curl_setopt($ch, CURLOPT_TIMEOUT, 90);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
curl_setopt($ch,CURLOPT_FAILONERROR,true);
$result = curl_exec($ch);
if(curl_error($ch))
{
	header("HTTP/1.1 401 Unathorized ".curl_error($ch));
    // echo 'error:' . curl_error($ch);
}
curl_close($ch);
return $result;
}

function put_data($url,$json) {
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($json)));
curl_setopt($ch, CURLOPT_TIMEOUT, 90);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);
$result = curl_exec($ch);
if(curl_error($ch))
{
        header("HTTP/1.1 401 Unathorized");
    // echo 'error:' . curl_error($ch);
}
curl_close($ch);
return $result;
}

function delete_data($url,$json) {
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($json)));
curl_setopt($ch, CURLOPT_TIMEOUT, 5);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
$result = curl_exec($ch);
if(curl_error($ch))
{
        header("HTTP/1.1 401 Unathorized");
    // echo 'error:' . curl_error($ch);
}
curl_close($ch);
return $result;
}


if (isset($_SESSION['LAST_ACTIVITY'] ) && (time() - $_SESSION['LAST_ACTIVITY'] > $timeout) ) {
    // if @POC API touched more than $timeout seconds ago, unset session data and destroy stored data
    unset($_SESSION['target_id']); 
}

$_SESSION['LAST_ACTIVITY'] = time(); // this becomes t=0

if (isset($_REQUEST["target_id"]  )){
    $_SESSION['target_id'] = $_REQUEST["target_id"];
} 

function checkcreds($token)
{

	$dbh1 = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=".$GLOBALS['m_database']." user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
        // connect to a database named "mary" on the host "sheep" with a username and password
        if (!$dbh1) {
                die('Could not connect: ' . mysql_error());
        }

	$authtoken="";
	$userid="";
        $query = sprintf("SELECT userid FROM people_oath WHERE authtoken='%s'",  pg_escape_string($token));
	// echo $query;
        $result = pg_query($query);
        while ($row = pg_fetch_assoc($result)) {
               $userid=$row['userid'];
        }

                if ($userid == "" ) // check if valid token exists
                {
			$return_code="FALSE";
                }
                else
                {
			$return_code="TRUE";
                        // $value = $token;
			// $account_num=substr($client_id,0,4);
                }
	pg_close($dbh1);

	$details['token']=$token;
	$details['userid']=$userid;
	return array ($return_code, $details);
}

function apilog($debug,$profile)
{
	// print_r($debug);
	// print_r($profile);
	$debugjson=json_encode($debug);

	$dbh1 = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=".$GLOBALS['m_database']." user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
        // connect to a database named "mary" on the host "sheep" with a username and password
        if (!$dbh1) {
                die('Could not connect: ' . mysql_error());
        }

	$query = sprintf("INSERT INTO log_api (event_time, token, scope, clientkey, parentkey, method, premethod, class_id, service_id, record_id, REQUEST_URI, HTTP_USER_AGENT, REMOTE_ADDR,PHPSESSID,rawdebug) VALUES (CURRENT_TIMESTAMP, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", pg_escape_string($profile['creds'][1]['token']),pg_escape_string($profile['creds'][1]['scope']),pg_escape_string($profile['creds'][1]['clientid']),pg_escape_string($profile['creds'][1]['accountnum']),pg_escape_string($debug['method']),pg_escape_string($debug['premethod']),pg_escape_string($debug['class_id']),pg_escape_string($debug['service_id']),pg_escape_string($debug['record_id']),pg_escape_string($debug['_SERVER']['REQUEST_URI']),pg_escape_string($debug['_SERVER']['HTTP_USER_AGENT']),pg_escape_string($debug['_SERVER']['REMOTE_ADDR']),pg_escape_string($debug['_COOKIE']['PHPSESSID']),pg_escape_string($debugjson));
	//       echo $query; 
	// $result = $mysqli->query($query);
	// $mysqli->close();
	$result = pg_query($query);
	pg_close($dbh1);
}

/* Postgres Log Table
CREATE TABLE IF NOT EXISTS log_api (
event_key bigserial NOT NULL,
  event_time timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  token varchar(100) NOT NULL,
  scope varchar(100) NOT NULL,
  clientkey varchar(100) NOT NULL,
  parentkey varchar(100) NOT NULL,
  method varchar(100) NOT NULL,
  premethod varchar(100) NOT NULL,
  class_id varchar(100) NOT NULL,
  service_id varchar(100) NOT NULL,
  record_id varchar(100) NOT NULL,
  REQUEST_URI varchar(250) NOT NULL,
  HTTP_USER_AGENT varchar(250) NOT NULL,
  REMOTE_ADDR varchar(100) NOT NULL,
  PHPSESSID varchar(100) NOT NULL,
  rawdebug varchar(10000) NOT NULL
)*/
?>
