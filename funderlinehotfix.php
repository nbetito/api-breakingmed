<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($method == "SELECT") {

        //db connections
        $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

        $csv_array   = array();
        $csv_headers = array("articleid", "jobnum", "releasedate", "hed", "dek", "funder_line");
        $csv_array[] = $csv_headers;

        $jobnum_q = "SELECT articleid, jobnum, releasedate, hed, dek from tbl_breakingmed_articles where jobnum like '%.%'";

        $jobnum_res = pg_query($atpoc_db, $jobnum_q);

        while ($jobnum = pg_fetch_assoc($jobnum_res)) {

            $funder_q = "SELECT * from jobfunders where jobnum = $1";

            $funder_res = pg_query_params($pik_db, $funder_q, array($jobnum["jobnum"]));

            $entry = $jobnum;

            $subcats = array();

            $funder = pg_fetch_assoc($funder_res);

            $entry["funder_line"] = str_replace("'", "&#39;", urldecode($funder["firstcolblurb"])) ;

            array_push($csv_array, $entry);

        }

        $filedate = date("Y-m-d");

        $csv_location = '/vol3/sandbox/sandbox-nb/';

        $output_csv_path_history = $csv_location . "BreakingMED_funder_line_hotfix" . $filedate . ".csv";
        $output_csv              = fopen($output_csv_path_history, "w") or die("Unable to open " . $output_csv_path_history);
        foreach ($csv_array as $fields) {
            fputcsv($output_csv, $fields);
        }

        $output["jobnums"] = $csv_array;

    } elseif ($method == "INSERT") {
        // print_r($POST);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        // header("HTTP/1.1 200 Created");
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        // header("HTTP/1.1 200 Created");
        $resJson = json_encode($output);
        echo $resJson;

    }

}
