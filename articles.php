<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle, $premethod, $requestjson)
{

/* Optional credentials (token) check
$creds=checkcreds($token);
if ($creds[0] <> "1")
{
$code=401;
$message="Credentials failure";
header("HTTP/1.1 ".$code." ".$message);
// echo json_response(401, 'Credentials Fail');
die();
}
 */

    $dbhost   = "169.54.235.132";
    $dbuser   = "postgres";
    $dbpasswd = "";
    
    // $dbdb="pik";
    $dbdb = "atpoc";
    
    // $dbtable="breakingmed_phn";
    $dbtable = "tbl_breakingmed_articles";
    
    // $dbhost=$GLOBALS['wawa_server'];
    // $dbdb=$GLOBALS['wawa_database'];
    // $dbuser=$GLOBALS['wawa_user'];
    // $dbpasswd=$GLOBALS['wawa_password'];

    $unique_key   = "articleid";
    $dir          = null;
    $filename_key = null;
    $filetype_key = null;
    $db_type      = "postgres";

// map new fields to old
    $db_map  = "";
    $db_join = "";

    // Make database connection
    switch ($db_type) {
        case "mysql":
            $mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbdb);
            // check connection
            if ($mysqli->connect_errno) {
                printf("Connect failed: %s\n", $mysqli->connect_error);
                exit();
            }
            break;
        case "postgres":
            $dbconn = pg_connect("host=" . $dbhost . " port=5432 dbname=" . $dbdb . " user=" . $dbuser . " password=" . $dbpasswd) or die("Could not connect");
            break;
    }

    if ($premethod == "GET") {
        $query = "SELECT * FROM " . $dbtable;
        // $querydate="2019-03-06";
        $querydate = date("Y-m-d", time() + 86400 + 15000);

        if ($record_id) {
            $query = "select * " . $db_map . " from " . $dbtable . $db_join . " where articleid='" . $record_id . "'";
        } else {
            $query = "select * " . $db_map . " from " . $dbtable . $db_join . " where releasedate <= '" . $querydate . "'";
        }
        // echo $query;
        switch ($db_type) {
            case "mysql":
                if ($result = $mysqli->query($query, MYSQLI_USE_RESULT)) {
                    while ($row = pg_fetch_assoc($result)) {
                        $index        = $row[$unique_key];
                        $data[$index] = $row;
                    }
                }
                $mysqli->close();
                break;
            case "postgres":
                if (!pg_connection_busy($dbconn)) {
                    pg_send_query($dbconn, $query);
                }
                if ($result = pg_get_result($dbconn)) {
                    while ($row = pg_fetch_assoc($result)) {
                        $index        = $row[$unique_key];
                        $data[$index] = $row;
                    }
                }
                pg_close($dbconn);
                break;
        }
        if ($record_id) {
            $output = $data[$record_id];
            if ($dir !== null) {
                $file   = base64_encode(file_get_contents($dir . $data[$record_id][$filename_key]));
                $output = array_merge($data[$record_id], array('raw_data' => $data[$record_id][$filetype_key] . $file));
            }
        } else {
            $output = $data;
        }
    } elseif ($premethod == "DELETE") {
        echo "method not supported";
    } elseif ($premethod == "PUT") {
        echo "method not supported";
    } elseif ($premethod == "POST") {
        echo "method not supported";
    }

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;
    }

// apilog($debug,$profile);

}

function fetch_all_assoc(&$result, $index_keys)
{

    // Args :    $result = mysqli result variable (passed as reference to allow a free() at the end
    //           $indexkeys = array of columns to index on
    // Returns : associative array indexed by the keys array

    $assoc = array(); // The array we're going to be returning

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $pointer = &$assoc; // Start the pointer off at the base of the array

        for ($i = 0; $i < count($index_keys); $i++) {

            $key_name = $index_keys[$i];
            if (!isset($row[$key_name])) {
                print "Error: Key $key_name is not present in the results output.\n";
                return (false);
            }

            $key_val = isset($row[$key_name]) ? $row[$key_name] : "";

            if (!isset($pointer[$key_val])) {

                $pointer[$key_val] = ""; // Start a new node
                $pointer           = &$pointer[$key_val]; // Move the pointer on to the new node
            } else {
                $pointer = &$pointer[$key_val]; // Already exists, move the pointer on to the new node
            }

        } // for $i

        // At this point, $pointer should be at the furthest point on the tree of keys
        // Now we can go through all the columns and place their values on the tree
        // For ease of use, include the index keys and their values at this point too

        foreach ($row as $key => $val) {
            $pointer[$key] = $val;
        }

    } // $row

    /* free result set */
    $result->close();

    return ($assoc);
}
