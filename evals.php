<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($record_id) {

        //db connections
        $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);

        // GPS article content from tbl_breakingmed_articles
        $articles_q     = "SELECT * from tbl_breakingmed_articles where articleid = " . $record_id;
        $articles_q_res = pg_query($atpoc_db, $articles_q);
        $articles       = pg_fetch_assoc($articles_q_res);
        $articleid      = $articles["articleid"];
        $jobnum         = $articles["jobnum"];

        // active pretests

        $activepretest_q     = "SELECT * FROM tbl_pretests where jobnum = '" . $jobnum . "' and isactive = 1";
        $activepretest_q_res = pg_query($pik_db, $activepretest_q);
        if (pg_num_rows($activepretest_q_res)) {
            $activepretest = array();
            while ($row = pg_fetch_assoc($activepretest_q_res)) {
                $activepretest[] = $row['pretestid'];
            }
        }

        // active posttest
        $activeposttest_q     = "SELECT * FROM tbl_posttests where jobnum = '" . $jobnum . "' and isactive = 1";
        $activeposttest_q_res = pg_query($pik_db, $activeposttest_q);
        if (pg_num_rows($activeposttest_q_res)) {
            $activeposttest = array();
            while ($row = pg_fetch_assoc($activeposttest_q_res)) {
                $activeposttest[] = $row['posttestid'];
            }
        }

        // inactive pretests

        $inactivepretest_q     = "SELECT * FROM tbl_pretests where jobnum = '" . $jobnum . "' and isactive = 0";
        $inactivepretest_q_res = pg_query($pik_db, $inactivepretest_q);
        if (pg_num_rows($inactivepretest_q_res)) {
            $inactivepretest = array();
            while ($row = pg_fetch_assoc($inactivepretest_q_res)) {
                $inactivepretest[] = $row['pretestid'];
            }
        }

        // inactive posttest
        $inactiveposttest_q     = "SELECT * FROM tbl_posttests where jobnum = '" . $jobnum . "' and isactive = 0";
        $inactiveposttest_q_res = pg_query($pik_db, $inactiveposttest_q);
        if (pg_num_rows($inactiveposttest_q_res)) {
            $inactiveposttest = array();
            while ($row = pg_fetch_assoc($inactiveposttest_q_res)) {
                $inactiveposttest[] = $row['posttestid'];
            }
        }

        $output["pretest"] = array(

            'active'   => $activepretest,
            'inactive' => $inactivepretest,

        );

        $output["posttest"] = array(

            'active'   => $activeposttest,
            'inactive' => $inactiveposttest,

        );

        // evals

        $eval_q     = "SELECT * FROM tbl_evals where jobnum = '" . $jobnum . "' and isactive = 1";
        $eval_q_res = pg_query($pik_db, $eval_q);
        if (pg_num_rows($eval_q_res)) {
            $eval = array();
            while ($row = pg_fetch_assoc($eval_q_res)) {
                $eval[] = $row['evalid'];
            }
        }

        // inactive evals

        $inactiveeval_q     = "SELECT * FROM tbl_evals where jobnum = '" . $jobnum . "' and isactive = 0";
        $inactiveeval_q_res = pg_query($pik_db, $inactiveeval_q);
        if (pg_num_rows($inactiveeval_q_res)) {
            $inactiveeval = array();
            while ($row = pg_fetch_assoc($inactiveeval_q_res)) {
                $inactiveeval[] = $row['evalid'];
            }
        }

        $output["evals"] = array(

            'active' => $eval,
            'inactive' => $inactiveeval

        );

    } else {

        $output["error"] = "please provide articleid";

    }

// debug stuff

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;

    }
}
