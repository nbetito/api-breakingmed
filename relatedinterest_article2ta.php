<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle, $premethod, $requestjson)
{

    // Make database connection
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);


    if ($premethod == "GET") {

        //
        // get categories by article
        //

        $articles_q     = "SELECT cattopicid from tbl_breakingmed_articles where articleid = ".$record_id ;
        $articles_res = pg_query($atpoc_db, $articles_q);
        $articles = pg_fetch_assoc($articles_res);
    
        $cattopicid = $articles["cattopicid"] ;

        // var_dump($cattopicid);

        // 
        // get verticals by categories
        // 

        $target_num_q    =  "SELECT DISTINCT(target_num) from tbl_breakingmed_categories_ww_target where categoryid in (".$cattopicid.") ;";
        $target_num_res = pg_query($atpoc_db, $target_num_q);

        $target_num = "";
        $cntt = 0;

        while ($row_jt = pg_fetch_assoc($target_num_res)){
            $cntt++;
            $target_num = $target_num . "'" . $row_jt["target_num"] . "'" ;
            if($cntt < pg_num_rows($target_num_res)) {$target_num = $target_num . ",";}
        }

        // var_dump($target_num);

        // 
        // get tas by verticals
        // 

        $ta_q    =  "SELECT DISTINCT(ta_num) from ww_target_ta where target_num in (".$target_num.") and archived = false ORDER by ta_num";
        $ta_res = pg_query($atpoc_db, $ta_q);

        $ta = "";
        $cntp = 0;

        while ($row_jp = pg_fetch_assoc($ta_res)){
            $cntp++;
            $ta = $ta . "'" . $row_jp["ta_num"] . "'" ;
            if($cntp < pg_num_rows($ta_res)) {$ta = $ta . ",";}
        }


        // var_dump($ta); 



        // 
        // get metadata
        // 


        // $cats = array();

        // if (!empty($cattopicid)){
        //     $categories = "SELECT categoryid, categorytitle, parentcategory FROM tbl_breakingmed_categories WHERE categoryid in (". $cattopicid .") ;" ;
        //     $categories_res = pg_query($atpoc_db,$categories);
        //     while ($metadata_cats = pg_fetch_assoc($categories_res)) {
        //                     $cats[] = $metadata_cats;
        //                 }            
        //     $output['categories'] = $cats;
        // } else {
        //     $output['error'] = "no jobnum to promote";
        // }
        

        // $verts = array();

        // if (!empty($target_num)){
        //     $verts_q = "SELECT target_num, target_name FROM ww_target_collection WHERE target_num in (". $target_num .") ;" ;
        //     $verts_res = pg_query($atpoc_db,$verts_q);
        //     while ($metadata_verts = pg_fetch_assoc($verts_res)) {
        //                     $verts[] = $metadata_verts;
        //                 }            
        //     $output['verts'] = $verts;
        // } else {
        //     $output['error'] = "no jobnum to promote";
        // }



        // $related_ta = array();

        // if (!empty($ta)){
        //     $therapeutic_area = "SELECT ta_num, ta_name, subdomain FROM ww_therapeutic_area WHERE ta_num in (". $ta .") ;" ;
        //     $therapeutic_area_res = pg_query($atpoc_db,$therapeutic_area);
        //     while ($metadata = pg_fetch_assoc($therapeutic_area_res)) {
        //                     $related_ta[] = $metadata;
        //                 }            
        //     $output['related_ta'] = $related_ta;
        // } else {
        //     $output['error'] = "no jobnum to promote";
        // }
        




    } elseif ($premethod == "DELETE") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "PUT") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "POST") {
        $output["error"] = "method not supported" ;
    }

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        // $resJson = json_encode($output);
        // echo $resJson;
        echo($ta);
    }

}
