<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle, $premethod, $requestjson)
{

    // Make database connection
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);

    // 
    // LIMIT of results
    // 
    if (isset($GET['limit'])){
        $limit = $GET['limit'];
    } else {
        $limit = 40;
    }

    if ($premethod == "GET") {

        //
        // get jobnum by bookid
        //

        $lmt_toc     = "SELECT jobnum FROM lmt_toc WHERE status = 'active' and toc_type = 'subchapter' and bookid in (".$record_id.")";
        $lmt_toc_res = pg_query($atpoc_db, $lmt_toc);

        $job_list = "";
        $cntj     = 0;

        while ($row_j = pg_fetch_assoc($lmt_toc_res)) {
            $cntj++;
            $job_list = $job_list . "'" . $row_j["jobnum"] . "'";
            if ($cntj < pg_num_rows($lmt_toc_res)) {$job_list = $job_list . ",";}
        }

        


        // 
        // get active CME only
        // 

        $termdate    =  "SELECT termdate, jobnum from joblist where jobnum in (".$job_list.") and termdate > now()";
        $termdate_res = pg_query($pik_db, $termdate);

        $job_list_valid = "";
        $cntv = 0;

        while ($row_jv = pg_fetch_assoc($termdate_res)){
            $cntv++;
            $job_list_valid = $job_list_valid . "'" . $row_jv["jobnum"] . "'" ;
            if($cntv < pg_num_rows($termdate_res)) {$job_list_valid = $job_list_valid . ",";}
        }




        // 
        // get !DNP only
        // 

        $dnp    =  "SELECT jobnum from cme_goals_subjob where jobnum in (".$job_list_valid.") and dnp = false";
        $dnp_res = pg_query($atpoc_db, $dnp);

        $job_list_promo = "";
        $cntp = 0;

        while ($row_jp = pg_fetch_assoc($dnp_res)){
            $cntp++;
            $job_list_promo = $job_list_promo . "'" . $row_jp["jobnum"] . "'" ;
            if($cntp < pg_num_rows($dnp_res)) {$job_list_promo = $job_list_promo . ",";}
        }


        // die(var_dump($job_list,$job_list_valid,$job_list_promo));


        // 
        // get metadata
        // 
        // move dnr to where
        // 

        $related_interest = array();

        if (!empty(job_list_promo)){

            $pacing_q = "SELECT jobnum, activity_title, reldate 
                        , actual_participation::float / subjob_goal as percent_of_goal
                        FROM cme_goals_subjob
                        WHERE jobnum in (". $job_list_promo .")
                        ORDER by dnr, rescue desc, percent_of_goal
                        LIMIT ".$limit." ;" ;

            $pacing_q_res = pg_query($atpoc_db,$pacing_q);

            while ($metadata = pg_fetch_assoc($pacing_q_res)) {
                            $related_interest[] = $metadata;
                        }            

            $output['related_interest'] = $related_interest;
        } else {
            $output['error'] = "no jobnum to promote";
        }
        










    } elseif ($premethod == "DELETE") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "PUT") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "POST") {
        $output["error"] = "method not supported" ;
    }

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;
    }

}
