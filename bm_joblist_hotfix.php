<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($record_id) {

        //db connections
        $pik_db   = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=".$GLOBALS['m_database']." user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=".$GLOBALS['wawa_server']." port=5432 dbname=".$GLOBALS['wawa_database']." user=".$GLOBALS['wawa_user']." password=".$GLOBALS['wawa_password']);


        // GPS article content from tbl_breakingmed_articles
        $articles_q      = "SELECT * from tbl_breakingmed_articles where jobnum = '" . $record_id . "' ;";
        $articles_q_res  = pg_query($atpoc_db, $articles_q);
        $articles        = pg_fetch_assoc($articles_q_res);
        $articleid       = $articles["articleid"];
        $jobnum          = $articles["jobnum"];
        $activity_title  = $articles['hed'];
        $dek             = $articles['dek'];
        $author          = $articles['author'];
        $authorbyline    = $articles['authorbyline'];
        $reviewer        = $articles['reviewer'];
        $reviewerbyline  = $articles['reviewerbyline'];
        $is_moc          = $articles["is_moc"];
        $moc_credit_type = $articles["moc_credit_type"];
        $pharmacy_uan    = $articles["pharmacy_uan"];
        $abim_specialty  = $articles["abim_specialty"];

        $byline = "By " . $authorbyline . "\nReviewed by " . $reviewerbyline;

        // Get subchapter tocid by jn
        $lmt_toc_q     = "select * from lmt_toc where jobnum = '" . $record_id . "' and toc_type = 'subchapter' and status = 'active' order by tocid asc limit 1";
        $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
        $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);

        $tocid    = $lmt_toc["tocid"];
        $parentid = $lmt_toc["parentid"];

        // update lmt_toc
        $query                      = "update lmt_toc set toc_title = '" . trim($activity_title) . "', publish2ios = 1 where jobnum = '" . $jobnum . "' and toc_type = 'subchapter' and status = 'active' ; ";
        $output['lmt_toc']['query'] = $query;
        $result                     = pg_query($atpoc_db, $query);
        $status                     = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_toc']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_toc']['status'] = pg_last_error($atpoc_db);
        }

        // update lmt_toc parent
        $query                               = "update lmt_toc set publish2ios = 1 where tocid = '" . $parentid . "' and toc_type = 'chapter' and status = 'active' ; ";
        $output['lmt_toc']['parentid_query'] = $query;
        $result                              = pg_query($atpoc_db, $query);
        $status                              = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_toc']['parentid_status'] = "tocid = " . $parentid . " updated";
        } else {
            $output['lmt_toc']['parentid_status'] = pg_last_error($atpoc_db);
        }

        // update lmt_pages
        $query                        = "update lmt_pages set page_title = '" . trim($activity_title) . "' where tocid = '" . $tocid . "'; ";
        $output['lmt_pages']['query'] = $query;
        $result                       = pg_query($atpoc_db, $query);
        $status                       = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_pages']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_pages']['status'] = pg_last_error($atpoc_db);
        }

        // update joblist

        // ADD WEBSITE FORMAT
        // ADD LEARNING FORMAT
        // ADD PD?

        // rewrite string
        $activity_url  = 'https://secureapi.atpoc.com/sandboxapi-nb/zapier/jn?jn=' . $record_id;
        $activity_json = file_get_contents($activity_url);
        $activity      = json_decode($activity_json, true);
        $subdomain     = $activity["subdomain"];
        $parentjobnum  = substr($record_id, 0, 4);
        $rewritestring = '//suiteweb.atpointofcare.com/#library/' . $subdomain . '/' . $record_id . '/page/0';

        $reldate  = $activity['reldate'];
        $termdate = date_create($reldate);
        date_modify($termdate, '+1 year');
        date_modify($termdate, '-1 day');
        $termdate = date_format($termdate, 'Y-m-d');

        $query = "UPDATE joblist set rewritestring ='" . $rewritestring . "', parentjobnum = " . $parentjobnum . ", parentcurric = " . $parentjobnum . ", byline ='" . $byline . "',  format = 'BreakingMED', learningformat = 'Enduring material', webemailname = 'breakingmed', hascme = TRUE, hasce = TRUE, hascpe = TRUE, termdate = '" . $termdate . "', termdaten = '" . $termdate . "', is_moc = " . $is_moc . ", moc_credit_type = '" . $moc_credit_type . "',abim_specialty = '" . $abim_specialty . "', accmecred = 0.50, acpecred = 0.50, aacncred = 0.50, pharmacy_activity_type = 'Knowledge' where jobnum = '" . $record_id . "';";

        $output['joblist']['query'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['status'] = "jobnum = " . $record_id . " updated";
        } else {
            $output['joblist']['status'] = pg_last_error($pik_db);
        }

        // copy metadata from parent

        $parentjoblist_q     = "SELECT * from joblist where jobnum = '" . $parentjobnum . "' ;";
        $parentjoblist_q_res = pg_query($pik_db, $parentjoblist_q);
        $parentjoblist       = pg_fetch_assoc($parentjoblist_q_res);
        $clientid            = $parentjoblist["clientid"];
        $primaryinit         = $parentjoblist["primaryinit"];
        $actgoal             = $parentjoblist["actgoal"];
        $targaud             = $parentjoblist["targaud"];

        // copy metadata from meeting parent

        $meetingparentjobnum        = substr($record_id, 0, 6) . "1";
        $meetingparentjoblist_q     = "SELECT * from joblist where jobnum = '" . $meetingparentjobnum . "' ;";
        $meetingparentjoblist_q_res = pg_query($pik_db, $meetingparentjoblist_q);
        $meetingparentjoblist       = pg_fetch_assoc($meetingparentjoblist_q_res);
        $activitypageblurb          = htmlspecialchars($dek);
        $activitypageblurb          = str_replace("'", "&#39;", $activitypageblurb);



        $query = "UPDATE joblist set clientid ='" . $clientid . "', primaryinit = '" . $primaryinit . "', actgoal = '" . $actgoal . "', targaud = '" . $targaud . "', activitypageblurb = '" . $activitypageblurb . "' where jobnum = '" . $record_id . "';";

        $output['joblist']['copyfromparentquery'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['copyfromparentstatus'] = "jobnum = " . $record_id . " updated";
        } else {
            $output['joblist']['copyfromparentstatus'] = pg_last_error($pik_db);
        }

        // update jobfunders

        // get parent funderlines

        $funder_q     = "SELECT * from jobfunders where jobnum = '" . $parentjobnum . "' ;";
        $funder_q_res = pg_query($pik_db, $funder_q);
        $funder       = pg_fetch_assoc($funder_q_res);
        $funder_line  = $funder["firstcolblurb"];

        // check if funder line already exists

        $funder_check_q     = "SELECT * from jobfunders where jobnum = '" . $record_id . "' ;";
        $funder_check_q_res = pg_query($pik_db, $funder_check_q);
        $funder_check       = pg_fetch_assoc($funder_check_q_res);

        if (pg_num_rows($funder_check_q_res) == 0) {

            $insertFunderQuery_set    = "jobnum,firstcolblurb,secondcolblurb,thirdcolblurb";
            $insertFunderQuery_values = "'" . $record_id . "', '" . $funder_line . "', '" . $funder_line . "', '" . $funder_line . "'";
            $insertQuery              = "INSERT into jobfunders (" . $insertFunderQuery_set . ") VALUES (" . $insertFunderQuery_values . ")";

            $output['jobfunders']['query'] = $insertQuery;

            $result = pg_query($pik_db, $insertQuery);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobfunders']['status'] = "jobnum = " . $record_id . " updated";
            } else {
                $output['jobfunders']['status'] = pg_last_error($pik_db);
            }

        } else {
            $output['jobfunders']['query']  = $funder_check_q;
            $output['jobfunders']['status'] = "funder line already exists for jobnum = " . $record_id;
        }

        // check jobcredit

        $jobcredit_existing_q    = "select * from jobcredit where jobnum = '" . $record_id . "';";
        $jobcredit_existing      = pg_query($pik_db, $jobcredit_existing_q);
        $jobcredit_existing_rows = pg_num_rows($jobcredit_existing);

        if ($jobcredit_existing_rows == 0) {

            // jobcredit CME
            $cme_query = "INSERT into jobcredit (datemodified, createdby , mincredit, maxcredit, termdate, reldate, increment, isincremental, accreditor, accreditorid, type, typeorder, jobnum) VALUES (now(), 999, 0.25, 0.50, '" . $termdate . "', '" . $reldate . "', '0.25', TRUE, 'JA IPCE PIK CME', 75, 'CME', 1, '" . $record_id . "');";

            // jobcredit CE
            $ce_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, termdate, reldate, increment, isincremental,  accreditor, accreditorid, accredno, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', 0, FALSE, 'JA IPCE PIK CE', 76, '4008235', 'CE', 2, '" . $record_id . "');";

            // jobcredit CPE
            $cpe_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, bundlemax, termdate, reldate, increment, isincremental, accreditor, accreditorid, accredno, acpejobtype, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', NULL, FALSE, 'JA PIK CPE', 66, '".$pharmacy_uan."', 'Knowledge', 'CPE', 3, '" . $record_id . "');";

            // jobcredit AAPA
            $aapa_query = "INSERT into jobcredit (jobnum, type, accreditor, accreditorid, mincredit, maxcredit, increment, datecreated, createdby, typeorder, isincremental, isactive, reldate, termdate) VALUES ('" . $record_id . "', 'AAPA', 'JA IPCE PIK AAPA', 77, '0.5', '0.5', 0, now(), 200, 4, false, 1, '" . $reldate . "', '" . $termdate . "');";

            $output['jobcredit']['cme']['query'] = $cme_query;
            $output['jobcredit']['ce']['query']  = $ce_query;
            $output['jobcredit']['cpe']['query'] = $cpe_query;
            $output['jobcredit']['aapa']['query'] = $aapa_query;

            $result = pg_query($pik_db, $cme_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cme']['status'] = "type 'CME', jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $ce_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['ce']['status'] = "type 'CE', jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $cpe_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cpe']['status'] = "type 'CPE', jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $aapa_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['aapa']['status'] = "type 'AAPA', jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
            }

        } else {

            // jobcredit CME
            $cme_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, mincredit = 0.25 , maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', increment = '0.25', isincremental = TRUE, accreditor = 'JA IPCE PIK CME', accreditorid = 75 where type = 'CME' and jobnum = '" . $record_id . "';";

            // jobcredit CE
            $ce_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK CE', accreditorid = 76, accredno = '4008235' where type = 'CE' and jobnum = '" . $record_id . "';";
            
            // jobcredit CPE
            $cpe_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , bundlemax = 0.50, termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA PIK CPE', accreditorid = 66, acpejobtype = 'Knowledge', accredno = '".$pharmacy_uan."' where type = 'CPE' and jobnum = '" . $record_id . "';";

            // jobcredit AAPA

            $aapa_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK AAPA', accreditorid = 77 where type = 'AAPA' and jobnum = '" . $record_id . "';";

            $output['jobcredit']['cme']['query'] = $cme_query;
            $output['jobcredit']['ce']['query']  = $ce_query;
            $output['jobcredit']['cpe']['query'] = $cpe_query;
            $output['jobcredit']['aapa']['query'] = $aapa_query;

            $result = pg_query($pik_db, $cme_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cme']['status'] = "jobnum = " . $record_id . " updated";
            } else {
                $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $ce_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['ce']['status'] = "jobnum = " . $record_id . " updated";
            } else {
                $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $cpe_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cpe']['status'] = "jobnum = " . $record_id . " updated";
            } else {
                $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $aapa_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['aapa']['status'] = "jobnum = " . $record_id . " updated";
            } else {
                $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
            }

        }

        // check jobfaculty

        $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $record_id . "';";
        $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
        $jobfaculty_existing_rows  = pg_num_rows($jobfaculty_existing_q_res);

        if ($jobfaculty_existing_rows == 0) {

            if ($reviewer == "Vandana Abramson") {
                $facultyid       = 1065;
                $listaffiliation = "<li class=\"affil\">Associate Professor of Medicine</li><li class=\"affil\">Vanderbilt University Medical Center</li>";
                $photoid         = 1308;

            } elseif ($reviewer == "Kevin Rodowicz") {
                $facultyid       = 1072;
                $listaffiliation = "<li class=\"affil\">Assistant Professor</li><li class=\"affil\">St. Luke''s University / Temple University</li>";
                $photoid         = 1317;

            } elseif ($reviewer == "Anupama Brixey") {
                $facultyid       = 1074;
                $listaffiliation = "<li class=\"affil\">Clinical Fellow in Cardiothoracic Imaging</li><li class=\"affil\">Oregon Health and Science University</li>";
                $photoid         = 1324;

            }

            // jobfaculty CME
            $jobfaculty_query = "INSERT into jobfaculty (jobnum, facultyid, disclosure, role, rank, inits, photoid, listaffiliation) VALUES ('" . $record_id . "', " . $facultyid . ", 'has no relevant financial relationships to disclose.', 'Faculty Reviewer', 4, 'DW', " . $photoid . ",'" . $listaffiliation . "');";

            $output['jobfaculty']['query'] = $jobfaculty_query;

            $result = pg_query($pik_db, $jobfaculty_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobfaculty']['status'] = "type 'CME', jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobfaculty']['status'] = pg_last_error($pik_db);
            }

        } else {

            $output['jobfaculty']['query'] = $jobfaculty_existing_q;

            $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $record_id . "';";
            $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
            $jobfaculty                = pg_fetch_assoc($jobfaculty_existing_q_res);

            $facultyid = $jobfaculty['facultyid'];
            if ($status == 1) {
                $output['jobfaculty']['status'] = "jobnum = " . $record_id . ", facultyid = " . $facultyid;
            } else {
                $output['jobfaculty']['status'] = pg_last_error($pik_db);
            }

        }

        // check jobwriters

        $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $record_id . "';";
        $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
        $jobwriters_existing_rows  = pg_num_rows($jobwriters_existing_q_res);

        $authorbyline_explode = explode(",", $authorbyline);
        $actual_author        = trim($authorbyline_explode[0]);

        if ($jobwriters_existing_rows == 0) {

            if ($actual_author == "Alison Palkhivala") {
                $writerid = 564;
            } elseif ($actual_author == "Candace Hoffmann") {
                $writerid = 557;
            } elseif ($actual_author == "Edward Susman") {
                $writerid = 559;
            } elseif ($actual_author == "Katherine Wandersee") {
                $writerid = 384;
            } elseif ($actual_author == "Michael Bassett") {
                $writerid = 552;
            } elseif ($actual_author == "Michael Smith") {
                $writerid = 548;
            } elseif ($actual_author == "Paul Smyth, MD") {
                $writerid = 562;
            } elseif ($actual_author == "Paul Smyth") {
                $writerid = 562;
            } elseif ($actual_author == "Peggy Peck") {
                $writerid = 554;
            } elseif ($actual_author == "Salynn Boyles") {
                $writerid = 563;
            } elseif ($actual_author == "Samuel Kailes") {
                $writerid = 558;
            } elseif ($actual_author == "Scott Baltic") {
                $writerid = 560;
            } elseif ($actual_author == "Shalmali Pal") {
                $writerid = 561;
            } elseif ($actual_author == "E.C. Meszaros") {
                $writerid = 565;
            } elseif ($actual_author == "Pam Harrison") {
                $writerid = 566;
            } elseif ($actual_author == "Liz Meszaros") {
                $writerid = 567;
            }

            // jobwriters CME
            $jobwriters_query = "INSERT into jobwriters (jobnum, writerid, disclosure) VALUES ('" . $record_id . "', " . $writerid . ", ' has no relevant financial relationships to disclose.');";

            $output['jobwriters']['query'] = $jobwriters_query;

            $result = pg_query($pik_db, $jobwriters_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobwriters']['status'] = $actual_author . " (".$writerid.") jobnum = " . $record_id . " INSERTED";
            } else {
                $output['jobwriters']['status'] = pg_last_error($pik_db);
            }

        } else {

            $output['jobwriters']['query'] = $jobwriters_existing_q;

            $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $record_id . "';";
            $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
            $jobwriters                = pg_fetch_assoc($jobwriters_existing_q_res);

            $writerid = $jobwriters['writerid'];
            if ($status == 1) {
                $output['jobwriters']['status'] = "jobnum = " . $record_id . ", ".$actual_author ." writerid = " . $writerid;
            } else {
                $output['jobwriters']['status'] = pg_last_error($pik_db);
            }

        }

        // job_initiatives
        // select * from job_initiatives where jobnum = '2410'

    } else {

        // $output["error"] = "please provide articleid";
        $output = "please provide jobnum after the slash";

    }

// debug stuff

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;
        // echo $output;

    }
}
