<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

//db connections
    $pik_db   = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=pik user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

//GET
    if ($method == "SELECT") {
        //print_r($GET);

        if (isset($GET['articleid'])) {

            $articleid = $GET['articleid'];

            if (!$atpoc_db) {
                die('Could not connect to the database' . pg_last_Error($atpoc_db));
            }

            $content_url  = 'https://secureapi.atpoc.com/api-contentstream/latest/articles/' . $articleid;
            $content_json = file_get_contents($content_url);
            $content      = json_decode($content_json, true);

            $hed            = trim($content["hed"]);
            $dek            = trim($content["dek"]);
            $body           = $content["body"];
            $authorbyline   = $content["authorbyline"];
            $reviewerbyline = $content["reviewerbyline"];
            $jobnum         = $content["jobnum"];

            $hed  = str_replace("&#8217;", "&#39;", $hed);
            $dek  = str_replace("&#8217;", "&#39;", $dek);
            $body = str_replace("&#8217;", "&#39;", $body);
            $body = str_replace("<p><strong>", "<h3>", $body);
            $body = str_replace("</strong></p>", "</h3>\r\n\r\n", $body);

            $lmt_toc_q     = "select t2.subdomain from lmt_toc t1 join ww_therapeutic_area t2 on t1.bookid = t2.ta_num where t1.jobnum = '" . $jobnum . "' limit 1; ";
            $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
            $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);

            $subdomain         = $lmt_toc["subdomain"];
            $fair_balance_stmt = '<div style="margin-bottom: var(--baseline)"><a href="https://suiteweb.atpointofcare.com/#library/' . $subdomain . '">For more BreakingMED news coverage from this collection, <em>click here</em></a></div>';

            echo "<!-- page 1 col a -->\r\n\r\n\r\n\r\n<h1>" . $hed . "</h1>\r\n<h2>" . $dek . "</h2>\r\n\r\n<div class=\"byline\" style=\"margin-bottom: var(--baseline)\">\r\nBy " . $authorbyline . "<br />\r\nReviewed by " . $reviewerbyline . "</div>\r\n\r\n\r\n" . $fair_balance_stmt . "\r\n\r\n<!--CME/CE INFORMATION --><div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter('" . $jobnum . "');</script>";

            echo "\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- NEW: page 1 col b | set pretest = **NO** -->\r\n\r\n\r\n\r\n<div id=\"pretestFrame\"><script>pretestGetter('" . $jobnum . "','##UserToken##')</script></div>";

            echo "\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!-- page 2 col a -->\r\n\r\n\r\n\r\n";

            $takeaways = $content["takeaways"];
            $takeaways = str_replace("<ol>", "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Take Away:</div><p>", $takeaways);
            $takeaways = str_replace("<li><p>", "<span class=\"li\">", $takeaways);
            $takeaways = str_replace("</p></li>", "</span>", $takeaways);
            $takeaways = str_replace("</ol>", "</p></div></div>\r\n\r\n\r\n\r\n", $takeaways);

            echo $takeaways;

            $body = str_replace("</p>\n<ul>", "", $body);
            $body = str_replace("<ul>", "", $body);
            $body = str_replace("</ul></p>", "</p>", $body);
            $body = str_replace("<li>", "<span class=\"li\">", $body);
            $body = str_replace("</li>", "</span>", $body);

            $body = str_replace("</p>", "</p>\r\n\r\n", $body);
            $body = str_replace("&#8217;", "&#39;", $body);
            $body = str_replace("&#946; ", "&beta;&nbsp;", $body);
            $body = str_replace("<ul>", "<ul class=\"list\">", $body);

            $p_count      = substr_count($body, '<p>');
            $p_count_each = ($p_count / 4);

            //
            // COUNT PARAGRAPHS, 4 columns
            //

            if ($GET['autolayout'] == '1') {
                $body_exploded = explode("</p>", $body);

                $body_fixed = array();

                $delimiter = "\r\n\r\n\r\n\r\n\r\n\r\n<!-- suggested column break -->\r\n\r\n\r\n";
                $d_count   = 0;

                foreach ($body_exploded as $p) {
                    if (!empty($p) and (strpos($p, "cdn.atpoc.com/cdn/activity/breakingmed") !== false)) {
                        array_push($body_fixed, "<!-- " . $p . "</p> -->\r\n\r\n");
                    } else if (!empty($p) and (strpos($p, "<p>") !== false)) {
                        array_push($body_fixed, $p . "</p>");
                        $d_count++;
                    }
                    if ($d_count >= $p_count_each) {
                        array_push($body_fixed, $delimiter);
                        $d_count = 0;
                    }

                }

                $body_imploded = implode($body_fixed);
                $body          = $body_imploded;
            }

            //
            // COUNT CHARACTERS, 4 columns
            //

            if ($GET['autolayout'] == '2') {

                $body_len      = strlen(strip_tags($body));
                $body_len_each = ($body_len / 4);

                $body_exploded = explode("</p>", $body);

                $body_fixed = array();

                // count characters

                $delimiter = "\r\n\r\n\r\n\r\n\r\n\r\n<!-- suggested column break -->\r\n\r\n\r\n";
                $col_len   = 0;

                foreach ($body_exploded as $p) {
                    if (!empty($p) and (strpos($p, "cdn.atpoc.com/cdn/activity/breakingmed") !== false)) {
                        array_push($body_fixed, "<!-- " . $p . "</p> -->\r\n\r\n");
                    } else if (!empty($p) and (strpos($p, "<p>") !== false)) {
                        array_push($body_fixed, $p . "</p>");

                        $p_len = strlen(strip_tags($p));

                        $col_len = $col_len + $p_len;

                    }

                    if ($col_len >= $body_len_each) {
                        array_push($body_fixed, $delimiter);
                        $col_len = 0;
                    }

                }

                $body_imploded = implode($body_fixed);
                $body          = $body_imploded;
            }

            //
            // COUNT CHARACTERS, n columns
            //

            if ($GET['autolayout'] == '3') {

                $body2parse = $body;
                $columns    = ($GET["columns"] > 0) ? $GET["columns"] : 4;
                $c          = $columns;

                for ($i = 1; $i <= $columns; $i++) {

                    $body_len      = strlen(strip_tags($body2parse));
                    $body_len_each = ($body_len / $c);
                    $body_exploded = explode("</p>", $body2parse);
                    $col_len       = 0;

                    $body_fixed = array();

                    foreach ($body_exploded as &$p) {
                        if ($col_len <= $body_len_each) {

                            if (!empty($p) and (strpos($p, "cdn.atpoc.com/cdn/activity/breakingmed") !== false)) {
                                $p = '';
                            } else if (!empty($p) and (strpos($p, "<p>") !== false)) {
                                array_push($body_fixed, $p . "</p>");
                                $p_len = strlen(strip_tags($p));
                                $col_len += $p_len;
                                $p = '';
                            }
                        }
                    }

                    $paginated[] = implode($body_fixed);

                    $body2parse = implode("</p>", $body_exploded);

                    $c--;
                }

                $delimiter = "\r\n\r\n\r\n\r\n\r\n\r\n<!-- suggested column break -->\r\n\r\n\r\n";

                $body = implode($delimiter, $paginated);

            }

            echo "\r\n\r\n" . '<div style="text-align: center;"><img src="https://cdn.atpoc.com/cdn/activity/breakingmed/image/' . $articleid . '.jpeg" width="400px" /></div>' . "\r\n\r\n\r\n<div>&nbsp;</div>\r\n" . $fair_balance_stmt . "\r\n\r\n\r\n<!-- page 2 col b for first 4-5 tags, then 4-5 tags per col until end || " . $p_count . " <p> tags counted, approx " . $p_count_each . " per column -->\r\n\r\n\r\n";

            echo $body;

            echo "\r\n\r\n\r\n<!-- final page col b -->\r\n" . "\r\n\r\n<div>&nbsp;</div>" . $fair_balance_stmt;

            $sourcedisclosures = $content["sourcedisclosures"];
            $sourcedisclosures = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $sourcedisclosures);
            $sourcedisclosures = str_replace("</p>", "</div>", $sourcedisclosures);
            echo "\r\n\r\n<h4>Disclosure:</h4>" . $sourcedisclosures;

            $sources = $content["sources"];
            $sources = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $sources);
            $sources = str_replace("</p>", "</div>", $sources);
            echo "\r\n\r\n<h4>Sources:</h4>" . $sources . "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";

        } else {
            header("HTTP/1.1 400 Bad Request");
            $output['Error'] = "Please supply jn=";
        }
    } elseif ($method == "INSERT") {
        // print_r($POST);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

}
