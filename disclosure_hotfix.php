<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($record_id) {

		//db connections
        $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);

		// GPS article content from tbl_breakingmed_articles
        $articles_q     = "SELECT * from tbl_breakingmed_articles where articleid = " . $record_id;
        $articles_q_res = pg_query($atpoc_db, $articles_q);
        $articles       = pg_fetch_assoc($articles_q_res);
        $articleid      = $articles["articleid"];
        $jobnum         = $articles["jobnum"];

// BASIC METADATA
        
        $output["articleid"]       = $articles["articleid"];
        $output["jobnum"]          = $articles["jobnum"];
        $output["articletype"]     = $articles["articletype"];
        $output["releasedate"]     = $articles["releasedate"];
        $output["updated"]         = $articles["updated"];
        $output["newsenginstatus"] = $articles["newsenginstatus"];

        // claim credit

        $claim_credit_url           = "https://api.atpoc.com/beta/poc-test-module/?jn=" . $articles["jobnum"] . "&poc_tkn=##UserToken##";
        $output["claim_credit_url"] = $claim_credit_url;

// STORY

        $output["story"] = array(

            'hed'               => $articles["hed"],
            'dek'               => $articles["dek"],
            'authorbyline'      => $articles["authorbyline"],
            'reviewerbyline'    => $articles["reviewerbyline"],
            'takeaways'         => $articles["takeaways"],
            'body'              => $articles["body"],
            'sources'           => $articles["sources"],
            'sourcedisclosures' => $articles["sourcedisclosures"],

        );

// CME INFO

        // learning objectives from tbl_breakingmed_learnobjs

        $learnobjs_q     = "SELECT * FROM tbl_breakingmed_learnobjs WHERE articleid = $1 and isactive = 1 order by sortorder";
        $learnobjs_q_res = pg_query_params($atpoc_db, $learnobjs_q, array($record_id));
        if (pg_num_rows($learnobjs_q_res)) {
            $learnobjs = array();
            while ($row = pg_fetch_assoc($learnobjs_q_res)) {
                $learnobjs[] = $row['lo'];
            }
        }

        // jobnum stuff

        $funder_q     = 'SELECT firstcolblurb from jobfunders where jobnum = \'' . $jobnum . '\'';
        $funder_q_res = pg_query($pik_db, $funder_q);

        if (pg_num_rows($funder_q_res)) {
            while ($pik_row = pg_fetch_assoc($funder_q_res)) {
                $funder_line_raw = $pik_row['firstcolblurb'];
                $funder_line     = urldecode($funder_line_raw);
            }
        }

        $joblist_q     = 'SELECT actgoal,targaud from joblist where jobnum = \'' . $jobnum . '\'';
        $joblist_q_res = pg_query($pik_db, $joblist_q);

        if (pg_num_rows($joblist_q_res)) {
            while ($pik_row = pg_fetch_assoc($joblist_q_res)) {
                $actgoal = $pik_row['actgoal'];
                $targaud = $pik_row['targaud'];
            }
        }

        $output["cme_info"] = array(

            'releasedate'        => $articles["releasedate"],
            'funder_line'        => $funder_line,
            'cme_credit'         => $articles["cme_credit"],
            'ce_credit'          => $articles["ce_credit"],
            'target_audience'    => $targaud,
            'activity_goal'      => $actgoal,
            'authorbyline'       => $articles["authorbyline"],
            'reviewerbyline'     => $articles["reviewerbyline"],
            'learnobjs'          => $learnobjs,
            'cmedisclosures'     => $articles["cmedisclosures"],
            'cme_statement'      => $articles["cme_statement"],
            'nursingcestatement' => $articles["nursingcestatement"],

        );

            // $query = "update tbl_breakingmed_articles set cmedisclosures = '<p>There is no fee for this activity.</p><p><strong>To Receive Credit</strong><ol><li>Read the article carefully.</li><li>Complete/submit the posttest and evaluation.</li><li>BreakingMED, a service of @Point of Care, LLC will record your participation for each article in your CME/CE Tracker.</li></ol></p><p><strong>Program Overview</strong></p><p>Among the many issues clinicians face today, staying current with advances in medicine is becoming a major challenge. MEDBrief&#174; articles will allow clinicians to stay up-to-date and assimilate new information into their daily practice. The content of these MEDBrief&#174; articles has been validated through an independent expert peer review process.</p><p><strong>Disclosures</strong></p><p>".$articles["reviewerbyline"].", and Bernadette Marie Maker, MSN, NP-C, APRN-C, Nurse Planner, have disclosed that they have no relevant financial relationships or conflicts of interest with commercial interests related directly or indirectly to this educational activity. The staff of Projects In Knowledge&#174;, Inc. including the staff of BreakingMED, a service of @Point of Care, LLC, have no relevant financial relationships or conflicts of interest with commercial interests related directly or indirectly to this educational activity.</p>' where articleid = ".$articleid." ;" ;
            $query = "update tbl_breakingmed_articles set cmedisclosures = '<p>There is no fee for this activity.</p><p><strong>To Receive Credit</strong><ol><li>Read the article carefully.</li><li>Complete/submit the posttest and evaluation.</li><li>BreakingMED<sup>&trade;</sup>, a service of @Point of Care, LLC will record your participation for each article in your CME/CE Tracker.</li></ol></p><p><strong>Program Overview</strong></p><p>Among the many issues clinicians face today, staying current with advances in medicine is becoming a major challenge. MEDBrief<sup>&reg;</sup> articles will allow clinicians to stay up-to-date and assimilate new information into their daily practice. The content of these MEDBrief<sup>&reg;</sup> articles has been validated through an independent expert peer review process.</p><p><strong>Disclosures</strong></p><p>The Disclosure Policy of Projects In Knowledge<sup>&reg;</sup> requires that all those who have control of the content of CE activities comply with the ACCME&#39;s <em>Standards for Commercial Support</em> regarding disclosure of relevant financial relationships. Faculty, planners, editorial scientific team, reviewers, and medical writers are required to disclose any financial relationship they or their spouse/partner have with any ACCME-defined commercial interest* relevant to the content of this activity. Any discussions of unlabeled/unapproved uses of drugs or devices will also be disclosed during the presentations.</p><p><strong>".$articles["reviewerbyline"]."</strong> has no relevant financial relationships to disclose.</p><p><strong>".$articles["authorbyline"]."</strong> has no relevant financial relationships to disclose.</p><p><strong>Bernadette Marie Makar, MSN, NP-C, APRN-C</strong> (nurse planner) has no relevant financial relationships to disclose.</p><p>Projects In Knowledge<sup>&reg;</sup>, @Point of Care<sup>&trade;</sup>, and BreakingMED<sup>&trade;</sup> staff members have no relevant financial relationships to disclose.</p><p>Conflicts of interest are thoroughly vetted by the Executive Committee of Projects In Knowledge<sup>&reg;</sup>. All conflicts are resolved prior to the beginning of the activity by the Trust In Knowledge peer review process.</p><p>The opinions expressed in this activity are those of the faculty and do not necessarily reflect those of Projects In Knowledge<sup>&reg;</sup>.</p><p>*Commercial Interest is any entity producing, marketing, re-selling, or distributing health care goods or services consumed by, or used, on patients.</p>' where articleid = ".$articleid." ;" ;

            $result = pg_query($atpoc_db, $query);

            $status = pg_result_status($result);

            if ($status == 1) {
                $output = 'articleid = '.$articleid.' updated';
            } else {
                $output = pg_last_error($atpoc_db);
            }


    } else {

        // $output["error"] = "please provide articleid";
        $output = "please provide articleid";

    }

// debug stuff

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        // $resJson = json_encode($output);
        // echo $resJson;
        echo $output;
            


    }
}
