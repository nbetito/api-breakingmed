<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    //db connections
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

    if (!empty($POST)) {

        $output    = array();
        $subdomain = 'news';

        $acpe_uan           = $POST["acpe_uan"];
        $abim_specialty     = $POST["abim_specialty"];
        $articleid          = $POST["articleid"];
        $jobnum             = $POST["jobnum"];
        $releasedate        = $POST["releasedate"];
        $funder_line        = $POST["shortdescrip"];
        $include_in_updates = (strlen($POST["include_in_updates"]) > 0) ? 1 : 0;

        $output["gps"]["acpe_uan"]       = $acpe_uan;
        $output["gps"]["abim_specialty"] = $abim_specialty;
        $output["gps"]["articleid"]      = $articleid;
        $output["gps"]["jobnum"]         = $jobnum;
        $output["gps"]["releasedate"]    = $releasedate;

        //
        // INGESTION HOTFIX FOR EXISTING RECORD (update only)
        //

        $articles_q     = "SELECT * FROM tbl_breakingmed_articles where articleid = " . $articleid;
        $articles_q_res = pg_query($atpoc_db, $articles_q);
        $articles       = pg_fetch_assoc($articles_q_res);

        $output["articles"]["pharmacy_uan"]   = $articles['pharmacy_uan'];
        $output["articles"]["abim_specialty"] = $articles['abim_specialty'];
        $output["articles"]["articleid"]      = $articles['articleid'];
        $output["articles"]["jobnum"]         = $articles['jobnum'];
        $output["articles"]["releasedate"]    = $articles['releasedate'];

        // add UAN and abim_specialty
        $tbl_breakingmed_articles = "update tbl_breakingmed_articles set include_in_updates = " . $include_in_updates . ", pharmacy_uan = '" . $acpe_uan . "', abim_specialty = '" . $abim_specialty . "' , funder_line = '" . $funder_line . "' where articleid = " . $articleid . ";";

        $output['tbl_breakingmed_articles']['query'] = $tbl_breakingmed_articles;

        $result = pg_query($atpoc_db, $tbl_breakingmed_articles);
        $status = pg_result_status($result);

        if ($status == 1) {
            $output['tbl_breakingmed_articles']['status'] = "articleid = " . $articleid . " updated";
        } else {
            $output['tbl_breakingmed_articles']['status'] = pg_last_error($atpoc_db);
        }

        $result         = array_diff($output["gps"], $output["articles"]);
        $output["diff"] = $result;
        if (empty($result)) {$output["result"] = "no difference";} else {
            $output["result"] = "something diff";
        }

        //
        //
        //
        //
        // OLDER HOTFIXES
        //
        //
        //
        //

        // GPS article content from tbl_breakingmed_articles
        $articles_q        = "SELECT * from tbl_breakingmed_articles where jobnum = '" . $jobnum . "' ;";
        $articles_q_res    = pg_query($atpoc_db, $articles_q);
        $articles          = pg_fetch_assoc($articles_q_res);
        $articleid         = $articles["articleid"];
        $jobnum            = $articles["jobnum"];
        $activity_title    = trim($articles['hed']);
        $hed               = trim($articles['hed']);
        $dek               = trim($articles['dek']);
        $author            = $articles['author'];
        $authorbyline      = $articles['authorbyline'];
        $reviewer          = $articles['reviewer'];
        $reviewerbyline    = $articles['reviewerbyline'];
        $is_moc            = $articles["is_moc"];
        $moc_credit_type   = $articles["moc_credit_type"];
        $pharmacy_uan      = $articles["pharmacy_uan"];
        $abim_specialty    = $articles["abim_specialty"];
        $body              = $articles["body"];
        $takeaways         = $articles["takeaways"];
        $sourcedisclosures = $articles["sourcedisclosures"];
        $sources           = $articles["sources"];

        $activity_title = str_replace("&#8217;", "&#39;", $activity_title);
        $dek            = str_replace("&#8217;", "&#39;", $dek);
        $body           = str_replace("&#8217;", "&#39;", $body);

        $byline = "By " . $authorbyline . "\nReviewed by " . $reviewerbyline;

        //
        // LMT tables
        //

        // Get subchapter tocid by jn
        $lmt_toc_q     = "select t1.*, t2.subdomain from lmt_toc t1 join ww_therapeutic_area t2 on t1.bookid = t2.ta_num where t1.jobnum = '" . $jobnum . "' and t1.toc_type = 'subchapter' and t1.status = 'active' order by tocid asc limit 1";
        $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
        $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);

        $tocid             = $lmt_toc["tocid"];
        $parentid          = $lmt_toc["parentid"];
        $bookid            = $lmt_toc["bookid"];
        $subdomain         = $lmt_toc["subdomain"];
        $fair_balance_stmt = '<div style="margin-bottom: var(--baseline)"><a href="https://suiteweb.atpointofcare.com/#library/' . $subdomain . '">For more BreakingMED news coverage from this collection, <em>click here</em></a></div>';

        if ($lmt_toc == false) {
            echo json_encode($output);
            die("subchapter tocid doesn't exist");
        }

        // update lmt_toc
        $query = "update lmt_toc set toc_title = '" . trim($activity_title) . "', publish2ios = 1 where jobnum = '" . $jobnum . "' and toc_type = 'subchapter' and status = 'active' ; ";

        $output['lmt_toc']['query'] = $query;
        $result                     = pg_query($atpoc_db, $query);
        $status                     = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_toc']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_toc']['status'] = "ERROR! NO TOCID // " . $query . " // " . pg_last_error($atpoc_db);
            echo json_encode($output);
            die();
        }

        // update lmt_toc parent
        $query = "update lmt_toc set publish2ios = 1 where tocid = '" . $parentid . "' and toc_type = 'chapter' and status = 'active' ; ";

        $output['lmt_toc']['parentid_query'] = $query;
        $result                              = pg_query($atpoc_db, $query);
        $status                              = pg_result_status($result);

        if ($status == 1) {
            $output['lmt_toc']['parentid_status'] = "tocid = " . $parentid . " updated";
        } else {
            $output['lmt_toc']['parentid_status'] = pg_last_error($atpoc_db);
            echo json_encode($output);
            die();
        }

        // update lmt_pages
        $query = "update lmt_pages set page_title = '" . trim($activity_title) . "' where tocid = '" . $tocid . "'; ";

        $output['lmt_pages']['query'] = $query;
        $result                       = pg_query($atpoc_db, $query);
        $status                       = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_pages']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_pages']['status'] = pg_last_error($atpoc_db);
            echo json_encode($output);
            die();
        }

        // count lmt_pages
        $query = "select * from lmt_toc where parentid = " . $tocid;

        $output['page_count']['query'] = $query;
        $result                        = pg_query($atpoc_db, $query);
        if (pg_num_rows($result) < 1) {
            $output['page_count']['status'] = "pages do not exist";
            // insert into lmt_toc and lmt_pages

            // autolayout

            $p1ca = "<h1>" . $activity_title . "</h1><h2>" . $dek . "</h2><div class=\"byline\" style=\"margin-bottom: var(--baseline)\">By " . $authorbyline . "<br />Reviewed by " . $reviewerbyline . "</div>" . $fair_balance_stmt . "<!--CME/CE INFORMATION --><div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter(''" . $jobnum . "'');</script>";

            $p1cb = "<div id=\"pretestFrame\"><script>pretestGetter(''" . $jobnum . "'',''##UserToken##'')</script></div>";

            $output['page_count']['insert']['query'] = "WITH lmt_toc_1 as (INSERT INTO lmt_toc (bookid,toc_title,parentid,sortorder,toc_type,status) VALUES (" . $bookid . ",'" . $dek . "'," . $tocid . ",1,'page','active') returning *, pg_sleep(.25) ), lmt_pages_1 as (INSERT INTO lmt_pages (page_title,page_type,dlu,updated_by,status,tocid,layout,cola,colb) VALUES ('" . $dek . "','page',now(),1,'active',(select tocid from lmt_toc_1),'default','" . $p1ca . "','" . $p1cb . "') returning * ), lmt_toc_2 as (INSERT INTO lmt_toc (bookid,toc_title,parentid,sortorder,toc_type,status) VALUES (" . $bookid . ",''," . $tocid . ",2,'page','active') returning *, pg_sleep(.25) ), lmt_pages_2 as (INSERT INTO lmt_pages (page_title,page_type,dlu,updated_by,status,tocid,layout) VALUES ('','page',now(),1,'active',(select tocid from lmt_toc_2),'default') returning * ), lmt_toc_3 as (INSERT INTO lmt_toc (bookid,toc_title,parentid,sortorder,toc_type,status) VALUES (" . $bookid . ",''," . $tocid . ",3,'page','active') returning *, pg_sleep(.25) ), lmt_pages_3 as (INSERT INTO lmt_pages (page_title,page_type,dlu,updated_by,status,tocid,layout) VALUES ('','page',now(),1,'active',(select tocid from lmt_toc_3),'default') returning * ), lmt_toc_4 as (INSERT INTO lmt_toc (bookid,toc_title,parentid,sortorder,toc_type,status) VALUES (" . $bookid . ",''," . $tocid . ",4,'page','active') returning *, pg_sleep(.25) ), lmt_pages_4 as (INSERT INTO lmt_pages (page_title,page_type,dlu,updated_by,status,tocid,layout) VALUES ('','page',now(),1,'active',(select tocid from lmt_toc_4),'default') returning * ) SELECT * FROM lmt_pages_1 UNION SELECT * FROM lmt_pages_2 UNION SELECT * FROM lmt_pages_3 UNION SELECT * FROM lmt_pages_4 order by tocid";

            $query  = $output['page_count']['insert']['query'];
            $result = pg_query($atpoc_db, $query);

            $status = pg_result_status($result);
            if ($status == 2) {
                $output['page_count']['insert']['status'] = "lmt_toc and lmt_pages inserted";
            } else {
                $output['page_count']['insert']['status'] = $status;
                $output['page_count']['insert']['error']  = pg_last_error($atpoc_db);
                echo json_encode($output);
                die();
            }

        } else {
            $output['page_count']['status'] = pg_num_rows($result) . " pages already exist";
        }

        //
        // PIK tables
        //

        // update joblist

        // ADD PD?

        // rewrite string
        $activity_url  = 'https://secureapi.atpoc.com/sandboxapi-nb/zapier/jn?jn=' . $jobnum;
        $activity_json = file_get_contents($activity_url);
        $activity      = json_decode($activity_json, true);
        $subdomain     = $activity["subdomain"];
        $parentjobnum  = substr($jobnum, 0, 4);
        $rewritestring = '//suiteweb.atpointofcare.com/#library/' . $subdomain . '/' . $jobnum . '/page/0';

        $reldate  = $POST["releasedate"];
        $termdate = date_create($reldate);
        date_modify($termdate, '+1 year');
        date_modify($termdate, '-1 day');
        $termdate = date_format($termdate, 'Y-m-d');

        $query                      = "UPDATE joblist set rewritestring ='" . $rewritestring . "', title = '" . $activity_title . "', subtitle = '" . $dek . "', parentjobnum = " . $parentjobnum . ", parentcurric = " . $parentjobnum . ", byline ='" . $byline . "', termdate = '" . $termdate . "', termdaten = '" . $termdate . "', is_moc = " . $is_moc . ", moc_credit_type = '" . $moc_credit_type . "', abim_specialty = '" . $abim_specialty . "', shortname = '" . $POST["slug"] . "', description = '" . $articles["promo_blurb"] . "', homepagetext = '" . $articles["promo_blurb"] . "', format = 'BreakingMED', learningformat = 'Enduring material', webemailname = 'breakingmed', hascme = TRUE , hasce = TRUE , hascpe = TRUE , accmecred = 0.50 , acpecred = 0.50 , aacncred = 0.50 , pharmacy_activity_type = 'Knowledge', pd = '71|1', upn = '" . $acpe_uan . "' where jobnum = '" . $jobnum . "';";
        $output['joblist']['query'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['status'] = "jobnum = " . $jobnum . " updated";
        } else {
            $output['joblist']['status'] = pg_last_error($pik_db);
        }

        // copy metadata from parent

        $parentjoblist_q     = "SELECT * from joblist where jobnum = '" . $parentjobnum . "' ;";
        $parentjoblist_q_res = pg_query($pik_db, $parentjoblist_q);
        $parentjoblist       = pg_fetch_assoc($parentjoblist_q_res);
        $clientid            = $parentjoblist["clientid"];
        $primaryinit         = $parentjoblist["primaryinit"];
        $actgoal             = $parentjoblist["actgoal"];
        $targaud             = $parentjoblist["targaud"];

        // copy metadata from meeting parent

        $meetingparentjobnum        = substr($jobnum, 0, 6) . "1";
        $meetingparentjoblist_q     = "SELECT * from joblist where jobnum = '" . $meetingparentjobnum . "' ;";
        $meetingparentjoblist_q_res = pg_query($pik_db, $meetingparentjoblist_q);
        $meetingparentjoblist       = pg_fetch_assoc($meetingparentjoblist_q_res);
        $activitypageblurb          = htmlspecialchars($dek);
        $activitypageblurb          = str_replace("'", "&#39;", $activitypageblurb);

        $query = "UPDATE joblist set clientid ='" . $clientid . "', primaryinit = '" . $primaryinit . "', actgoal = '" . $actgoal . "', targaud = '" . $targaud . "', activitypageblurb = '" . $activitypageblurb . "' where jobnum = '" . $jobnum . "';";

        $output['joblist']['copyfromparentquery'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['copyfromparentstatus'] = "jobnum = " . $jobnum . " updated";
        } else {
            $output['joblist']['copyfromparentstatus'] = pg_last_error($pik_db);
        }

        // update jobfunders

        // // get parent funderlines

        // $funder_q     = "SELECT * from jobfunders where jobnum = '" . $parentjobnum . "' ;";
        // $funder_q_res = pg_query($pik_db, $funder_q);
        // $funder       = pg_fetch_assoc($funder_q_res);
        // $funder_line  = $funder["firstcolblurb"];

        $funder_line = urlencode($funder_line);

        // check if funder line already exists

        $funder_check_q     = "SELECT * from jobfunders where jobnum = '" . $jobnum . "' ;";
        $funder_check_q_res = pg_query($pik_db, $funder_check_q);
        $funder_check       = pg_fetch_assoc($funder_check_q_res);

        if (pg_num_rows($funder_check_q_res) == 0) {

            $insertFunderQuery_set    = "jobnum,firstcolblurb,secondcolblurb,thirdcolblurb";
            $insertFunderQuery_values = "'" . $jobnum . "', '" . $funder_line . "', '" . $funder_line . "', '" . $funder_line . "'";
            $insertQuery              = "INSERT into jobfunders (" . $insertFunderQuery_set . ") VALUES (" . $insertFunderQuery_values . ")";

            $output['jobfunders']['query'] = $insertQuery;

            $result = pg_query($pik_db, $insertQuery);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobfunders']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobfunders']['status'] = pg_last_error($pik_db);
            }

        } else {

            $updateQuery = "update jobfunders set firstcolblurb = '" . $funder_line . "', secondcolblurb = '" . $funder_line . "',thirdcolblurb = '" . $funder_line . "' where jobnum = '" . $jobnum . "' ;";

            $output['jobfunders']['query'] = $updateQuery;

            $result = pg_query($pik_db, $updateQuery);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobfunders']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobfunders']['status'] = pg_last_error($pik_db);
            }

        }

        // check jobcredit

        $jobcredit_existing_q    = "select * from jobcredit where jobnum = '" . $jobnum . "';";
        $jobcredit_existing      = pg_query($pik_db, $jobcredit_existing_q);
        $jobcredit_existing_rows = pg_num_rows($jobcredit_existing);

        if ($jobcredit_existing_rows == 0) {

            // jobcredit CME
            $cme_query = "INSERT into jobcredit (datemodified, createdby , mincredit, maxcredit, termdate, reldate, increment, isincremental, accreditor, accreditorid, type, typeorder, jobnum) VALUES (now(), 999, 0.25, 0.50, '" . $termdate . "', '" . $reldate . "', '0.25', TRUE, 'JA IPCE PIK CME', 75, 'CME', 1, '" . $jobnum . "');";

            // jobcredit CE
            $ce_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, termdate, reldate, increment, isincremental,  accreditor, accreditorid, accredno, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', 0, FALSE, 'JA IPCE PIK CE', 76, '4008235', 'CE', 2, '" . $jobnum . "');";

            // jobcredit CPE
            $cpe_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, bundlemax, termdate, reldate, increment, isincremental, accreditor, accreditorid, accredno, acpejobtype, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', NULL, FALSE, 'JA PIK CPE', 66, '" . $pharmacy_uan . "', 'Knowledge', 'CPE', 3, '" . $jobnum . "');";

            // jobcredit AAPA
            $aapa_query = "INSERT into jobcredit (jobnum, type, accreditor, accreditorid, mincredit, maxcredit, increment, datecreated, createdby, typeorder, isincremental, isactive, reldate, termdate) VALUES ('" . $jobnum . "', 'AAPA', 'JA IPCE PIK AAPA', 77, '0.5', '0.5', 0, now(), 200, 4, false, 1, '" . $reldate . "', '" . $termdate . "');";

            $output['jobcredit']['cme']['query']  = $cme_query;
            $output['jobcredit']['ce']['query']   = $ce_query;
            $output['jobcredit']['cpe']['query']  = $cpe_query;
            $output['jobcredit']['aapa']['query'] = $aapa_query;

            $result = pg_query($pik_db, $cme_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cme']['status'] = "type 'CME', jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $ce_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['ce']['status'] = "type 'CE', jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $cpe_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cpe']['status'] = "type 'CPE', jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $aapa_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['aapa']['status'] = "type 'AAPA', jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
            }

        } else {

            // jobcredit CME
            $cme_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, mincredit = 0.25 , maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', increment = '0.25', isincremental = TRUE, accreditor = 'JA IPCE PIK CME', accreditorid = 75 where type = 'CME' and jobnum = '" . $jobnum . "';";

            // jobcredit CE
            $ce_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK CE', accreditorid = 76, accredno = '4008235' where type = 'CE' and jobnum = '" . $jobnum . "';";

            // jobcredit CPE
            $cpe_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , bundlemax = 0.50, termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA PIK CPE', accreditorid = 66, acpejobtype = 'Knowledge', accredno = '" . $pharmacy_uan . "' where type = 'CPE' and jobnum = '" . $jobnum . "';";

            // jobcredit AAPA

            $aapa_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK AAPA', accreditorid = 77 where type = 'AAPA' and jobnum = '" . $jobnum . "';";

            $output['jobcredit']['cme']['query']  = $cme_query;
            $output['jobcredit']['ce']['query']   = $ce_query;
            $output['jobcredit']['cpe']['query']  = $cpe_query;
            $output['jobcredit']['aapa']['query'] = $aapa_query;

            $result = pg_query($pik_db, $cme_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cme']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $ce_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['ce']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $cpe_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['cpe']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
            }

            $result = pg_query($pik_db, $aapa_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobcredit']['aapa']['status'] = "jobnum = " . $jobnum . " updated";
            } else {
                $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
            }

        }

        // check jobfaculty

        $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $jobnum . "';";
        $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
        $jobfaculty_existing_rows  = pg_num_rows($jobfaculty_existing_q_res);

        if ($jobfaculty_existing_rows == 0) {

            if ($reviewer == "Vandana Abramson") {
                $facultyid       = 1065;
                $listaffiliation = "<li class=\"affil\">Associate Professor of Medicine</li><li class=\"affil\">Vanderbilt University Medical Center</li>";
                $photoid         = 1308;

            } elseif ($reviewer == "Kevin Rodowicz") {
                $facultyid       = 1072;
                $listaffiliation = "<li class=\"affil\">Assistant Professor</li><li class=\"affil\">St. Luke''s University / Temple University</li>";
                $photoid         = 1317;

            } elseif ($reviewer == "Anupama Brixey") {
                $facultyid       = 1074;
                $listaffiliation = "<li class=\"affil\">Clinical Fellow in Cardiothoracic Imaging</li><li class=\"affil\">Oregon Health and Science University</li>";
                $photoid         = 1324;

            }

            // jobfaculty CME
            $jobfaculty_query = "INSERT into jobfaculty (jobnum, facultyid, disclosure, role, rank, inits, photoid, listaffiliation) VALUES ('" . $jobnum . "', " . $facultyid . ", 'has no relevant financial relationships to disclose.', 'Faculty Reviewer', 4, 'DW', " . $photoid . ",'" . $listaffiliation . "');";

            $output['jobfaculty']['query'] = $jobfaculty_query;

            $result = pg_query($pik_db, $jobfaculty_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobfaculty']['status'] = $reviewer . " (" . $facultyid . ") jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobfaculty']['status'] = pg_last_error($pik_db);
            }

        } else {

            $output['jobfaculty']['query'] = $jobfaculty_existing_q;

            $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $jobnum . "';";
            $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
            $jobfaculty                = pg_fetch_assoc($jobfaculty_existing_q_res);

            $facultyid = $jobfaculty['facultyid'];
            if ($status == 1) {
                $output['jobfaculty']['status'] = "jobnum = " . $jobnum . ", " . $reviewer . " facultyid = " . $facultyid;
            } else {
                $output['jobfaculty']['status'] = pg_last_error($pik_db);
            }

        }

        // check jobwriters

        $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $jobnum . "';";
        $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
        $jobwriters_existing_rows  = pg_num_rows($jobwriters_existing_q_res);

        $authorbyline_explode = explode(",", $authorbyline);
        $actual_author        = trim($authorbyline_explode[0]);

        if ($jobwriters_existing_rows == 0) {

            if ($actual_author == "Alison Palkhivala") {
                $writerid = 564;
            } elseif ($actual_author == "Candace Hoffmann") {
                $writerid = 557;
            } elseif ($actual_author == "Edward Susman") {
                $writerid = 559;
            } elseif ($actual_author == "Katherine Wandersee") {
                $writerid = 384;
            } elseif ($actual_author == "Michael Bassett") {
                $writerid = 552;
            } elseif ($actual_author == "Michael Smith") {
                $writerid = 548;
            } elseif ($actual_author == "Paul Smyth, MD") {
                $writerid = 562;
            } elseif ($actual_author == "Paul Smyth") {
                $writerid = 562;
            } elseif ($actual_author == "Peggy Peck") {
                $writerid = 554;
            } elseif ($actual_author == "Salynn Boyles") {
                $writerid = 563;
            } elseif ($actual_author == "Samuel Kailes") {
                $writerid = 558;
            } elseif ($actual_author == "Scott Baltic") {
                $writerid = 560;
            } elseif ($actual_author == "Shalmali Pal") {
                $writerid = 561;
            } elseif ($actual_author == "E.C. Meszaros") {
                $writerid = 565;
            } elseif ($actual_author == "Pam Harrison") {
                $writerid = 566;
            } elseif ($actual_author == "Liz Meszaros") {
                $writerid = 567;
            }

            // jobwriters CME
            $jobwriters_query = "INSERT into jobwriters (jobnum, writerid, disclosure) VALUES ('" . $jobnum . "', " . $writerid . ", ' has no relevant financial relationships to disclose.');";

            $output['jobwriters']['query'] = $jobwriters_query;

            $result = pg_query($pik_db, $jobwriters_query);
            $status = pg_result_status($result);
            if ($status == 1) {
                $output['jobwriters']['status'] = $actual_author . " (" . $writerid . ") jobnum = " . $jobnum . " INSERTED";
            } else {
                $output['jobwriters']['status'] = pg_last_error($pik_db);
            }

        } else {

            $output['jobwriters']['query'] = $jobwriters_existing_q;

            $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $jobnum . "';";
            $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
            $jobwriters                = pg_fetch_assoc($jobwriters_existing_q_res);

            $writerid = $jobwriters['writerid'];
            if ($status == 1) {
                $output['jobwriters']['status'] = "jobnum = " . $jobnum . ", " . $actual_author . " writerid = " . $writerid;
            } else {
                $output['jobwriters']['status'] = pg_last_error($pik_db);
            }

        }

        // job_initiatives
        // select * from job_initiatives where jobnum = '2410'

        // tbl_pretestans
        // ADD "I do NOT know" as last pretest choice
        // find active pretest and for each question find last answer

        $actual_pre_q = "WITH actual_pretest as (SELECT tbl_pretests.jobnum, tbl_pretestqs.rank, tbl_pretests.pretestid, tbl_pretestqs.qid, max(tbl_pretestans.ansrank) as ansrank, tbl_pretestqs.qtext FROM tbl_pretests JOIN tbl_pretestqs on tbl_pretests.pretestid = tbl_pretestqs.pretestid JOIN tbl_pretestans on tbl_pretestqs.qid = tbl_pretestans.qid WHERE tbl_pretests.jobnum = $1 and tbl_pretestqs.qtypenum = 40 and tbl_pretests.isactive = 1 GROUP by tbl_pretests.jobnum, tbl_pretests.pretestid, tbl_pretestqs.qid, tbl_pretestqs.qtext), last_answer_pretest as (SELECT qid, ansid, anstext from tbl_pretestans WHERE (qid, ansrank) IN (SELECT qid, ansrank from actual_pretest)) SELECT actual_pretest.*, last_answer_pretest.ansid, last_answer_pretest.anstext from actual_pretest join last_answer_pretest on actual_pretest.qid = last_answer_pretest.qid";

        $actual_pre_res = pg_query_params($pik_db, $actual_pre_q, array($jobnum));

        while ($actual_pre = pg_fetch_assoc($actual_pre_res)) {
            $output['pretest']['meta'][] = $actual_pre;

            // check that anstext doesn't already = "I do NOT know"
            if (strpos($actual_pre["anstext"], "I do NOT know") === false) {
                
                // form insert statement
                $qid = $actual_pre["qid"];
                $ansrank = intval($actual_pre["ansrank"]) + 1;
                $insert_q = "INSERT INTO tbl_pretestans (qid,anstext,ansvalue,ansrank,iscorrect) VALUES ( $1, 'I do NOT know', NULL, $2, '0') RETURNING *;";
                
                // insert "I do NOT know"
                $insert_q_res = pg_query_params($pik_db, $insert_q, array($qid, $ansrank));
                $insert = pg_fetch_assoc($insert_q_res);
                $output['pretest'][$qid] = $insert;
            } 
        }


        
        

        // job_strategies dependency for eval q's

            // determine if multiple test records exist
                // SELECT * from tbl_pretests where jobnum = $1
                // SELECT * from tbl_posttests where jobnum = $1
                // SELECT * from tbl_evals where jobnum = $1

            // if multiple tests exist

                // find jobstrategies
                    // SELECT * FROM jobstrategies WHERE jobnum = $1 order by id

                // while results find dependent eval subqs, update with what's in jobstrategies
                    // $hotfix .= "UPDATE tbl_pretestsubqs set subqtext = (SELECT strattext from jobstrategies where id = $1), dlm = now() where externalid = $1 ;";
                    // $hotfix .= "UPDATE tbl_evalsubqs set subqtext = (SELECT strattext from jobstrategies where id = $1), dlm = now() where externalid = $1 ;";





        // AUTOLAYOUT i.e. update lmt_pages

        if ($GET["autolayout"] == 1 and !empty($POST)) {
            // $output["autolayout"] = 'yes';

            // Get subchapter tocid by jn
            $lmt_toc_q     = "select * from lmt_toc where parentid = '" . $tocid . "' and toc_type = 'page' and status = 'active' order by sortorder";
            $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);


            // get element_url by articleid
            $element_q     = "select * from tbl_breakingmed_elements t1 join tbl_breakingmed_element_items t2 on t1.element_id = t2.element_id where articleid = $1";
            $element_q_res = pg_query_params($atpoc_db, $element_q, array($articleid));
            $element = pg_fetch_assoc($element_q_res);
            $element_url = $element["element_url"];
            if (strlen($element_url)<10){
                $element_url = 'https://cdn.atpoc.com/cdn/activity/breakingmed/image/' . $articleid . '.jpeg';
            }



            if (pg_num_rows($lmt_toc_q_res) != 0) {

                $tocids = array();
                while ($lmt_toc = pg_fetch_assoc($lmt_toc_q_res)) {
                    $tocids[$lmt_toc["sortorder"]] = $lmt_toc["tocid"];
                }
                $output["autolayout"]["check"]   = "rows exist";
                $output["autolayout"]["lmt_toc"] = $tocids;
                $paginated                       = autolayout($articleid, $jobnum, $hed, $dek, $authorbyline, $reviewerbyline, $body, $takeaways, $subdomain, $sourcedisclosures, $sources, $element_url);
                // $output["autolayout"]["paginated"] = $paginated;

                $p1_cola = cleanup($paginated[0]);
                $p1_colb = cleanup($paginated[1]);
                $lmt_pages_update .= "update lmt_pages set cola = '" . $p1_cola . "', colb = '" . $p1_colb . "', dlu = now() where tocid = " . $tocids["1"] . ";\r\n\r\n\r\n\r\n";

                $p2_cola = cleanup($paginated[2]);
                $p2_colb = cleanup($paginated[3]);
                $lmt_pages_update .= "update lmt_pages set cola = '" . $p2_cola . "', colb = '" . $p2_colb . "', dlu = now() where tocid = " . $tocids["2"] . ";\r\n\r\n\r\n\r\n";

                $p3_cola = cleanup($paginated[4]);
                $p3_colb = cleanup($paginated[5]);
                $lmt_pages_update .= "update lmt_pages set cola = '" . $p3_cola . "', colb = '" . $p3_colb . "', dlu = now() where tocid = " . $tocids["3"] . ";\r\n\r\n\r\n\r\n";

                $p4_cola = cleanup($paginated[6]);
                $p4_colb = cleanup($paginated[7]);
                $lmt_pages_update .= "update lmt_pages set cola = '" . $p4_cola . "', colb = '" . $p4_colb . "', dlu = now() where tocid = " . $tocids["4"] . ";\r\n\r\n\r\n\r\n";

                $lmt_pages_update_single = "update lmt_pages set cola = (case when tocid = " . $tocids["1"] . " then '" . $p1_cola . "' when tocid = " . $tocids["2"] . " then '" . $p2_cola . "' when tocid = " . $tocids["3"] . " then '" . $p3_cola . "' when tocid = " . $tocids["4"] . " then '" . $p4_cola . "' end), colb = (case when tocid = " . $tocids["1"] . " then '" . $p1_colb . "' when tocid = " . $tocids["2"] . " then '" . $p2_colb . "' when tocid = " . $tocids["3"] . " then '" . $p3_colb . "' when tocid = " . $tocids["4"] . " then '" . $p4_colb . "' end), dlu = now() where tocid in (" . implode(",", $tocids) . ") ; ";

                $output["autolayout"]["lmt_pages_update"]["query"] = $lmt_pages_update_single;

                $query  = $output["autolayout"]["lmt_pages_update"]["query"];
                $result = pg_query($atpoc_db, $query);
                $status = pg_result_status($result);
                if ($status == 1) {
                    $output["autolayout"]["lmt_pages_update"]['status'] = "lmt_toc and lmt_pages inserted";
                    $output["autolayout"]["lmt_pages_update"]['feed']   = "https://secureapi.atpoc.com/sandboxapi-nb/api-webcms/blue_line_subfeed_pik/" . $jobnum . "?prod=1";
                } else {
                    $output["autolayout"]["lmt_pages_update"]['status'] = pg_last_error($atpoc_db);
                    echo json_encode($output);
                    die();
                }

            } else {
                $output["autolayout"] = "no rows";
                //  write output JSON using preset delimiters
                // option by target number pages?
                // option for
            }

        } else {
            $output["autolayout"] = 'no autolayout';
        }

    } else {
        $output["error"] = "must POST JSON";
    }

    echo json_encode($output);

}

function autolayout($articleid, $jobnum, $hed, $dek, $authorbyline, $reviewerbyline, $body, $takeaways, $subdomain, $sourcedisclosures, $sources, $element_url)
{

    $new_body = $body;

    $fair_balance_stmt = '<div style="margin-bottom: var(--baseline)"><a href="https://suiteweb.atpointofcare.com/#library/' . $subdomain . '">For more BreakingMED news coverage from this collection, <em>click here</em></a></div>';

    $takeaways = str_replace("<ol>", "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Take Away:</div><p>", $takeaways);
    $takeaways = str_replace("<li><p>", "<span class=\"li\">", $takeaways);
    $takeaways = str_replace("</p></li>", "</span>", $takeaways);
    $takeaways = str_replace("</ol>", "</p></div></div>\r\n\r\n\r\n\r\n", $takeaways);

    $paginated[] = "<h1>" . $hed . "</h1><h2>" . $dek . "</h2><div class=\"byline\" style=\"margin-bottom: var(--baseline)\">By " . $authorbyline . "<br />Reviewed by " . $reviewerbyline . "</div>" . $fair_balance_stmt . "<!--CME/CE INFORMATION --><div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter(''" . $jobnum . "'');</script>";

    $paginated[] = "<div id=\"pretestFrame\"><script>pretestGetter(''" . $jobnum . "'',''##UserToken##'')</script></div>";

    $paginated[] = $takeaways . "\r\n\r\n" . '<div style="text-align: center;"><img src="'.$element_url.'" width="400px" /></div>' . "\r\n\r\n\r\n<div>&nbsp;</div>\r\n" . $fair_balance_stmt;

    $new_body = str_replace("</p>\n<ul>", "", $new_body);
    $new_body = str_replace("<ul>", "", $new_body);
    $new_body = str_replace("</ul></p>", "</p>", $new_body);
    $new_body = str_replace("<li>", "<span class=\"li\">", $new_body);
    $new_body = str_replace("</li>", "</span>", $new_body);

    $new_body = str_replace("</p>", "</p>\r\n\r\n", $new_body);
    $new_body = str_replace("&#8217;", "&#39;", $new_body);
    $new_body = str_replace("&#946; ", "&beta;&nbsp;", $new_body);
    $new_body = str_replace("<ul>", "<ul class=\"list\">", $new_body);

    $p_count      = substr_count($new_body, '<p>');
    $p_count_each = ($p_count / 4);

    $body2parse = $new_body;
    $columns    = 4;
    $c          = $columns;

    for ($i = 1; $i <= $columns; $i++) {

        $body_len      = strlen(strip_tags($body2parse));
        $body_len_each = ($body_len / $c);
        $body_exploded = explode("</p>", $body2parse);
        $col_len       = 0;

        $body_fixed = array();

        foreach ($body_exploded as &$p) {
            if ($col_len <= $body_len_each) {

                if (!empty($p) and (strpos($p, "cdn.atpoc.com/cdn/activity/breakingmed") !== false)) {
                    $p = '';
                } else if (!empty($p) and (strpos($p, "<p>") !== false)) {
                    array_push($body_fixed, $p . "</p>");
                    $p_len = strlen(strip_tags($p));
                    $col_len += $p_len;
                    $p = '';
                }
            }
        }

        $paginated[] = trim(implode($body_fixed));

        $body2parse = implode("</p>", $body_exploded);

        $c--;
    }

    $new_sourcedisclosures = $sourcedisclosures;
    $new_sourcedisclosures = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $new_sourcedisclosures);
    $new_sourcedisclosures = str_replace("</p>", "</div>", $new_sourcedisclosures);
    $end_body              = "<h4>Disclosure:</h4>" . $new_sourcedisclosures;

    $new_sources = $sources;
    $new_sources = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $new_sources);
    $new_sources = str_replace("</p>", "</div>", $new_sources);
    $end_body .= "<h4>Sources:</h4>" . $new_sources;

    $paginated[] = $fair_balance_stmt . $end_body;

    return $paginated;

}

function cleanup($text)
{
    $new_text   = $text;
    $whitespace = array("\r", "\n", "\t");
    $new_text   = str_replace($whitespace, "", $new_text);
    return $new_text;
}
