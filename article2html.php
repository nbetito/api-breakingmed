<?php

function api($service_id,$method,$listmode,$record_id,$token,$GET,$POST,$PUT,$debug,$debugtoggle)
{


//db connections
$pik_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=pik user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
$atpoc_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=atpoc user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);

//GET
if ( $method == "SELECT")
{	//print_r($GET);


	if ($record_id) {
		
		$articleid = $record_id;

		if (!$atpoc_db) {
			die('Could not connect to the database' . pg_last_Error($atpoc_db));
		}


        $articles     = "SELECT * FROM tbl_breakingmed_articles WHERE articleid = $1";
        $articles_res = pg_query_params($atpoc_db, $articles, array($record_id));		
        $metadata = pg_fetch_array($articles_res);

		// $content_url = 'https://api.atpoc.com/feed-breakingmed/latest/articles/'. $articleid ;
		// $content_json = file_get_contents($content_url);
		// $content = json_decode($content_json, true);

		$hed = $metadata["hed"];
		$dek = $metadata["dek"];
		$authorbyline = $metadata["authorbyline"];
		$reviewerbyline = $metadata["reviewerbyline"];
		$jobnum = $metadata["jobnum"];
		$learning_objectives = $metadata["learning_objectives"];
		$cme_credit = explode(" ", $metadata["cme_credit"]);
		$credit = $cme_credit[0];
		$jobnum = $metadata["jobnum"];

		$jobnum = $metadata["jobnum"];
		$jobnum = $metadata["jobnum"];

		echo "<html><head></head>\r\n\r\n\r\n<h4>jobnum: ".$jobnum."</h4>\r\n<h5>articleid: ".$articleid."</h5>\r\n\r\n\r\n\r\n<h1>".$hed."</h1>\r\n<h2>".$dek."</h2>\r\n\r\n<p>\r\nBy ".$authorbyline."<br />\r\nReviewed by ".$reviewerbyline."\r\n</p>\r\n\r\n\r\n";


		echo "<h3>Credit Amounts</h3>\r\n\r\n<p>CME: ".$credit."<br>CE: ".$credit."<br>CPE: ".$credit."</p>\r\n\r\n\r\n";





        $promo_blurb = $metadata['promo_blurb'];

		echo "<h3>Promo Blurb</h3>\r\n".$promo_blurb."\r\n\r\n\r\n";



        $learnobjs     = "SELECT * FROM tbl_breakingmed_learnobjs WHERE isactive = 1 and articleid = $1 order by sortorder";
        $learnobjs_res = pg_query_params($atpoc_db, $learnobjs, array($record_id));

        echo "<h3>Learning Objectives</h3>\r\n\r\n<ul>\r\n";

        if ($learnobjs_res) {
            while ($array = pg_fetch_array($learnobjs_res)) {

                $lo         = $array['lo'];
				$lo = str_replace("p>", "li>", $lo);

                echo $lo . "\r\n" ;

            }
        } else {
            echo "\r\n\r\n\r\nerror!";
        }

        echo "</ul>\r\n\r\n\r\n\r\n<h3>Body</h3>\r\n\r\n<h4>Take Aways:</h4>\r\n";





		$takeaways = $metadata["takeaways"];
		$takeaways = str_replace("<p>", "", $takeaways);
		$takeaways = str_replace("</p>", "", $takeaways);
		$takeaways = str_replace("</li>", "</li>\r\n", $takeaways);
		$takeaways = str_replace("<ol>", "<ol>\r\n", $takeaways);

		echo $takeaways;

		$body = $metadata["body"];
		$body = str_replace("</p>", "</p>\r\n\r\n", $body);
		$body = str_replace("&#8217;", "&#39;", $body);
		$body = str_replace("&#946; ", "&beta;&nbsp;", $body);
		$body = str_replace("<ul>", "<ul class=\"list\">", $body);

		$p_count = substr_count($body, '<p>'); 
		$p_count_each = $p_count / 4 ;

		echo "\r\n\r\n\r\n";
		echo $body;


		$sourcedisclosures = $metadata["sourcedisclosures"];
		// $sourcedisclosures = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $sourcedisclosures);
		// $sourcedisclosures = str_replace("</p>", "</div>", $sourcedisclosures);
		echo "\r\n\r\n<h4>Disclosure:</h4>\r\n".$sourcedisclosures;

		$sources = $metadata["sources"];
		// $sources = str_replace('<p>', '<div style="margin-bottom: var(--baseline)">', $sources);
		// $sources = str_replace("</p>", "</div>", $sources);
		echo "\r\n\r\n<h4>Sources:</h4>\r\n".$sources."\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";




        $test     = "SELECT p.posttestid, q.qid, q.rank, q.qtext, q.ansexplanation
							--, r.ansrank, r.anstext, r.iscorrect
							FROM tbl_breakingmed_posttests p
							LEFT JOIN tbl_breakingmed_posttestqs q ON q.posttestid = p.posttestid AND q.isactive=1
							--LEFT JOIN tbl_breakingmed_posttestans r ON r.qid = q.qid 
							WHERE p.articleid = $1 and p.isactive = 1
							ORDER BY p.posttestid, q.qid, q.rank
							--, r.ansrank";
        $test_res = pg_query_params($atpoc_db, $test, array($record_id));

        echo "\r\n\r\n<h3>Pretest / Posttest</h3>\r\n\r\n<ol>\r\n";
        if ($test_res) {
            while ($questions = pg_fetch_array($test_res)) {

                $qtext         = $questions['qtext'];
                $ansexplanation         = $questions['ansexplanation'];
                $qid         = $questions['qid'];

                echo "\r\n<li>".$qtext."\r\n\t<ol style=\"list-style-type: lower-alpha;\">\r\n";
                
                    $answers_q     = "SELECT r.qid, r.ansrank, r.anstext, r.iscorrect
									FROM tbl_breakingmed_posttestans r 
									WHERE r.qid = $1
									ORDER BY r.ansrank";

			        $answers_res = pg_query_params($atpoc_db, $answers_q, array($qid));

			                if ($answers_res) {
				            while ($answers = pg_fetch_array($answers_res)) {
				                $anstext         = $answers['anstext'];
				                $anstext         = str_replace("<br />", "", $anstext);
				                $iscorrect         = $answers['iscorrect'];
				                if ($iscorrect ==1){echo "\t\t<li><strong>".$anstext."</strong>\r\n\t\t\t<br>Answer Explanation:<br>".$ansexplanation."\r\n";}
				                else{echo "\t\t<li>".$anstext."</li>\r\n";}				               
				            	}
							}						
							echo "\t</ol>\r\n</li>\r\n";

            }
        } else {
            echo "\r\n\r\n\r\nerror!";
        }

				echo "\r\n<li>How often do you currently apply the following clinical practice strategies?\r\n\t<ol style=\"list-style-type: lower-alpha;\">\r\n";


                    $strategies_q     = "SELECT * FROM tbl_breakingmed_strategies WHERE articleid = $1 and isactive = 1 order by sortorder";
			        $strategies_res = pg_query_params($atpoc_db, $strategies_q, array($record_id));

			        	if ($strategies_res) {
			        		while ($strategies = pg_fetch_assoc($strategies_res)) {
			        			$strategy        = $strategies['strategy'];
			        			$strategy        = str_replace("<p>", "", $strategy);
			        			$strategy        = str_replace("</p>", "", $strategy);
			        			echo "\t\t<li>".$strategy."</li>\r\n";
			        		}
			        	}	

			        	echo "\t</ol>\r\n</li>\r\n";

        echo "\r\n</ol>";







        echo "\r\n\r\n\r\n</html>";

	}
	else {
	header("HTTP/1.1 400 Bad Request");		
	echo "Please supply article_id" ;	
	}	
}


elseif ( $method == "INSERT")
{
	// print_r($POST);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
       
}
elseif ( $method == "UPDATE")
{
	// print_r($PUT);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";	
       
}

elseif ($method == "DELETE")
{
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
}



}
?>
