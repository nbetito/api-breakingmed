<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($record_id) {

		//db connections
        $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);

		// GPS article content from tbl_breakingmed_articles
        $articles_q     = "SELECT * from tbl_breakingmed_articles where articleid = " . $record_id;
        $articles_q_res = pg_query($atpoc_db, $articles_q);
        $articles       = pg_fetch_assoc($articles_q_res);
        $articleid      = $articles["articleid"];
        $jobnum         = $articles["jobnum"];

// BASIC METADATA
        
        $output["articleid"]       = $articles["articleid"];
        $output["jobnum"]          = $articles["jobnum"];
        $output["articletype"]     = $articles["articletype"];
        $output["releasedate"]     = $articles["releasedate"];
        $output["updated"]         = $articles["updated"];
        $output["newsenginstatus"] = $articles["newsenginstatus"];

        // claim credit

        $claim_credit_url           = "https://api.atpoc.com/beta/poc-test-module/?jn=" . $articles["jobnum"] . "&poc_tkn=##UserToken##";
        $output["claim_credit_url"] = $claim_credit_url;

// STORY

        $output["story"] = array(

            'hed'               => $articles["hed"],
            'dek'               => $articles["dek"],
            'authorbyline'      => $articles["authorbyline"],
            'reviewerbyline'    => $articles["reviewerbyline"],
            'takeaways'         => $articles["takeaways"],
            'body'              => $articles["body"],
            'sources'           => $articles["sources"],
            'sourcedisclosures' => $articles["sourcedisclosures"],

        );

// CME INFO

        // learning objectives from tbl_breakingmed_learnobjs

        $learnobjs_q     = "SELECT * FROM tbl_breakingmed_learnobjs WHERE articleid = $1 and isactive = 1 order by sortorder";
        $learnobjs_q_res = pg_query_params($atpoc_db, $learnobjs_q, array($record_id));
        if (pg_num_rows($learnobjs_q_res)) {
            $learnobjs = array();
            while ($row = pg_fetch_assoc($learnobjs_q_res)) {
                $learnobjs[] = $row['lo'];
            }
        }

        // jobnum stuff

        $funder_q     = 'SELECT firstcolblurb from jobfunders where jobnum = \'' . $jobnum . '\'';
        $funder_q_res = pg_query($pik_db, $funder_q);

        if (pg_num_rows($funder_q_res)) {
            while ($pik_row = pg_fetch_assoc($funder_q_res)) {
                $funder_line_raw = $pik_row['firstcolblurb'];
                $funder_line     = urldecode($funder_line_raw);
            }
        }

        $joblist_q     = 'SELECT actgoal,targaud from joblist where jobnum = \'' . $jobnum . '\'';
        $joblist_q_res = pg_query($pik_db, $joblist_q);

        if (pg_num_rows($joblist_q_res)) {
            while ($pik_row = pg_fetch_assoc($joblist_q_res)) {
                $actgoal = $pik_row['actgoal'];
                $targaud = $pik_row['targaud'];
            }
        }

        $output["cme_info"] = array(

            'releasedate'        => $articles["releasedate"],
            'funder_line'        => $funder_line,
            'cme_credit'         => $articles["cme_credit"],
            'ce_credit'          => $articles["ce_credit"],
            'disease_state'      => $articles["disease_state"],
            'target_audience'    => $targaud,
            'activity_goal'      => $actgoal,
            'authorbyline'       => $articles["authorbyline"],
            'reviewerbyline'     => $articles["reviewerbyline"],
            'learnobjs'          => $learnobjs,
            'cmedisclosures'     => $articles["cmedisclosures"],
            'cme_statement'      => $articles["cme_statement"],
            'nursingcestatement' => $articles["nursingcestatement"],

        );

    } else {

        $output["error"] = "please provide articleid";

    }

// debug stuff

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;

    }
}
