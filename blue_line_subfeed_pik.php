<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

/////// ===========  CHANGE LOCATION TO /var/www/s1/moonlight/content/ if needed =========== ///////

    if ($GET['prod'] == 1) {
        $content_location = '/var/www/s1/moonlight/content/';
    } else {
        $content_location = '/var/www/s1/moonlight/content-stage/';
    }


    $dbhost_d   = $GLOBALS['wawa_server'];
    $dbuser_d   = $GLOBALS['wawa_user'];
    $dbpasswd_d = $GLOBALS['wawa_password'];
    $dbdb_d     = $GLOBALS['wawa_database'];

    $dbhost_legacycms   = $GLOBALS['wawa_server'] ;
    $dbuser_legacycms   = $GLOBALS['wawa_user'];
    $dbpasswd_legacycms = $GLOBALS['wawa_password'];
    $dbdb_legacycms     = $GLOBALS['wawa_database'];
    $dbdb_pik           = $GLOBALS['m_database'];

    $chapter_order             = 0;
    $subchapter_order          = 0;
    $page_order                = 1;
    $jn                        = '0000';
    $default_cme_instructions  = "\n<p class=\"body12arial\"><br>To obtain CME/CE credit:</p>\n<ol id=\"to_obtain_credit_list\">\n<li class=\"learning_obj\">Read or listen to each activity carefully.</li>\n<li class=\"learning_obj\">Complete/submit each posttest and evaluation.</li>\n<li class=\"learning_obj\">A record of your participation will be available in the CME Tracker area of this application and the certificate will be available on our website.</li>\n\n</ol>\n";
    $default_no_fee_disclaimer = "There is no fee for this activity.";
    $default_trademark         = "\n<p class=\"body11arial\">Projects In Knowledge<sup>&reg;</sup> is a registered trademark of Projects In Knowledge, Inc.</p>\n";

    $subjob = array();

//GET
    if ($method == "SELECT") {

        #var_dump($record_id);

        $jn = (strlen($record_id) > 0 ? $record_id : $jn);

        $dbconn_legacycms = pg_connect("host=" . $dbhost_legacycms . " port=5432 dbname=" . $dbdb_legacycms . " user=" . $dbuser_legacycms . " password=" . $dbpasswd_legacycms);
        $dbconn_pik       = pg_connect("host=" . $dbhost_legacycms . " port=5432 dbname=" . $dbdb_pik . " user=" . $dbuser_legacycms . " password=" . $dbpasswd_legacycms);

        if (!$dbconn_legacycms) {
            die('Could not connect to the LegacyCMS database' . pg_last_error($dbconn));
        }

        $cme_pik_q = "SELECT jobnum
                    ,title as activity_title
                    ,accmecred as cme_credit
                    ,acpecred as ce_credit
                    ,aacncred as cpe_credit
                    ,reldate as releasedate
                    ,termdate
                    ,targaud as target_audience
                    ,actgoal as activity_goal
                    ,is_moc
                    ,moc_credit_type
                    ,upn ,abim_specialty

                    FROM joblist
                    WHERE jobnum = $1";

        $aapa_pik_q = "SELECT * from jobcredit where jobnum = $1 and type = 'AAPA' and isactive = 1";

        $funder_pik_q = "SELECT thirdcolblurb as funder_line
                    FROM jobfunders
                    WHERE jobnum = $1";

        $faculty_pik_q = "SELECT job.role as role
                    ,faculty.fname as name
                    ,faculty.affil as affiliation
                    ,'' as organization
                    ,'https://www.projectsinknowledge.com' || photo.filename as photo
                    ,job.disclosure

                    FROM jobfaculty job
                    JOIN faculty faculty on job.facultyid = faculty.facultyid
                    JOIN peoplephotos photo on job.photoid = photo.id

                    WHERE jobnum = $1

                    ORDER BY job.rank";

        $writer_pik_q = "SELECT job.role as role
                    ,writer.fname as name
                    ,job.disclosure

                    FROM jobwriters job
                    JOIN writers writer on job.writerid = writer.writerid

                    WHERE jobnum = $1";

        $lo_pik_q = "SELECT lo
                    FROM learnobjs
                    WHERE jobnum = $1
                    ORDER BY sortorder";

        $pages_legacycms_q = "SELECT t2.pageid
                                ,t2.page_title
                                ,t2.show_pretest
                                ,t1.parentid
                                ,t10.jobnum
                                ,t1.sortorder-1 as page_num
                                ,t1.tocid
                                ,t1.bookid
                                ,t2.cola
                                ,t2.colb
                                ,t3.bookid
                          FROM lmt_toc t1
                                    join lmt_pages t2 on t1.tocid=t2.tocid
                                      join lmt_toc t10  on t10.tocid=t1.parentid
                                      join lmt_toc t100 on t100.tocid = t10.parentid
                                      join lmt_book t3 on t3.bookid = t1.bookid
                          WHERE t10.jobnum = $1
                                            and t1.toc_type = 'page'
                                            and t1.status = 'active'
                                            and t10.status = 'active'
                                            and t100.publish2ios = 1
                          ORDER by t1.parentid
                                  ,page_num
                                  ,t2.pageid
                                  ,t2.tocid";                          

        $cme_pik_res         = pg_query_params($dbconn_pik, $cme_pik_q, array($jn));
        $aapa_pik_res        = pg_query_params($dbconn_pik, $aapa_pik_q, array($jn));
        $funder_pik_res      = pg_query_params($dbconn_pik, $funder_pik_q, array($jn));
        $faculty_pik_res     = pg_query_params($dbconn_pik, $faculty_pik_q, array($jn));
        $writer_pik_res      = pg_query_params($dbconn_pik, $writer_pik_q, array($jn));
        $lo_pik_res          = pg_query_params($dbconn_pik, $lo_pik_q, array($jn));
        $pages_legacycms_res = pg_query_params($dbconn_legacycms, $pages_legacycms_q, array($jn));

        $cme_statement                   = null;
        $ce_statement                    = null;
        $cpe_statement                   = null;
        $ja_statement                    = null;
        $cmedisclosures                  = null;
        $funder_line                     = null;
        $contact_information             = null;
        $mutual_responsibility_statement = null;
        $ipe_affiliations                = null;

        if (!pg_num_rows($cme_pik_res)) {

            header("HTTP/1.1 400 Bad Request");
            //$output ['Error'] = "Jobnum does not exist" ;
            die('Jobnum >> ' . $jn . ' << does not exist');

        } else {

            while ($row_cme = pg_fetch_assoc($cme_pik_res)) {



                $row_funder  = pg_fetch_assoc($funder_pik_res);
                $funder_line = urldecode($row_funder["funder_line"]);

                $subjob["surNum"]                             = ""; // deprecated
                $subjob["subNum"]                             = ""; // deprecated
                $subjob["testjobnum"]                         = $row_cme["jobnum"];
                $subjob["title"]                              = $row_cme["activity_title"];
                $subjob["cme"]["funderLogos"]["peerReviewed"] = array();

                $subjob["cme"]["funderLogos"]["peerReviewed"][0]["link"]   = "https://www.projectsinknowledge.com";
                $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgsrc"] = "https://cdn.atpoc.com/cdn/DESIGN/breakingmed/logos/PIK.gif";
                $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgAlt"] = "Projects In Knowledge&reg;";

                $subjob["cme"]["funderLogos"]["peerReviewed"][1]["link"]   = "https://www.projectsinknowledge.com";
                $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgsrc"] = "https://cdn.atpoc.com/cdn/DESIGN/breakingmed/logos/TIK.gif";
                $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgAlt"] = "Peer Reviewed";

                $subjob["cme"]["funderLogos"]["tieredFunders"] = $funder_line;

                $subjob["cme"]["faculty"] = array();

                $faculty_cnt        = 0;
                $faculty_disclosure = "";

                while ($row_fcl = pg_fetch_assoc($faculty_pik_res)) {

                    $subjob["cme"]["faculty"][$faculty_cnt]["role"]            = $row_fcl["role"];
                    $subjob["cme"]["faculty"][$faculty_cnt]["facultyImage"]    = $row_fcl["photo"];
                    $subjob["cme"]["faculty"][$faculty_cnt]["fullName"]        = $row_fcl["name"];
                    $subjob["cme"]["faculty"][$faculty_cnt]["credentials"]     = null;
                    $subjob["cme"]["faculty"][$faculty_cnt]["jobAffiliations"] = $row_fcl["affiliation"];

                    $faculty_disclosure = $faculty_disclosure . "<p><strong>" . $row_fcl["name"] . ",</strong> " . $row_fcl["disclosure"] . "</p>";

                    $faculty_cnt++;
                }

                $writer_cnt        = 0;
                $writer_disclosure = "";
                while ($row_writer = pg_fetch_assoc($writer_pik_res)) {
                    $writer_disclosure = $writer_disclosure . "<p><strong>" . $row_writer["name"] . "</strong> " . $row_writer["disclosure"] . "</p>";
                    $writer_cnt++;
                }

                #$row_cme["is_moc"] = '1';
                $subjob["cme"]["estimatedTimeToComplete"]                           = array();
                $subjob["cme"]["estimatedTimeToComplete"][0]["creditType"]          = ""; // deprecated
                $subjob["cme"]["estimatedTimeToComplete"][0]["availFor"]            = ($row_cme["is_moc"]) == '1' ? "CME / CE / ABIM MOC" : "CME / CE";
                $subjob["cme"]["estimatedTimeToComplete"][0]["cme_publishDate"]     = date('M d, Y', strtotime($row_cme["releasedate"]));
                $subjob["cme"]["estimatedTimeToComplete"][0]["cme_terminationDate"] = date('M d, Y', strtotime($row_cme["termdate"]));
                $subjob["cme"]["estimatedTimeToComplete"][0]["ce_publishDate"]      = date('M d, Y', strtotime($row_cme["releasedate"]));
                $subjob["cme"]["estimatedTimeToComplete"][0]["ce_terminationDate"]  = date('M d, Y', strtotime($row_cme["termdate"]));
                $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_publishDate"]     = date('M d, Y', strtotime($row_cme["releasedate"]));
                $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_terminationDate"] = date('M d, Y', strtotime($row_cme["termdate"]));

                $subjob["cme"]["credits"] = array(); //correlates to  'estimated time for completion'
                $cme_minutes              = floatval($row_cme["cme_credit"]) * 60;
                // $aapa_minutes             = floatval($row_cme["aapa_credit"]) * 60;
                $ce_minutes  = floatval($row_cme["ce_credit"]) * 60;
                $cpe_minutes = floatval($row_cme["cpe_credit"]) * 60;

                $row_aapa     = pg_fetch_assoc($aapa_pik_res);
                $is_aapa = $row_aapa["maxcredit"];
                $aapa_minutes = floatval($row_aapa["maxcredit"]) * 60;
                $aapa_credit  = strval(number_format(floatval($row_aapa["maxcredit"]), 2));

                if (isset($is_aapa)) {
                    $subjob["cme"]["credits"][0]["creditType"]   = "CME for Physicians";
                    $subjob["cme"]["credits"][0]["actualCredit"] = $cme_minutes . " minutes";
                    $subjob["cme"]["credits"][1]["creditType"]   = "CME for Physician Assistants";
                    $subjob["cme"]["credits"][1]["actualCredit"] = $aapa_minutes . " minutes";
                    $subjob["cme"]["credits"][2]["creditType"]   = "CNE for Nurses";
                    $subjob["cme"]["credits"][2]["actualCredit"] = $ce_minutes . " minutes";
                    $subjob["cme"]["credits"][3]["creditType"]   = "CPE for Pharmacists";
                    $subjob["cme"]["credits"][3]["actualCredit"] = $cpe_minutes . " minutes";
                } else {
                    $subjob["cme"]["credits"][0]["creditType"]   = "CME for Physicians";
                    $subjob["cme"]["credits"][0]["actualCredit"] = $cme_minutes . " minutes";
                    $subjob["cme"]["credits"][1]["creditType"]   = "CNE for Nurses";
                    $subjob["cme"]["credits"][1]["actualCredit"] = $ce_minutes . " minutes";
                    $subjob["cme"]["credits"][2]["creditType"]   = "CPE for Pharmacists";
                    $subjob["cme"]["credits"][2]["actualCredit"] = $cpe_minutes . " minutes";
                }

                $subjob["cme"]["abim_credit"] = ($row_cme["is_moc"] == '1') ? "<em>You are Eligible for AMA PRA Category 1 Credit(s)<sup>TM</sup> ABIM MOC points</em>" : null;

                $subjob["cme"]["credits_available"] = array(); // new from AAPA spec

                $cme_credit = strval(number_format(floatval($row_cme["cme_credit"]), 2));
                $ce_credit  = strval(number_format(floatval($row_cme["ce_credit"]), 2));
                $cpe_credit = strval(number_format(floatval($row_cme["cpe_credit"]), 2));

                $subjob["cme"]["credits_available"][0]["creditType"]   = "Physicians";
                $subjob["cme"]["credits_available"][0]["actualCredit"] = $cme_credit;
                $subjob["cme"]["credits_available"][1]["creditType"]   = "ABIM MOC";
                $subjob["cme"]["credits_available"][1]["actualCredit"] = ($row_cme["is_moc"] == '1') ? $cme_credit : null;
                $subjob["cme"]["credits_available"][2]["creditType"]   = "Physician Assistants";
                $subjob["cme"]["credits_available"][2]["actualCredit"] = (isset($is_aapa)) ? $aapa_credit : null;
                $subjob["cme"]["credits_available"][3]["creditType"]   = "Nurses";
                $subjob["cme"]["credits_available"][3]["actualCredit"] = $ce_credit;
                $subjob["cme"]["credits_available"][4]["creditType"]   = "Pharmacists";
                $subjob["cme"]["credits_available"][4]["actualCredit"] = $cpe_credit;
                $subjob["cme"]["credits_available"][5]["creditType"]   = "IPCE";
                $subjob["cme"]["credits_available"][5]["actualCredit"] = (isset($is_aapa)) ? $aapa_credit : null;

                // $subjob["cme"]["cmeInstructions"] = array();
                // $subjob["cme"]["cmeInstructions"][0]["title"] = 'CME/CE Instructions';
                // $subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = $default_cme_instructions;
                // $subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = $default_no_fee_disclaimer;

                // IPE hotfix
                $subjob["cme"]["cmeInstructions"]                       = array();
                $subjob["cme"]["cmeInstructions"][0]["title"]           = "Interprofessional Education Committee";
                $subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = "<p>Interprofessional Education (IPE) requires collaboration among and between professions in order to enhance skills and strategies for effective healthcare team practices including communication and coordination of care. In guiding the overall CE Program, the role of the IPE Committee is to improve interprofessional collaborative practice by helping to recognize the different roles, responsibilities, and expertise of other healthcare professionals to develop team strategies that improve outcomes.</p><p>The IPE Committee members include:</p><p><strong>Vandana G. Abramson, MD</strong><br />Associate Professor of Medicine<br />Vanderbilt University Medical Center<br />Nashville, Tennessee</p><p><strong>James D. Bowen, MD</strong><br />Medical Director, Multiple Sclerosis Center<br />Swedish Neuroscience Institute<br />Seattle, Washington</p><p><strong>Veronica Esquible, PharmD</strong><br />Clinical Lead Pharmacist<br />Department of Pharmacotherapy<br />Swedish Medical Center<br />Seattle, Washington</p><p><strong>Bradley J. Monk, MD, FACS, FACOG</strong><br />Arizona Oncology (US Oncology Network)<br />Professor, Gynecologic Oncology<br />University of Arizona and Creighton University<br />Medical Director of US Oncology Research Gynecology Program<br />Phoenix, Arizona</p><p><strong>Brant Oliver, PhD, MS, MPH, APRN-BC</strong><br />Healthcare Improvement Scientist<br />Associate Professor<br />The Dartmouth Institute &amp; Geisel School of Medicine at Dartmouth<br />Hanover, New Hampshire</p><p><strong>Kendall Shultes, PharmD</strong><br />Clinical Pharmacist/Assistant Professor<br />Vanderbilt/Belmont University<br />Nashville, Tennessee</p><p><strong>Katherine Sibler, MSN, WHNP-BC</strong><br />Nurse Practitioner<br />Vanderbilt Breast Center<br />Nashville, Tennessee</p>";
                $subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = "";
                $subjob["cme"]["cmeInstructions"][1]["title"]           = 'CME/CE Instructions';
                $subjob["cme"]["cmeInstructions"][1]["cmeInstructions"] = $default_cme_instructions;
                $subjob["cme"]["cmeInstructions"][1]["noFeeDisclaimer"] = $default_no_fee_disclaimer;

                $subjob["cme"]["contactInfo"]    = "<h4>Contact Information</h4><p>For questions about content or obtaining CME credit, please <a href=\"https://www.projectsinknowledge.com/contact_us/contact_us.cfm\">contact us</a>.</p>";
                $subjob["cme"]["targetAudience"] = $row_cme["target_audience"];
                $subjob["cme"]["activityGoal"]   = $row_cme["activity_goal"];

                $subjob["cme"]["learningObjectives"] = array();

                $lo_cnt = 0;
                while ($row_lo = pg_fetch_assoc($lo_pik_res)) {

                    $subjob["cme"]["learningObjectives"][$lo_cnt]["objective"] = $row_lo["lo"];

                    $lo_cnt++;
                }

                $subjob["cme"]["cmeInfo"] = array();

                if (isset($is_aapa)) {
                    $ja_statement = '<p><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ja.png" width="120" border="0">In support of improving patient care, Projects In Knowledge<sup>&reg;</sup> is jointly accredited by the Accreditation Council for Continuing Medical Education (ACCME), the Accreditation Council for Pharmacy Education (ACPE), and the American Nurses Credentialing Center (ANCC), to provide continuing education for the healthcare team.</p><br><p style="clear:both"><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ipce.png" width="120" border="0">This activity was planned by and for the healthcare team, and learners will receive ' . $cme_credit . ' Interprofessional Continuing Education (IPCE) credit for learning and change.</p><br><br>';
                } else {
                    $ja_statement = '<p><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ja.png" width="120" border="0">In support of improving patient care, Projects In Knowledge<sup>&reg;</sup> is jointly accredited by the Accreditation Council for Continuing Medical Education (ACCME), the Accreditation Council for Pharmacy Education (ACPE), and the American Nurses Credentialing Center (ANCC), to provide continuing education for the healthcare team.</p><br>';
                }

                $cme_statement = ($row_cme["is_moc"] == '1') ? "<p>Projects In Knowledge<sup>®</sup> designates this enduring material for a maximum of " . $cme_credit . " <i>AMA PRA Category 1 Credit(s)</i><sup>&trade;</sup>. Physicians should claim only the credit commensurate with the extent of their participation in the activity.</p><p>Successful completion of this CME activity, which includes participation in the evaluation component, enables the participant to earn up to " . $cme_credit . " Medical Knowledge MOC points in the American Board of Internal Medicine&#8217;s (ABIM) Maintenance of Certification (MOC) program. Participants will earn MOC points equivalent to the amount of CME credits claimed for the activity. It is the CME activity provider&#8217;s responsibility to submit participant completion information to ACCME for the purpose of granting ABIM MOC credit.</p>" : "<p class=\"body12arial\">Projects In Knowledge<sup>®</sup> designates this enduring material for a maximum of " . $cme_credit . " <i>AMA PRA Category 1 Credit(s)</i><sup>&trade;</sup>. Physicians should claim only the credit commensurate with the extent of their participation in the activity.</p>";

                $aapa_statement = '<p>Projects In Knowledge<sup>&#174;</sup> has been authorized by the American Academy of PAs (AAPA) to award AAPA Category 1 CME credit for activities planned in accordance with AAPA CME Criteria. This activity is designated for ' . $aapa_credit . ' <em>AAPA Category 1 Credit(s)</em><sup>&#8482;</sup>. Approval is valid until ' . date('m/d/Y', strtotime($row_cme["termdate"])) . '. PAs should only claim credit commensurate with the extent of their participation.</p>';

                $ce_statement = "<p>Upon completion of this course, participants will be awarded " . $ce_credit . " nursing contact hour(s).</p><p>Joint Accreditation Nursing Provider Number: 4008235</p><p style=\"font-size:smaller;\">DISCLAIMER: Accreditation refers to educational content only and does not imply ANCC, or PIK endorsement of any commercial product or service.</p>";

                $cpe_statement = "<p>This program has been planned and implemented in accordance with the ACPE Criteria for Quality and Interpretive Guidelines. This activity is worth up to " . $cpe_credit . " contact hour(s)(" . strval(number_format(floatval($row_cme["cpe_credit"]) * 0.1, 2)) . " CEU). The ACPE Universal Activity Number assigned to this Knowledge-type activity is " . $row_cme["upn"] . ".</p><p>Pharmacists should only claim credit commensurate with the extent of their participation in the activity.</p>";

                $subjob["cme"]["cmeInfo"][0]["title"]                    = "Joint Accreditation Statement";
                $subjob["cme"]["cmeInfo"][0]["statementOfAccreditation"] = null;
                $subjob["cme"]["cmeInfo"][0]["creditDesignation"]        = $ja_statement;

                $subjob["cme"]["cmeInfo"][1]["title"]                    = "Physicians";
                $subjob["cme"]["cmeInfo"][1]["statementOfAccreditation"] = null;
                $subjob["cme"]["cmeInfo"][1]["creditDesignation"]        = $cme_statement;

                if (isset($is_aapa)) {
                    $ce_statement_order                                      = 3;
                    $cpe_statement_order                                     = 4;
                    $subjob["cme"]["cmeInfo"][2]["title"]                    = "Physician Assistants";
                    $subjob["cme"]["cmeInfo"][2]["statementOfAccreditation"] = null;
                    $subjob["cme"]["cmeInfo"][2]["creditDesignation"]        = $aapa_statement;
                } else {
                    $ce_statement_order  = 2;
                    $cpe_statement_order = 3;
                }

                $subjob["cme"]["cmeInfo"][$ce_statement_order]["title"]                    = "Nurses";
                $subjob["cme"]["cmeInfo"][$ce_statement_order]["statementOfAccreditation"] = null;
                $subjob["cme"]["cmeInfo"][$ce_statement_order]["creditDesignation"]        = $ce_statement;

                $subjob["cme"]["cmeInfo"][$cpe_statement_order]["title"]                    = "Pharmacists";
                $subjob["cme"]["cmeInfo"][$cpe_statement_order]["statementOfAccreditation"] = null;
                $subjob["cme"]["cmeInfo"][$cpe_statement_order]["creditDesignation"]        = $cpe_statement;

                $subjob["cme"]["disclosureInformation"] = "<p>Projects In Knowledge<sup>&reg;</sup> adheres to the ACCME's <em>Standards for Integrity and Independence in Accredited Continuing Education</em>. Any individuals in a position to control the content of a CE activity, including faculty, planners, reviewers or others are required to disclose all relevant financial relationships with ineligible entities (commercial interests)*. All relevant conflicts of interest have been mitigated prior to the commencement of the activity.</p>" . $faculty_disclosure . $writer_disclosure . "<p><strong>Peer Reviewer</strong> has no relevant financial relationships to disclose.</p><p><strong>Vandana G. Abramson, MD</strong> (IPE Committee) has no relevant financial relationships to disclose.</p><p><strong>James D. Bowen, MD</strong> (IPE Committee), has received honoraria from Biogen, EMD Serono, Inc., Genentech, and Novartis; has received consulting fees from Biogen, Celgene, EMD Serono, Genentech, a Member of the Roche Group, and Novartis Pharmaceuticals Corporation; has conducted contracted research for Alexion Pharmaceuticals, Inc, Alkermes, Biogen, Celgene Corporation, Genentech, a Member of the Roche Group, Genzyme, Novartis Pharmaceuticals Corporation, and TG Therapeutics, Inc; and has ownership interest in Amgen Inc.</p><p><strong>Veronica Esquible, PharmD</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p><strong>Bradley Monk, MD, FACOG, FACS</strong> (IPE Committee), has received consulting fees from AbbVie, Inc, Advaxis, Inc., Agenus Inc., Amgen Inc., Aravive, Inc., Asymmetric Therapeutics, Boston Biomedical, Chemocare, ChemoID, Circulogene, Conjupro Biotherapeutics Inc, Eisai Co., Ltd, Geistlich Pharma North America, Inc., Genmab/Seattle Genetics, GOG Foundation, Immunomedics, Inc., ImmunoGen, Inc., Incyte Corporation, Laekna Healthcare Private Limited, Mateon Therapeutics, Merck Sharp &amp; Dohme Corp., a subsidiary of Merck &amp; Co., Inc, Mersana Therapeutics, Inc., Myriad Genetics, Nucana, OncoMed Pharmaceuticals, OncoQuest Inc., OncoSec Medical Inc, Perthera Inc., Pfizer Inc., Precision Oncology, Puma Biotechnology Inc, Regeneron Pharmaceuticals, Samumed, LLC, Takeda Pharmaceutical Company, Ltd, VBL Therapeutics, and Vigeo Therapeutics; and has been a Speaker/Consultant for AstraZeneca, Clovis Oncology, F. Hoffmann-La Roche Ltd/ Genentech, A Member of the Roche Group, Janssen Pharmaceuticals/Johnson &amp; Johnson, Inc., and Tesaro, Inc/GlaxoSmithKline.</p><p><strong>Brant Oliver, PhD, MS, MPH, APRN-BC</strong> (IPE Committee), has conducted contracted research for Biogen and EMD Serono.</p><p><strong>Kendall Shultes, PharmD</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p><strong>Katherine Sibler, MSN, WHNP-BC</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p>Projects In Knowledge staff members have no relevant financial relationships to disclose.</p><p>Conflicts of interest are thoroughly vetted by the Executive Committee of Projects In Knowledge. All conflicts are resolved prior to the beginning of the activity by the Trust In Knowledge peer review process.</p><p>The opinions expressed in this activity are those of the faculty and do not necessarily reflect those of Projects In Knowledge.</p><p>*An ineligible company (commercial interest) is any entity producing, marketing, re-selling, or distributing health care goods or services consumed by, or used, on patients. </p>";

                $subjob["cme"]["facultyDisclosures"]                  = array();
                $subjob["cme"]["facultyDisclosures"][0]["fullName"]   = ""; // deprecated
                $subjob["cme"]["facultyDisclosures"][0]["disclosure"] = ""; // deprecated
                $subjob["cme"]["facultyDisclosures"][0]["role"]       = ""; // deprecated

                $subjob["cme"]["medicalWriters"]                  = array();
                $subjob["cme"]["medicalWriters"][0]["fullName"]   = ""; // deprecated
                $subjob["cme"]["medicalWriters"][0]["disclosure"] = ""; // deprecated

                $subjob["cme"]["noPeerReviewerDisclosures"] = ""; // deprecated

                $subjob["cme"]["ceDisclosures"]                  = array();
                $subjob["cme"]["ceDisclosures"][0]["fullName"]   = ""; // deprecated
                $subjob["cme"]["ceDisclosures"][0]["disclosure"] = ""; // deprecated

                $subjob["cme"]["cmeAccreditorProvider"] = ""; // deprecated
                $subjob["cme"]["PIKRelationship"]       = ""; // deprecated
                $subjob["cme"]["conflictsOfInterest"]   = ""; // deprecated

                $subjob["cme"]["finalBlurb"] = ""; // deprecated

                $subjob["cme"]["funderInfo"]                   = array();
                $subjob["cme"]["funderInfo"][0]["funderJN"]    = (float) $row_cme["jobnum"];
                $subjob["cme"]["funderInfo"][0]["fundertext"]  = $funder_line;
                $subjob["cme"]["funderInfo"][0]["fundertext2"] = $funder_line;
                $subjob["cme"]["funderInfo"][0]["funderHTML"]  = '<p id="fundertext">' . $funder_line . '</p>';

                $funder_line = $funder_line; //need in pages

                // $subjob["cme"]["mutualResponsibility"] = $row_cme["mutual_responsibility_statement"];
                $subjob["cme"]["mutualResponsibility"] = "<h4>CONTRACT FOR MUTUAL RESPONSIBILITY IN CME/CE</h4><p>Projects In Knowledge has developed the contract to demonstrate our commitment to providing the highest quality professional education to clinicians, and to help clinicians set educational goals to challenge and enhance their learning experience.</p><p>For more information on the contract, <a href=\"https://www.projectsinknowledge.com/mutual_responsibility.cfm\" target=\"_blank\">click here</a></p>";

                $subjob["cme"]["trademark"]       = $default_trademark;
                $subjob["cme"]["is_moc"]          = $row_cme["is_moc"];
                $subjob["cme"]["moc_credit_type"] = $row_cme["moc_credit_type"];
                $subjob["cme"]["abim_specialty"]  = $row_cme["abim_specialty"];


                $subjob["cmeNum"] = (float) $row_cme["jobnum"];

            }

            $subjob["pages"] = array();

            $page_cnt = 0;
            while ($row_p = pg_fetch_assoc($pages_legacycms_res)) {

                $subjob["pages"][$page_cnt]["pageNum"]       = $row_p["page_num"];
                $subjob["pages"][$page_cnt]["tocTitle"]      = $row_p["page_title"];
                $subjob["pages"][$page_cnt]["tocType"]       = "page";
                $subjob["pages"][$page_cnt]["tocID"]         = $row_p["tocid"];
                $subjob["pages"][$page_cnt]["parentID"]      = $row_p["parentid"];
                $subjob["pages"][$page_cnt]["jobNum"]        = ""; // deprecated
                $subjob["pages"][$page_cnt]["jobNumDot"]     = ""; // deprecated
                $subjob["pages"][$page_cnt]["cola"]          = $row_p["cola"];
                $subjob["pages"][$page_cnt]["colb"]          = $row_p["colb"];
                $subjob["pages"][$page_cnt]["colc"]          = ""; // deprecated
                $subjob["pages"][$page_cnt]["cold"]          = ""; // deprecated
                $subjob["pages"][$page_cnt]["cole"]          = ""; // deprecated
                $subjob["pages"][$page_cnt]["page_type"]     = "page";
                $subjob["pages"][$page_cnt]["chapternum"]    = ""; // deprecated
                $subjob["pages"][$page_cnt]["subchapternum"] = ""; // deprecated
                $subjob["pages"][$page_cnt]["page_title"]    = $row_p["page_title"];
                $subjob["pages"][$page_cnt]["pageid"]        = ""; // deprecated
                $subjob["pages"][$page_cnt]["swipeleft"]     = ""; // deprecated
                $subjob["pages"][$page_cnt]["swiperight"]    = ""; // deprecated
                $subjob["pages"][$page_cnt]["swipedown"]     = ""; // deprecated
                $subjob["pages"][$page_cnt]["swipeup"]       = ""; // deprecated
                $subjob["pages"][$page_cnt]["sortorder"]     = $page_cnt + 1;
                $subjob["pages"][$page_cnt]["dlu"]           = ""; // deprecated
                $subjob["pages"][$page_cnt]["datepublished"] = ""; // deprecated
                $subjob["pages"][$page_cnt]["publishedby"]   = ""; // deprecated
                $subjob["pages"][$page_cnt]["publish2ios"]   = ""; // deprecated
                $subjob["pages"][$page_cnt]["showPosttest"]  = ""; // deprecated
                $subjob["pages"][$page_cnt]["showPretest"]   = $row_p["show_pretest"];
                $subjob["pages"][$page_cnt]["showClaim"]     = ""; // deprecated
                $subjob["pages"][$page_cnt]["bookid"]        = $row_p["bookid"];
                $subjob["pages"][$page_cnt]["portBack"]      = ""; // deprecated
                $subjob["pages"][$page_cnt]["landBack"]      = ""; // deprecated
                $subjob["pages"][$page_cnt]["testjobnum"]    = $jn;
                $subjob["pages"][$page_cnt]["funderText"]    = $funder_line;
                $subjob["pages"][$page_cnt]["layout"]        = ""; // deprecated
                $subjob["pages"][$page_cnt]["template"]      = ""; // deprecated

                $page_cnt++;

            }

            $subjob["pretest"]               = null;
            $subjob["posttest"]["questions"] = array();
            $subjob["survey"]                = null;

            pg_close($dbconn_pik);
            // $profile['details'] = json_decode( $profilejson, true );

        }

        $output = $subjob;

        $subjob_json_file_ext = $content_location . $jn . ".json";
        $subjob_json_file     = $content_location . $jn;

        $subjob_json_ext = fopen($subjob_json_file_ext, "w") or die("Unable to open file!");
        fwrite($subjob_json_ext, json_encode($output));

        $subjob_json = fopen($subjob_json_file, "w") or die("Unable to open file!");
        fwrite($subjob_json, json_encode($output));

        fclose($subjob_json_ext);
        fclose($subjob_json);

        $result = array();
        $result['json'] = "https://secureapi.atpoc.com/s1/moonlight/content/".$record_id;        
        $result['suiteweb'] = "https://suiteweb.atpointofcare.com/#library/news/".$record_id."/page/0";


    } elseif ($method == "INSERT") {
        // print_r($POST);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        header("HTTP/1.1 201 Created");
        $resJson = json_encode($result);
        echo $resJson;
        // echo "https://secureapi.atpoc.com/s1/moonlight/content/".$record_id;
    }

    apilog($debug, $profile);

}
