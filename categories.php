<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    // if ($method == "SELECT") {

    //db connections
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

    $updates  = array();
    $mappings = array();

    $mapping_q = "SELECT map.ta_num
        ,map.subdomain
        ,string_agg( '''' || map.categoryid::text || '''', ','::text) as cats
        ,map.displayname
        ,map.keywords
        FROM tbl_breakingmed_categories_ww_ta map
        join ww_therapeutic_area ta on map.ta_num = ta.ta_num
        WHERE ta.archived = false and map.active_yn = 1
        group by map.subdomain, map.ta_num, map.displayname, map.keywords
        order by map.displayname";

    $mapping_res = pg_query($atpoc_db, $mapping_q);

    while ($mapping = pg_fetch_assoc($mapping_res)) {

        $articles_q = "SELECT releasedate as update_date
            ,trim(hed) as update_text
            ,coalesce ('<strong>'||trim(dek)|| '</strong><br> ', '')  || coalesce (trim(promo_blurb), '')  as update_text_full
            ,articleid
            ,jobnum, '' as page_url
            ,'https://suiteweb.atpointofcare.com/#library/news/breakingmed/' || articleid as suiteweb_url
            ,disease_state
            ,articletype
            ,cattopicid
            FROM tbl_breakingmed_articles
            WHERE releasedate > (now() - '90 days'::interval)
            and include_in_updates = 1
            and published = '1'
            and slug not like 'KHN%' and slug not like 'LP%'
            and string_to_array(cattopicid, ',') && array[" . $mapping["cats"] . "]";

        $articles_q .= (strlen($mapping["keywords"]) > 1) ? "and (hed like '%" . $mapping["keywords"] . "%' or dek like '%" . $mapping["keywords"] . "%' or promo_blurb like '%" . $mapping["keywords"] . "%')" : "";

        $articles_q .= "ORDER by releasedate desc LIMIT 20;";

        $articles_res = pg_query($atpoc_db, $articles_q);

        while ($articles = pg_fetch_assoc($articles_res)) {

            $url = $articles["suiteweb_url"];
            $url = str_replace("news", $mapping["subdomain"], $url);

            $entry = array(
                'update_date'       => $articles["update_date"],
                'update_text'       => $articles["update_text"],
                'update_text_full'  => $articles["update_text_full"],
                'shortname'         => $mapping["displayname"],
                'subdomain'         => $mapping["subdomain"],
                'ta_num'            => $mapping["ta_num"],
                'displayname'       => $mapping["displayname"],
                'page_url'          => $url,
                'page_url_standard' => str_replace("https://suiteweb.atpointofcare.com/", "", $url),
                'page_url_jobnum'   => $url,
                'rel_url'           => str_replace("https://suiteweb.atpointofcare.com/#", "", $url),
                'flag_hot'          => 't', // stubbed
                'sponsor_name'      => '', //deprecated
                'sponsor_info'      => '', //deprecated
            );
            array_push($updates, $entry);
        }

        // while ($articles = pg_fetch_row($articles_res)) {
        //     $entry = array(
        //         'update_date'       => $articles[0],
        //         'update_text'       => $articles[1],
        //         'update_text_full'  => $articles[2],
        //         'shortname'         => $mapping[1],
        //         'subdomain'         => $mapping[1],
        //         'ta_num'            => $mapping[0],
        //         'displayname'       => $mapping[3],
        //         'page_url'          => $articles[6],
        //         'page_url_standard' => str_replace("https://suiteweb.atpointofcare.com/", "", $articles[6]),
        //         'page_url_jobnum'   => $articles[6],
        //         'rel_url'           => str_replace("https://suiteweb.atpointofcare.com/#", "", $articles[6]),
        //         'flag_hot'          => 't',         // stubbed
        //         'sponsor_name'      => '',          //deprecated
        //         'sponsor_info'      => '',          //deprecated
        //     );
        //     array_push($updates, $entry);
        // }

        array_push($mappings, $mapping);

    }

    // GET OLD UPDATES from ww_updates

    $ww_updates_q = "SELECT t1.update_date
                                ,t1.update_text
                                ,t1.page_url
                                ,t2.shortname
                                ,t1.ta_num
                                ,t2.displayname
                                ,t1.page_url_jobnum
                                ,t1.update_date > current_date - interval '30' day as flag_hot
                                ,t3.name
                                ,t3.info
                                ,t5.subdomain
                        FROM ww_updates t1
                                join ww_therapeutic_area t5 on t5.ta_num = t1.ta_num
                                left join ww_ta_auxiliary t2 on t1.ta_num=t2.ta_num
                                left join ww_sponsors t3 on t1.sponsor_id=t3.id

                        WHERE update_date is not NULL
                            and t1.archived = false
                            and update_date > current_date - interval '90' day

                        ORDER BY t2.displayname asc
                                ,update_date desc";

    $ww_updates_res = pg_query($atpoc_db, $ww_updates_q);
    while ($ww_updates = pg_fetch_assoc($ww_updates_res)) {
            $url = $ww_updates["page_url_jobnum"];
            $hash = explode("#", $url);
            $rel_url = $hash[1];
            $page_url = "https://suiteweb.atpointofcare.com/#library/".$ww_updates["subdomain"].$rel_url;


$headline = implode(' ', array_slice(explode(' ', $ww_updates["update_text"]), 0, 12))."..."; # get the first 12 word tokens


            // var_dump($ww_updates,$rel_url,$page_url,$headline);die();


            

            $entry = array(
                'update_date'       => $ww_updates["update_date"],
                'update_text'       => $headline,
                'update_text_full'  => $ww_updates["update_text"],
                'shortname'         => $ww_updates["shortname"],
                'subdomain'         => $ww_updates["subdomain"],
                'ta_num'            => $ww_updates["ta_num"],
                'displayname'       => $ww_updates["displayname"],
                'page_url'          => $page_url,
                'page_url_standard' => str_replace("https://suiteweb.atpointofcare.com/", "", $page_url),
                'page_url_jobnum'   => $page_url,
                'rel_url'           => str_replace("https://suiteweb.atpointofcare.com/#", "", $page_url),
                'flag_hot'          => 't', // stubbed
                'sponsor_name'      => '', //deprecated
                'sponsor_info'      => '', //deprecated
            );
            array_push($updates, $entry);
    }

    



    // var_dump($ww_updates); die();


    $updates = group_and_sort_by_displayname($updates);


    $output["updates"]  = $updates;
    $output["mappings"] = $mappings;
    $output["token"]    = $token;

    $output['status']    = '1-success';
    $output['message']   = "Latest updates...";
    $output['n_records'] = count($updates);

    // } elseif ($method == "INSERT") {
    //     // print_r($POST);
    //     header("HTTP/1.1 400 Bad Request");
    //     $output['Error'] = "Method not supported";

    // } elseif ($method == "UPDATE") {
    //     // print_r($PUT);
    //     header("HTTP/1.1 400 Bad Request");
    //     $output['Error'] = "Method not supported";

    // } elseif ($method == "DELETE") {
    //     header("HTTP/1.1 400 Bad Request");
    //     $output['Error'] = "Method not supported";
    // }

    if ($debugtoggle == 1) {
        // header("HTTP/1.1 200 Created");
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        // header("HTTP/1.1 200 Created");
        $resJson = json_encode($output);
        echo $resJson;

    }

}






function group_and_sort_by_displayname($arr)
{

    $displaynames = array();
    $ordered      = array();


    $keys = array_column($arr, 'update_date');

    array_multisort($keys, SORT_DESC, $arr);



    foreach ($arr as $entry) {
        if (!in_array($entry["displayname"], $displaynames)) {
            array_push($displaynames, $entry["displayname"]);
        }
    }

    sort($displaynames);

    foreach ($displaynames as $header) {
        foreach ($arr as $entry) {
            if ($header == $entry["displayname"]) {
                array_push($ordered, $entry);
            }
        }
    }

    return $ordered;
}


