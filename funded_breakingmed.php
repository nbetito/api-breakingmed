<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    //db connections
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

    if (!$pik_db) {
        die('Could not connect to the pik database' . pg_last_error($dbconn));
    }

    if (!$atpoc_db) {
        die('Could not connect to the atpoc database' . pg_last_error($dbconn));
    }

    if ($method == "INSERT") {

        //
        // INGESTION HOTFIX FOR EXISTING RECORD
        //

        // add UAN and abim_specialty
        // $tbl_breakingmed_articles = "update tbl_breakingmed_articles set pharmacy_uan = $1, abim_specialty = $2 where articleid = $3 returning *;";
        // $result = pg_query_params($atpoc_db, $tbl_breakingmed_articles, array($POST["acpe_uan"],$POST["abim_specialty"],$POST["articleid"]));
        // if (pg_num_rows($result) < 1) {
        //     die("articleid does not exist");
        // }

        // //
        // //
        // //
        // //
        // // OLDER HOTFIXES
        // //
        // //
        // //
        // //

        // // GPS article content from tbl_breakingmed_articles
        // $articles_q      = "SELECT * from tbl_breakingmed_articles where jobnum = '" . $jobnum . "' ;";
        // $articles_q_res  = pg_query($atpoc_db, $articles_q);
        // $articles        = pg_fetch_assoc($articles_q_res);
        // $articleid       = $articles["articleid"];
        // $jobnum          = $articles["jobnum"];
        // $activity_title  = $articles['hed'];
        // $dek             = $articles['dek'];
        // $author          = $articles['author'];
        // $authorbyline    = $articles['authorbyline'];
        // $reviewer        = $articles['reviewer'];
        // $reviewerbyline  = $articles['reviewerbyline'];
        // $is_moc          = $articles["is_moc"];
        // $moc_credit_type = $articles["moc_credit_type"];
        // $pharmacy_uan    = $articles["pharmacy_uan"];
        // $abim_specialty  = $articles["abim_specialty"];

        // //
        // // LMT tables
        // //

        // Get subchapter tocid by jn
        $lmt_toc_q     = "select lmt.*, ta.subdomain from lmt_toc lmt join ww_therapeutic_area ta on lmt.bookid = ta.ta_num where lmt.jobnum = '" . $POST["jobnum"] . "' and toc_type = 'subchapter' and status = 'active' order by tocid asc limit 1";
        $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
        $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);


        // $tocid    = $lmt_toc["tocid"];
        // $parentid = $lmt_toc["parentid"];

        // // update lmt_toc
        // $query = "update lmt_toc set toc_title = '" . trim($activity_title) . "', publish2ios = 1 where jobnum = '" . $jobnum . "' and toc_type = 'subchapter' and status = 'active' ; ";

        // $output['lmt_toc']['query'] = $query;
        // $result                     = pg_query($atpoc_db, $query);
        // $status                     = pg_result_status($result);
        // if ($status == 1) {
        //     $output['lmt_toc']['status'] = "tocid = " . $tocid . " updated";
        // } else {
        //     $output['lmt_toc']['status'] = "ERROR! NO TOCID // " . $query . " // " . pg_last_error($atpoc_db);
        // }

        // // update lmt_toc parent
        // $query = "update lmt_toc set publish2ios = 1 where tocid = '" . $parentid . "' and toc_type = 'chapter' and status = 'active' ; ";

        // $output['lmt_toc']['parentid_query'] = $query;
        // $result                              = pg_query($atpoc_db, $query);
        // $status                              = pg_result_status($result);

        // if ($status == 1) {
        //     $output['lmt_toc']['parentid_status'] = "tocid = " . $parentid . " updated";
        // } else {
        //     $output['lmt_toc']['parentid_status'] = pg_last_error($atpoc_db);
        // }

        // // update lmt_pages
        // $query = "update lmt_pages set page_title = '" . trim($activity_title) . "' where tocid = '" . $tocid . "'; ";

        // $output['lmt_pages']['query'] = $query;
        // $result                       = pg_query($atpoc_db, $query);
        // $status                       = pg_result_status($result);
        // if ($status == 1) {
        //     $output['lmt_pages']['status'] = "tocid = " . $tocid . " updated";
        // } else {
        //     $output['lmt_pages']['status'] = pg_last_error($atpoc_db);
        // }

        // //
        // // PIK tables
        // //

        // // $joblist_q               = "SELECT jobnum, is_moc, moc_credit_type FROM joblist where jobnum = '" . $jobnum . "'";
        // // $joblist_q_res           = pg_query($pik_db, $joblist_q);
        // // $joblist                 = pg_fetch_assoc($joblist_q_res);
        // // $joblist_is_moc          = $joblist['is_moc'];
        // // $joblist_moc_credit_type = $joblist['moc_credit_type'];

        // // update joblist

        // // ADD PD?

        // // rewrite string
        // $activity_url  = 'https://secureapi.atpoc.com/sandboxapi-nb/zapier/jn?jn=' . $jobnum;
        // $activity_json = file_get_contents($activity_url);
        // $activity      = json_decode($activity_json, true);
        // $subdomain     = $activity["subdomain"];
        // $parentjobnum  = substr($jobnum, 0, 4);
        // $rewritestring = '//suiteweb.atpointofcare.com/#library/' . $subdomain . '/' . $jobnum . '/page/0';

        // $reldate  = $POST["releasedate"];
        // $termdate = date_create($reldate);
        // date_modify($termdate, '+1 year');
        // date_modify($termdate, '-1 day');
        // $termdate = date_format($termdate, 'Y-m-d');

        // $query = "UPDATE joblist set rewritestring ='" . $rewritestring . "'
        // , parentjobnum = " . $parentjobnum . "
        // , parentcurric = " . $parentjobnum . "
        // , byline ='" . $byline . "'
        // , termdate = '" . $termdate . "'
        // , termdaten = '" . $termdate . "'
        // , is_moc = " . $is_moc . "
        // , moc_credit_type = '" . $moc_credit_type . "'
        // , abim_specialty = '" . $abim_specialty . "'
        // , shortname = '" . $POST["slug"] . "'
        // , format = 'BreakingMED'
        // , learningformat = 'Enduring material'
        // , webemailname = 'breakingmed'
        // , hascme = TRUE
        // , hasce = TRUE
        // , hascpe = TRUE
        // , accmecred = 0.50
        // , acpecred = 0.50
        // , aacncred = 0.50
        // , pharmacy_activity_type = 'Knowledge'
        // , pd = '71|1'
        //  where jobnum = '" . $jobnum . "';";

        // $output['joblist']['query'] = $query;

        // $result = pg_query($pik_db, $query);
        // $status = pg_result_status($result);
        // if ($status == 1) {
        //     $output['joblist']['status'] = "jobnum = " . $jobnum . " updated";
        // } else {
        //     $output['joblist']['status'] = pg_last_error($pik_db);
        // }

        // // copy metadata from parent

        // $parentjoblist_q     = "SELECT * from joblist where jobnum = '" . $parentjobnum . "' ;";
        // $parentjoblist_q_res = pg_query($pik_db, $parentjoblist_q);
        // $parentjoblist       = pg_fetch_assoc($parentjoblist_q_res);
        // $clientid            = $parentjoblist["clientid"];
        // $primaryinit         = $parentjoblist["primaryinit"];
        // $actgoal             = $parentjoblist["actgoal"];
        // $targaud             = $parentjoblist["targaud"];

        // // copy metadata from meeting parent

        // $meetingparentjobnum        = substr($jobnum, 0, 6) . "1";
        // $meetingparentjoblist_q     = "SELECT * from joblist where jobnum = '" . $meetingparentjobnum . "' ;";
        // $meetingparentjoblist_q_res = pg_query($pik_db, $meetingparentjoblist_q);
        // $meetingparentjoblist       = pg_fetch_assoc($meetingparentjoblist_q_res);
        // $activitypageblurb          = htmlspecialchars($dek);
        // $activitypageblurb          = str_replace("'", "&#39;", $activitypageblurb);

        // $query = "UPDATE joblist set clientid ='" . $clientid . "', primaryinit = '" . $primaryinit . "', actgoal = '" . $actgoal . "', targaud = '" . $targaud . "', activitypageblurb = '" . $activitypageblurb . "' where jobnum = '" . $jobnum . "';";

        // $output['joblist']['copyfromparentquery'] = $query;

        // $result = pg_query($pik_db, $query);
        // $status = pg_result_status($result);
        // if ($status == 1) {
        //     $output['joblist']['copyfromparentstatus'] = "jobnum = " . $jobnum . " updated";
        // } else {
        //     $output['joblist']['copyfromparentstatus'] = pg_last_error($pik_db);
        // }

        // // update jobfunders

        // // get parent funderlines

        // $funder_q     = "SELECT * from jobfunders where jobnum = '" . $parentjobnum . "' ;";
        // $funder_q_res = pg_query($pik_db, $funder_q);
        // $funder       = pg_fetch_assoc($funder_q_res);
        // $funder_line  = $funder["firstcolblurb"];

        // // check if funder line already exists

        // $funder_check_q     = "SELECT * from jobfunders where jobnum = '" . $jobnum . "' ;";
        // $funder_check_q_res = pg_query($pik_db, $funder_check_q);
        // $funder_check       = pg_fetch_assoc($funder_check_q_res);

        // if (pg_num_rows($funder_check_q_res) == 0) {

        //     $insertFunderQuery_set    = "jobnum,firstcolblurb,secondcolblurb,thirdcolblurb";
        //     $insertFunderQuery_values = "'" . $jobnum . "', '" . $funder_line . "', '" . $funder_line . "', '" . $funder_line . "'";
        //     $insertQuery              = "INSERT into jobfunders (" . $insertFunderQuery_set . ") VALUES (" . $insertFunderQuery_values . ")";

        //     $output['jobfunders']['query'] = $insertQuery;

        //     $result = pg_query($pik_db, $insertQuery);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobfunders']['status'] = "jobnum = " . $jobnum . " updated";
        //     } else {
        //         $output['jobfunders']['status'] = pg_last_error($pik_db);
        //     }

        // } else {
        //     $output['jobfunders']['query']  = $funder_check_q;
        //     $output['jobfunders']['status'] = "funder line already exists for jobnum = " . $jobnum;
        // }

        // // check jobcredit

        // $jobcredit_existing_q    = "select * from jobcredit where jobnum = '" . $jobnum . "';";
        // $jobcredit_existing      = pg_query($pik_db, $jobcredit_existing_q);
        // $jobcredit_existing_rows = pg_num_rows($jobcredit_existing);

        // if ($jobcredit_existing_rows == 0) {

        //     // jobcredit CME
        //     $cme_query = "INSERT into jobcredit (datemodified, createdby , mincredit, maxcredit, termdate, reldate, increment, isincremental, accreditor, accreditorid, type, typeorder, jobnum) VALUES (now(), 999, 0.25, 0.50, '" . $termdate . "', '" . $reldate . "', '0.25', TRUE, 'JA IPCE PIK CME', 75, 'CME', 1, '" . $jobnum . "');";

        //     // jobcredit CE
        //     $ce_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, termdate, reldate, increment, isincremental,  accreditor, accreditorid, accredno, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', 0, FALSE, 'JA IPCE PIK CE', 76, '4008235', 'CE', 2, '" . $jobnum . "');";

        //     // jobcredit CPE
        //     $cpe_query = "INSERT into jobcredit (datemodified, createdby, mincredit, maxcredit, bundlemax, termdate, reldate, increment, isincremental, accreditor, accreditorid, accredno, acpejobtype, type, typeorder, jobnum) VALUES (now(), 999, 0.50, 0.50, 0.50, '" . $termdate . "', '" . $reldate . "', NULL, FALSE, 'JA PIK CPE', 66, '" . $pharmacy_uan . "', 'Knowledge', 'CPE', 3, '" . $jobnum . "');";

        //     // jobcredit AAPA
        //     $aapa_query = "INSERT into jobcredit (jobnum, type, accreditor, accreditorid, mincredit, maxcredit, increment, datecreated, createdby, typeorder, isincremental, isactive, reldate, termdate) VALUES ('" . $jobnum . "', 'AAPA', 'JA IPCE PIK AAPA', 77, '0.5', '0.5', 0, now(), 200, 4, false, 1, '" . $reldate . "', '" . $termdate . "');";

        //     $output['jobcredit']['cme']['query']  = $cme_query;
        //     $output['jobcredit']['ce']['query']   = $ce_query;
        //     $output['jobcredit']['cpe']['query']  = $cpe_query;
        //     $output['jobcredit']['aapa']['query'] = $aapa_query;

        //     $result = pg_query($pik_db, $cme_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['cme']['status'] = "type 'CME', jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $ce_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['ce']['status'] = "type 'CE', jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $cpe_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['cpe']['status'] = "type 'CPE', jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $aapa_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['aapa']['status'] = "type 'AAPA', jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
        //     }

        // } else {

        //     // jobcredit CME
        //     $cme_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, mincredit = 0.25 , maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', increment = '0.25', isincremental = TRUE, accreditor = 'JA IPCE PIK CME', accreditorid = 75 where type = 'CME' and jobnum = '" . $jobnum . "';";

        //     // jobcredit CE
        //     $ce_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK CE', accreditorid = 76, accredno = '4008235' where type = 'CE' and jobnum = '" . $jobnum . "';";

        //     // jobcredit CPE
        //     $cpe_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , bundlemax = 0.50, termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA PIK CPE', accreditorid = 66, acpejobtype = 'Knowledge', accredno = '" . $pharmacy_uan . "' where type = 'CPE' and jobnum = '" . $jobnum . "';";

        //     // jobcredit AAPA

        //     $aapa_query = "UPDATE jobcredit set datemodified = now(), modifiedby = 999, maxcredit = 0.50 , termdate = '" . $termdate . "', reldate = '" . $reldate . "', accreditor = 'JA IPCE PIK AAPA', accreditorid = 77 where type = 'AAPA' and jobnum = '" . $jobnum . "';";

        //     $output['jobcredit']['cme']['query']  = $cme_query;
        //     $output['jobcredit']['ce']['query']   = $ce_query;
        //     $output['jobcredit']['cpe']['query']  = $cpe_query;
        //     $output['jobcredit']['aapa']['query'] = $aapa_query;

        //     $result = pg_query($pik_db, $cme_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['cme']['status'] = "jobnum = " . $jobnum . " updated";
        //     } else {
        //         $output['jobcredit']['cme']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $ce_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['ce']['status'] = "jobnum = " . $jobnum . " updated";
        //     } else {
        //         $output['jobcredit']['ce']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $cpe_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['cpe']['status'] = "jobnum = " . $jobnum . " updated";
        //     } else {
        //         $output['jobcredit']['cpe']['status'] = pg_last_error($pik_db);
        //     }

        //     $result = pg_query($pik_db, $aapa_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobcredit']['aapa']['status'] = "jobnum = " . $jobnum . " updated";
        //     } else {
        //         $output['jobcredit']['aapa']['status'] = pg_last_error($pik_db);
        //     }

        // }

        // // check jobfaculty

        // $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $jobnum . "';";
        // $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
        // $jobfaculty_existing_rows  = pg_num_rows($jobfaculty_existing_q_res);

        // if ($jobfaculty_existing_rows == 0) {

        //     if ($reviewer == "Vandana Abramson") {
        //         $facultyid       = 1065;
        //         $listaffiliation = "<li class=\"affil\">Associate Professor of Medicine</li><li class=\"affil\">Vanderbilt University Medical Center</li>";
        //         $photoid         = 1308;

        //     } elseif ($reviewer == "Kevin Rodowicz") {
        //         $facultyid       = 1072;
        //         $listaffiliation = "<li class=\"affil\">Assistant Professor</li><li class=\"affil\">St. Luke''s University / Temple University</li>";
        //         $photoid         = 1317;

        //     } elseif ($reviewer == "Anupama Brixey") {
        //         $facultyid       = 1074;
        //         $listaffiliation = "<li class=\"affil\">Clinical Fellow in Cardiothoracic Imaging</li><li class=\"affil\">Oregon Health and Science University</li>";
        //         $photoid         = 1324;

        //     }

        //     // jobfaculty CME
        //     $jobfaculty_query = "INSERT into jobfaculty (jobnum, facultyid, disclosure, role, rank, inits, photoid, listaffiliation) VALUES ('" . $jobnum . "', " . $facultyid . ", 'has no relevant financial relationships to disclose.', 'Faculty Reviewer', 4, 'DW', " . $photoid . ",'" . $listaffiliation . "');";

        //     $output['jobfaculty']['query'] = $jobfaculty_query;

        //     $result = pg_query($pik_db, $jobfaculty_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobfaculty']['status'] = $reviewer . " (" . $facultyid . ") jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobfaculty']['status'] = pg_last_error($pik_db);
        //     }

        // } else {

        //     $output['jobfaculty']['query'] = $jobfaculty_existing_q;

        //     $jobfaculty_existing_q     = "SELECT * from jobfaculty where jobnum = '" . $jobnum . "';";
        //     $jobfaculty_existing_q_res = pg_query($pik_db, $jobfaculty_existing_q);
        //     $jobfaculty                = pg_fetch_assoc($jobfaculty_existing_q_res);

        //     $facultyid = $jobfaculty['facultyid'];
        //     if ($status == 1) {
        //         $output['jobfaculty']['status'] = "jobnum = " . $jobnum . ", " . $reviewer . " facultyid = " . $facultyid;
        //     } else {
        //         $output['jobfaculty']['status'] = pg_last_error($pik_db);
        //     }

        // }

        // // check jobwriters

        // $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $jobnum . "';";
        // $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
        // $jobwriters_existing_rows  = pg_num_rows($jobwriters_existing_q_res);

        // $authorbyline_explode = explode(",", $authorbyline);
        // $actual_author        = trim($authorbyline_explode[0]);

        // if ($jobwriters_existing_rows == 0) {

        //     if ($actual_author == "Alison Palkhivala") {
        //         $writerid = 564;
        //     } elseif ($actual_author == "Candace Hoffmann") {
        //         $writerid = 557;
        //     } elseif ($actual_author == "Edward Susman") {
        //         $writerid = 559;
        //     } elseif ($actual_author == "Katherine Wandersee") {
        //         $writerid = 384;
        //     } elseif ($actual_author == "Michael Bassett") {
        //         $writerid = 552;
        //     } elseif ($actual_author == "Michael Smith") {
        //         $writerid = 548;
        //     } elseif ($actual_author == "Paul Smyth, MD") {
        //         $writerid = 562;
        //     } elseif ($actual_author == "Paul Smyth") {
        //         $writerid = 562;
        //     } elseif ($actual_author == "Peggy Peck") {
        //         $writerid = 554;
        //     } elseif ($actual_author == "Salynn Boyles") {
        //         $writerid = 563;
        //     } elseif ($actual_author == "Samuel Kailes") {
        //         $writerid = 558;
        //     } elseif ($actual_author == "Scott Baltic") {
        //         $writerid = 560;
        //     } elseif ($actual_author == "Shalmali Pal") {
        //         $writerid = 561;
        //     } elseif ($actual_author == "E.C. Meszaros") {
        //         $writerid = 565;
        //     } elseif ($actual_author == "Pam Harrison") {
        //         $writerid = 566;
        //     } elseif ($actual_author == "Liz Meszaros") {
        //         $writerid = 567;
        //     }

        //     // jobwriters CME
        //     $jobwriters_query = "INSERT into jobwriters (jobnum, writerid, disclosure) VALUES ('" . $jobnum . "', " . $writerid . ", ' has no relevant financial relationships to disclose.');";

        //     $output['jobwriters']['query'] = $jobwriters_query;

        //     $result = pg_query($pik_db, $jobwriters_query);
        //     $status = pg_result_status($result);
        //     if ($status == 1) {
        //         $output['jobwriters']['status'] = $actual_author . " (" . $writerid . ") jobnum = " . $jobnum . " INSERTED";
        //     } else {
        //         $output['jobwriters']['status'] = pg_last_error($pik_db);
        //     }

        // } else {

        //     $output['jobwriters']['query'] = $jobwriters_existing_q;

        //     $jobwriters_existing_q     = "SELECT * from jobwriters where jobnum = '" . $jobnum . "';";
        //     $jobwriters_existing_q_res = pg_query($pik_db, $jobwriters_existing_q);
        //     $jobwriters                = pg_fetch_assoc($jobwriters_existing_q_res);

        //     $writerid = $jobwriters['writerid'];
        //     if ($status == 1) {
        //         $output['jobwriters']['status'] = "jobnum = " . $jobnum . ", " . $actual_author . " writerid = " . $writerid;
        //     } else {
        //         $output['jobwriters']['status'] = pg_last_error($pik_db);
        //     }

        // }

        // // job_initiatives
        // // select * from job_initiatives where jobnum = '2410'

        // //  job_strategies dependency for eval q's
        // //  need to find eval qs that have empty string for strategies, update with what's in POST JSON

        // if ($GET["autolayout"] == 1 and !empty($POST)) {
        //     $output["autolayout"] = 'yes';

        //     // Get subchapter tocid by jn
        //     $lmt_toc_q     = "select * from lmt_toc where parentid = '" . $tocid . "' and toc_type = 'page' and status = 'active'";
        //     $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
        //     $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);

        //     if (pg_num_rows($lmt_toc_q_res) != 0) {
        //         $output["autolayout"] = "rows exist";
        //         //  write output JSON using existing layout
        //     } else {
        //         $output["autolayout"] = "no rows";
        //         //  write output JSON using preset delimiters
        //         // option by target number pages?
        //         // option for
        //     }

        // } else {
        //     $output["autolayout"] = 'no autolayout';
        // }

        $content_location = '/var/www/s1/moonlight/content-stage/';

        $output = array();

        $jobnum       = $POST["jobnum"];
        $parentjobnum = substr($jobnum, 0, 4);

        $subdomain = 'news';

        $joblist_q = "SELECT jobnum
                    ,title as activity_title
                    ,accmecred as credit_text
                    ,acpecred as ce_credit
                    ,aacncred as cpe_credit
                    ,reldate as releasedate
                    ,termdate
                    ,targaud as target_audience
                    ,actgoal as activity_goal
                    ,is_moc
                    ,moc_credit_type
                    ,upn
                    ,abim_specialty
                    FROM joblist
                    WHERE jobnum = $1";
        $joblist_res = pg_query_params($pik_db, $joblist_q, array($parentjobnum));
        $joblist     = pg_fetch_assoc($joblist_res);

        $jobfunders_q = "SELECT thirdcolblurb as funder_line
                    FROM jobfunders
                    WHERE jobnum = $1";
        $jobfunders_res = pg_query_params($pik_db, $jobfunders_q, array($parentjobnum));
        $row_funder     = pg_fetch_assoc($jobfunders_res);
        $funder_line    = urldecode($row_funder["funder_line"]);

        $subjob["surNum"]                             = ""; // deprecated
        $subjob["subNum"]                             = ""; // deprecated
        $subjob["testjobnum"]                         = $jobnum;
        $subjob["title"]                              = trim($POST["hed"]);
        $subjob["cme"]["funderLogos"]["peerReviewed"] = array();

        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["link"]   = "https://www.projectsinknowledge.com";
        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgsrc"] = "https://cdn.atpoc.com/cdn/DESIGN/breakingmed/logos/PIK.gif";
        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgAlt"] = "Projects In Knowledge&reg;";

        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["link"]   = "https://www.projectsinknowledge.com";
        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgsrc"] = "https://cdn.atpoc.com/cdn/DESIGN/breakingmed/logos/TIK.gif";
        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgAlt"] = "Peer Reviewed";

        $subjob["cme"]["funderLogos"]["tieredFunders"] = $funder_line;

        $subjob["cme"]["faculty"] = array();

        $subjob["cme"]["faculty"][0]["role"]            = "Faculty Reviewer";
        $subjob["cme"]["faculty"][0]["facultyImage"]    = "https://cdn.atpoc.com/cdn/activity/breakingmed/image/" . $POST["reviewer"]["id"] . ".jpg"; // NEED FACULTYID Mapping
        $subjob["cme"]["faculty"][0]["fullName"]        = $POST["reviewer_bylines"][0]["name"];
        $subjob["cme"]["faculty"][0]["credentials"]     = null;
        $subjob["cme"]["faculty"][0]["jobAffiliations"] = $POST["reviewer_bylines"][0]["title"] . "<br>" . $POST["reviewer_bylines"][0]["organization"];

        $faculty_disclosure = "<p><strong>" . $POST["reviewer_bylines"][0]["name"] . ",</strong> has no relevant financial relationships to disclose.</p>";

        $writer_disclosure = "<p><strong>" . $POST["bylines"][0]["name"] . ",</strong> has no relevant financial relationships to disclose.</p>";

        $releasedate = $POST["releasedate"];
        $termdate    = date_create($releasedate);
        date_modify($termdate, '+1 year');
        date_modify($termdate, '-1 day');
        $termdate = date_format($termdate, 'Y-m-d');

        $subjob["cme"]["estimatedTimeToComplete"]                           = array();
        $subjob["cme"]["estimatedTimeToComplete"][0]["creditType"]          = ""; // deprecated
        $subjob["cme"]["estimatedTimeToComplete"][0]["availFor"]            = ($POST["moc"]) == '1' ? "CME / CE / ABIM MOC" : "CME / CE";
        $subjob["cme"]["estimatedTimeToComplete"][0]["cme_publishDate"]     = date('M d, Y', strtotime($releasedate));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cme_terminationDate"] = date('M d, Y', strtotime($termdate));
        $subjob["cme"]["estimatedTimeToComplete"][0]["ce_publishDate"]      = date('M d, Y', strtotime($releasedate));
        $subjob["cme"]["estimatedTimeToComplete"][0]["ce_terminationDate"]  = date('M d, Y', strtotime($termdate));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_publishDate"]     = date('M d, Y', strtotime($releasedate));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_terminationDate"] = date('M d, Y', strtotime($termdate));

        $cme_credit = $POST["cme_credit"];

        $subjob["cme"]["credits"] = array(); //correlates to  'estimated time for completion'
        $cme_minutes              = floatval($cme_credit) * 60;

        $subjob["cme"]["credits"][0]["creditType"]   = "CME for Physicians";
        $subjob["cme"]["credits"][0]["actualCredit"] = $cme_minutes . " minutes";
        $subjob["cme"]["credits"][1]["creditType"]   = "CME for Physician Assistants";
        $subjob["cme"]["credits"][1]["actualCredit"] = $cme_minutes . " minutes";
        $subjob["cme"]["credits"][2]["creditType"]   = "CNE for Nurses";
        $subjob["cme"]["credits"][2]["actualCredit"] = $cme_minutes . " minutes";
        $subjob["cme"]["credits"][3]["creditType"]   = "CPE for Pharmacists";
        $subjob["cme"]["credits"][3]["actualCredit"] = $cme_minutes . " minutes";

        $subjob["cme"]["abim_credit"] = ($POST["moc"] == '1') ? "<em>You are Eligible for AMA PRA Category 1 Credit(s)<sup>TM</sup> ABIM MOC points</em>" : null;

        $subjob["cme"]["credits_available"] = array(); // new from AAPA spec

        $credit_text = strval(number_format(floatval($cme_credit), 2));

        $subjob["cme"]["credits_available"][0]["creditType"]   = "Physicians";
        $subjob["cme"]["credits_available"][0]["actualCredit"] = $credit_text;
        $subjob["cme"]["credits_available"][1]["creditType"]   = "ABIM MOC";
        $subjob["cme"]["credits_available"][1]["actualCredit"] = ($POST["moc"] == '1') ? $credit_text : null;
        $subjob["cme"]["credits_available"][2]["creditType"]   = "Physician Assistants";
        $subjob["cme"]["credits_available"][2]["actualCredit"] = $credit_text;
        $subjob["cme"]["credits_available"][3]["creditType"]   = "Nurses";
        $subjob["cme"]["credits_available"][3]["actualCredit"] = $credit_text;
        $subjob["cme"]["credits_available"][4]["creditType"]   = "Pharmacists";
        $subjob["cme"]["credits_available"][4]["actualCredit"] = $credit_text;
        $subjob["cme"]["credits_available"][5]["creditType"]   = "IPCE";
        $subjob["cme"]["credits_available"][5]["actualCredit"] = $credit_text;

        // IPE hotfix
        $subjob["cme"]["cmeInstructions"]                       = array();
        $subjob["cme"]["cmeInstructions"][0]["title"]           = "Interprofessional Education Committee";
        $subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = "<p>Interprofessional Education (IPE) requires collaboration among and between professions in order to enhance skills and strategies for effective healthcare team practices including communication and coordination of care. In guiding the overall CE Program, the role of the IPE Committee is to improve interprofessional collaborative practice by helping to recognize the different roles, responsibilities, and expertise of other healthcare professionals to develop team strategies that improve outcomes.</p><p>The IPE Committee members include:</p><p><strong>Vandana G. Abramson, MD</strong><br />Associate Professor of Medicine<br />Vanderbilt University Medical Center<br />Nashville, Tennessee</p><p><strong>James D. Bowen, MD</strong><br />Medical Director, Multiple Sclerosis Center<br />Swedish Neuroscience Institute<br />Seattle, Washington</p><p><strong>Veronica Esquible, PharmD</strong><br />Clinical Lead Pharmacist<br />Department of Pharmacotherapy<br />Swedish Medical Center<br />Seattle, Washington</p><p><strong>Bradley J. Monk, MD, FACS, FACOG</strong><br />Arizona Oncology (US Oncology Network)<br />Professor, Gynecologic Oncology<br />University of Arizona and Creighton University<br />Medical Director of US Oncology Research Gynecology Program<br />Phoenix, Arizona</p><p><strong>Brant Oliver, PhD, MS, MPH, APRN-BC</strong><br />Healthcare Improvement Scientist<br />Associate Professor<br />The Dartmouth Institute &amp; Geisel School of Medicine at Dartmouth<br />Hanover, New Hampshire</p><p><strong>Kendall Shultes, PharmD</strong><br />Clinical Pharmacist/Assistant Professor<br />Vanderbilt/Belmont University<br />Nashville, Tennessee</p><p><strong>Katherine Sibler, MSN, WHNP-BC</strong><br />Nurse Practitioner<br />Vanderbilt Breast Center<br />Nashville, Tennessee</p>";
        $subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = "";

        $subjob["cme"]["cmeInstructions"][1]["title"]           = 'CME/CE Instructions';
        $subjob["cme"]["cmeInstructions"][1]["cmeInstructions"] = '<p class="body12arial"><br>To obtain CME/CE credit:</p><ol id="to_obtain_credit_list"><li class="learning_obj">Read or listen to each activity carefully.</li><li class="learning_obj">Complete/submit each posttest and evaluation.</li><li class="learning_obj">A record of your participation will be available in the CME Tracker area of this application and the certificate will be available on our website.</li></ol>';
        $subjob["cme"]["cmeInstructions"][1]["noFeeDisclaimer"] = "There is no fee for this activity.";

        $subjob["cme"]["contactInfo"]    = '<h4>Contact Information</h4><p>For questions about content or obtaining CME credit, please <a href="https://www.projectsinknowledge.com/contact_us/contact_us.cfm">contact us</a>.</p>';
        $subjob["cme"]["targetAudience"] = $joblist["targaud"]; // need to get from parent in joblist
        $subjob["cme"]["activityGoal"]   = $joblist["actgoal"]; // need to get from parent in joblist

        $subjob["cme"]["learningObjectives"] = array();

        $lo_cnt = 0;
        foreach ($POST["learning_objectives"] as $lo) {
            $subjob["cme"]["learningObjectives"][$lo_cnt]["objective"] = strip_tags($lo);
            $lo_cnt++;
        }

        $subjob["cme"]["cmeInfo"] = array();

        $ja_statement = '<p><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ja.png" width="120" border="0">In support of improving patient care, Projects In Knowledge<sup>&reg;</sup> is jointly accredited by the Accreditation Council for Continuing Medical Education (ACCME), the Accreditation Council for Pharmacy Education (ACPE), and the American Nurses Credentialing Center (ANCC), to provide continuing education for the healthcare team.</p><br><p style="clear:both"><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ipce.png" width="120" border="0">This activity was planned by and for the healthcare team, and learners will receive ' . $credit_text . ' Interprofessional Continuing Education (IPCE) credit for learning and change.</p><br><br>';

        $cme_statement = ($POST["moc"] == '1') ? "<p>Projects In Knowledge<sup>®</sup> designates this enduring material for a maximum of " . $credit_text . " <i>AMA PRA Category 1 Credit(s)</i><sup>&trade;</sup>. Physicians should claim only the credit commensurate with the extent of their participation in the activity.</p><p>Successful completion of this CME activity, which includes participation in the evaluation component, enables the participant to earn up to " . $credit_text . " Medical Knowledge MOC points in the American Board of Internal Medicine&#8217;s (ABIM) Maintenance of Certification (MOC) program. Participants will earn MOC points equivalent to the amount of CME credits claimed for the activity. It is the CME activity provider&#8217;s responsibility to submit participant completion information to ACCME for the purpose of granting ABIM MOC credit.</p>" : "<p class=\"body12arial\">Projects In Knowledge<sup>®</sup> designates this enduring material for a maximum of " . $credit_text . " <i>AMA PRA Category 1 Credit(s)</i><sup>&trade;</sup>. Physicians should claim only the credit commensurate with the extent of their participation in the activity.</p>";

        $aapa_statement = '<p>Projects In Knowledge<sup>&#174;</sup> has been authorized by the American Academy of PAs (AAPA) to award AAPA Category 1 CME credit for activities planned in accordance with AAPA CME Criteria. This activity is designated for ' . $credit_text . ' <em>AAPA Category 1 Credit(s)</em><sup>&#8482;</sup>. Approval is valid until ' . date('m/d/Y', strtotime($termdate)) . '. PAs should only claim credit commensurate with the extent of their participation.</p>';

        $ce_statement = "<p>Upon completion of this course, participants will be awarded " . $credit_text . " nursing contact hour(s).</p><p>Joint Accreditation Nursing Provider Number: 4008235</p><p style=\"font-size:smaller;\">DISCLAIMER: Accreditation refers to educational content only and does not imply ANCC, or PIK endorsement of any commercial product or service.</p>";

        $cpe_statement = "<p>This program has been planned and implemented in accordance with the ACPE Criteria for Quality and Interpretive Guidelines. This activity is worth up to " . $credit_text . " contact hour(s)(" . strval(number_format(floatval($cme_credit) * 0.1, 2)) . " CEU). The ACPE Universal Activity Number assigned to this Knowledge-type activity is " . $POST["acpe_uan"] . ".</p><p>Pharmacists should only claim credit commensurate with the extent of their participation in the activity.</p>";

        $subjob["cme"]["cmeInfo"][0]["title"]                    = "Joint Accreditation Statement";
        $subjob["cme"]["cmeInfo"][0]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][0]["creditDesignation"]        = $ja_statement;

        $subjob["cme"]["cmeInfo"][1]["title"]                    = "Physicians";
        $subjob["cme"]["cmeInfo"][1]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][1]["creditDesignation"]        = $cme_statement;

        $subjob["cme"]["cmeInfo"][2]["title"]                    = "Physician Assistants";
        $subjob["cme"]["cmeInfo"][2]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][2]["creditDesignation"]        = $aapa_statement;

        $subjob["cme"]["cmeInfo"][3]["title"]                    = "Nurses";
        $subjob["cme"]["cmeInfo"][3]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][3]["creditDesignation"]        = $ce_statement;

        $subjob["cme"]["cmeInfo"][4]["title"]                    = "Pharmacists";
        $subjob["cme"]["cmeInfo"][4]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][4]["creditDesignation"]        = $cpe_statement;

        $subjob["cme"]["disclosureInformation"] = "<p>Projects In Knowledge<sup>&reg;</sup> adheres to the ACCME's <em>Standards for Integrity and Independence in Accredited Continuing Education</em>. Any individuals in a position to control the content of a CE activity, including faculty, planners, reviewers or others are required to disclose all relevant financial relationships with ineligible entities (commercial interests)*. All relevant conflicts of interest have been mitigated prior to the commencement of the activity.</p>" . $faculty_disclosure . $writer_disclosure . "<p><strong>Peer Reviewer</strong> has no relevant financial relationships to disclose.</p><p><strong>Vandana G. Abramson, MD</strong> (IPE Committee) has no relevant financial relationships to disclose.</p><p><strong>James D. Bowen, MD</strong> (IPE Committee), has received honoraria from Biogen, EMD Serono, Inc., Genentech, and Novartis; has received consulting fees from Biogen, Celgene, EMD Serono, Genentech, a Member of the Roche Group, and Novartis Pharmaceuticals Corporation; has conducted contracted research for Alexion Pharmaceuticals, Inc, Alkermes, Biogen, Celgene Corporation, Genentech, a Member of the Roche Group, Genzyme, Novartis Pharmaceuticals Corporation, and TG Therapeutics, Inc; and has ownership interest in Amgen Inc.</p><p><strong>Veronica Esquible, PharmD</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p><strong>Bradley Monk, MD, FACOG, FACS</strong> (IPE Committee), has received consulting fees from AbbVie, Inc, Advaxis, Inc., Agenus Inc., Amgen Inc., Aravive, Inc., Asymmetric Therapeutics, Boston Biomedical, Chemocare, ChemoID, Circulogene, Conjupro Biotherapeutics Inc, Eisai Co., Ltd, Geistlich Pharma North America, Inc., Genmab/Seattle Genetics, GOG Foundation, Immunomedics, Inc., ImmunoGen, Inc., Incyte Corporation, Laekna Healthcare Private Limited, Mateon Therapeutics, Merck Sharp &amp; Dohme Corp., a subsidiary of Merck &amp; Co., Inc, Mersana Therapeutics, Inc., Myriad Genetics, Nucana, OncoMed Pharmaceuticals, OncoQuest Inc., OncoSec Medical Inc, Perthera Inc., Pfizer Inc., Precision Oncology, Puma Biotechnology Inc, Regeneron Pharmaceuticals, Samumed, LLC, Takeda Pharmaceutical Company, Ltd, VBL Therapeutics, and Vigeo Therapeutics; and has been a Speaker/Consultant for AstraZeneca, Clovis Oncology, F. Hoffmann-La Roche Ltd/ Genentech, A Member of the Roche Group, Janssen Pharmaceuticals/Johnson &amp; Johnson, Inc., and Tesaro, Inc/GlaxoSmithKline.</p><p><strong>Brant Oliver, PhD, MS, MPH, APRN-BC</strong> (IPE Committee), has conducted contracted research for Biogen and EMD Serono.</p><p><strong>Kendall Shultes, PharmD</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p><strong>Katherine Sibler, MSN, WHNP-BC</strong> (IPE Committee), has no relevant financial relationships to disclose.</p><p>Projects In Knowledge staff members have no relevant financial relationships to disclose.</p><p>Conflicts of interest are thoroughly vetted by the Executive Committee of Projects In Knowledge. All conflicts are resolved prior to the beginning of the activity by the Trust In Knowledge peer review process.</p><p>The opinions expressed in this activity are those of the faculty and do not necessarily reflect those of Projects In Knowledge.</p><p>*An ineligible company (commercial interest) is any entity producing, marketing, re-selling, or distributing health care goods or services consumed by, or used, on patients. </p>";

        $subjob["cme"]["facultyDisclosures"]                  = array();
        $subjob["cme"]["facultyDisclosures"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["facultyDisclosures"][0]["disclosure"] = ""; // deprecated
        $subjob["cme"]["facultyDisclosures"][0]["role"]       = ""; // deprecated

        $subjob["cme"]["medicalWriters"]                  = array();
        $subjob["cme"]["medicalWriters"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["medicalWriters"][0]["disclosure"] = ""; // deprecated

        $subjob["cme"]["noPeerReviewerDisclosures"] = ""; // deprecated

        $subjob["cme"]["ceDisclosures"]                  = array();
        $subjob["cme"]["ceDisclosures"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["ceDisclosures"][0]["disclosure"] = ""; // deprecated

        $subjob["cme"]["cmeAccreditorProvider"] = ""; // deprecated
        $subjob["cme"]["PIKRelationship"]       = ""; // deprecated
        $subjob["cme"]["conflictsOfInterest"]   = ""; // deprecated

        $subjob["cme"]["finalBlurb"] = ""; // deprecated

        $subjob["cme"]["funderInfo"]                   = array();
        $subjob["cme"]["funderInfo"][0]["funderJN"]    = (float) $POST["jobnum"];
        $subjob["cme"]["funderInfo"][0]["fundertext"]  = $funder_line;
        $subjob["cme"]["funderInfo"][0]["fundertext2"] = $funder_line;
        $subjob["cme"]["funderInfo"][0]["funderHTML"]  = '<p id="fundertext">' . $funder_line . '</p>';

        $subjob["cme"]["mutualResponsibility"] = '<h4>CONTRACT FOR MUTUAL RESPONSIBILITY IN CME/CE</h4><p>Projects In Knowledge has developed the contract to demonstrate our commitment to providing the highest quality professional education to clinicians, and to help clinicians set educational goals to challenge and enhance their learning experience.</p><p>For more information on the contract, <a href="https://www.projectsinknowledge.com/mutual_responsibility.cfm" target="_blank">click here</a></p>';

        $subjob["cme"]["trademark"]       = "<p class=\"body11arial\">Projects In Knowledge<sup>&reg;</sup> is a registered trademark of Projects In Knowledge, Inc.</p>\n";
        $subjob["cme"]["is_moc"]          = $POST["moc"];
        $subjob["cme"]["moc_credit_type"] = $POST["moc_credit_type"];
        $subjob["cme"]["abim_specialty"]  = $POST["abim_specialty"];

        $subjob["cmeNum"] = (float) $POST["jobnum"];



        $subjob["pages"][0]["pageNum"]       = "0";
        $subjob["pages"][0]["tocTitle"]      = "stage";
        $subjob["pages"][0]["tocType"]       = "page";
        $subjob["pages"][0]["tocID"]         = "0";
        $subjob["pages"][0]["parentID"]      = "0";
        $subjob["pages"][0]["jobNum"]        = ""; // deprecated
        $subjob["pages"][0]["jobNumDot"]     = ""; // deprecated
        $subjob["pages"][0]["cola"]          = "";
        $subjob["pages"][0]["colb"]          = "";
        $subjob["pages"][0]["colc"]          = ""; // deprecated
        $subjob["pages"][0]["cold"]          = ""; // deprecated
        $subjob["pages"][0]["cole"]          = ""; // deprecated
        $subjob["pages"][0]["page_type"]     = "page";
        $subjob["pages"][0]["chapternum"]    = ""; // deprecated
        $subjob["pages"][0]["subchapternum"] = ""; // deprecated
        $subjob["pages"][0]["page_title"]    = "stage";
        $subjob["pages"][0]["pageid"]        = ""; // deprecated
        $subjob["pages"][0]["swipeleft"]     = ""; // deprecated
        $subjob["pages"][0]["swiperight"]    = ""; // deprecated
        $subjob["pages"][0]["swipedown"]     = ""; // deprecated
        $subjob["pages"][0]["swipeup"]       = ""; // deprecated
        $subjob["pages"][0]["sortorder"]     = 1;
        $subjob["pages"][0]["dlu"]           = ""; // deprecated
        $subjob["pages"][0]["datepublished"] = ""; // deprecated
        $subjob["pages"][0]["publishedby"]   = ""; // deprecated
        $subjob["pages"][0]["publish2ios"]   = ""; // deprecated
        $subjob["pages"][0]["showPosttest"]  = ""; // deprecated
        $subjob["pages"][0]["showPretest"]   = "no";
        $subjob["pages"][0]["showClaim"]     = ""; // deprecated
        $subjob["pages"][0]["bookid"]        = "0";
        $subjob["pages"][0]["portBack"]      = ""; // deprecated
        $subjob["pages"][0]["landBack"]      = ""; // deprecated
        $subjob["pages"][0]["testjobnum"]    = $POST["job_number"];
        $subjob["pages"][0]["funderText"]    = $funder_line;
        $subjob["pages"][0]["layout"]        = ""; // deprecated
        $subjob["pages"][0]["template"]      = ""; // deprecated


        $subjob["pages"][1] = $subjob["pages"][0];
        $subjob["pages"][2] = $subjob["pages"][0];
        $subjob["pages"][3] = $subjob["pages"][0];



        // page 0

        // FAIR BALANCE STATEMENT

        $subdomain         = $lmt_toc["subdomain"];
        $fair_balance_stmt = '<div style="margin-bottom: var(--baseline)"><a href="https://suiteweb.atpointofcare.com/#library/' . $subdomain . '">For more BreakingMED news coverage from this collection, <em>click here</em></a></div>';

        $p0_cola = "<h1>" . $POST["hed"] . "</h1><h2>" . $POST["dek"] . "</h2><div class=\"byline\" style=\"margin-bottom: var(--baseline)\">By " . $POST["bylines"][0]["name"] . ", " . $POST["bylines"][0]["title"] . ", " . $POST["bylines"][0]["organization"] . "Reviewed by " . $POST["reviewer_bylines"][0]["name"] . ", " . $POST["reviewer_bylines"][0]["title"] . ", " . $POST["reviewer_bylines"][0]["organization"] . "</div>" . $fair_balance_stmt . "<!--CME/CE INFORMATION --><div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter('" . $jobnum . "');</script>";

        $p0_colb = "<div id=\"pretestFrame\"><script>pretestGetter('" . $jobnum . "','##UserToken##')</script></div>";


        $subjob["pages"][0]["cola"]          = $p0_cola;
        $subjob["pages"][0]["colb"]          = $p0_colb;


        // page 1

        $p1_cola = "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Take Away:</div><p>";
        foreach ($POST["takeaways"] as $takeaways) {
            $takeaway = str_replace("<p>", "<span class=\"li\">", $takeaways);
            $takeaway = str_replace("</p>", "</span>", $takeaway);
            $p1_cola .= $takeaway ;
        }
        $p1_cola .= '</p></div></div><div style="text-align: center;"><img src="https://cdn.atpoc.com/cdn/activity/breakingmed/image/'.$POST["articleid"].'.jpeg" width="400px" /></div><div>&nbsp;</div>' . $fair_balance_stmt ;



        $subjob["pages"][1] = $subjob["pages"][0];
        $subjob["pages"][1]["cola"]          = $p1_cola;







        $subjob["pages"][1]["colb"]          = $p1_colb;


        $output = $subjob;

    } elseif ($method == "SELECT") {
        // print_r($GET);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        header("HTTP/1.1 201 Created");
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        header("HTTP/1.1 201 Created");
        $resJson = json_encode($output);
        echo $resJson;
        // echo "https://secureapi.atpoc.com/s1/moonlight/content/".$record_id;

    }

    apilog($debug, $profile);

}
