<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

//db connections
    $pik_db         = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db       = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);
    $channellive_db = pg_connect("host=" . $GLOBALS['channellive_server'] . " port=5432 dbname=" . $GLOBALS['channellive_database'] . " user=" . $GLOBALS['channellive_user'] . " password=" . $GLOBALS['channellive_password']);

//GET
    if ($method == "SELECT") {

        $articlemeta_q   = "SELECT * from tbl_breakingmed_articles where jobnum = $1 LIMIT 1";
        $articlemeta_res = pg_query_params($atpoc_db, $articlemeta_q, array($record_id));
        $articlemeta     = pg_fetch_assoc($articlemeta_res);

        $articleid = $articlemeta["articleid"];

        $test = "SELECT p.posttestid, q.qid, q.rank, q.qtext, q.ansexplanation
                            FROM tbl_breakingmed_posttests p
                            LEFT JOIN tbl_breakingmed_posttestqs q ON q.posttestid = p.posttestid AND q.isactive=1
                            WHERE p.articleid = $1 and p.isactive = 1
                            ORDER BY p.posttestid, q.qid, q.rank";
        $test_res = pg_query_params($atpoc_db, $test, array($articleid));

        $output .= "\n\n\n\n\n\n-----\nPretest / Posttest\n-----\n\n";

        if ($test_res) {
            while ($questions = pg_fetch_array($test_res)) {

                $qtext          = $questions['qtext'];
                $ansexplanation = $questions['ansexplanation'];
                $qid            = $questions['qid'];
                $rank           = $questions['rank'];

                $output .= "\nQ" . $rank . ") " . $qtext . "\n\t\n";

                $answers_q = "SELECT r.qid, r.ansrank, r.anstext, r.iscorrect
                                    FROM tbl_breakingmed_posttestans r
                                    WHERE r.qid = $1
                                    ORDER BY r.ansrank";

                $answers_res = pg_query_params($atpoc_db, $answers_q, array($qid));

                if ($answers_res) {

                    $tbl_evalans = "INSERT INTO tbl_evalans (qid,anstext,ansvalue,ansrank) VALUES ";

                    while ($answers = pg_fetch_array($answers_res)) {
                        $anstext   = $answers['anstext'];
                        $anstext   = str_replace("<br />", "", $anstext);
                        $iscorrect = $answers['iscorrect'];
                        if ($iscorrect == 1) {
                            $output .= "\t✓ " . $anstext . "\n";
                        } else {
                            $output .= "\tx " . $anstext . "\n";
                        }

                        $ansrank = $answers["ansrank"];
                        $tbl_evalans .= ($ansrank !== '1') ? ",((select qid from tbl_evalqs_ins), '" . $anstext . "', '', " . $answers["ansrank"] . ")" : "((select qid from tbl_evalqs_ins), '" . $anstext . "', '', " . $answers["ansrank"] . ")";

                        // $tbl_evalans .= "(QID, '".$anstext."', '', ".$answers["ansrank"]."), ";

                    }
                }

                if (strpos($tbl_evalans, "I do NOT know") == false) {

                    $tbl_evalans .= ",((select qid from tbl_evalqs_ins), 'I do NOT know', '', " . (intval($ansrank) + 1) . ")";

                }

                $output .= "\n\tAnswer Explanation:\n\t" . $ansexplanation . "\n";

                if (isset($GET["surveyjobnum"])) {
                    $survey_q       = "select * from tbl_evals where jobnum = $1 LIMIT 1";
                    $survey_res     = pg_query_params($pik_db, $survey_q, array($GET["surveyjobnum"]));
                    $survey         = pg_fetch_assoc($survey_res);
                    $survey_eval_id = $survey["evalid"];
                } else {
                    $survey_eval_id = (isset($GET["evalid"])) ? $GET["evalid"] : "SURVEY_EVAL_ID";
                }

                $output .= "\n\n--add to survey (set evalid or surveyjobnum in url param)\nWITH tbl_evalqs_ins as (INSERT INTO tbl_evalqs (evalid,qtext,qtype,rank,isactive,isrequired,qtypenum) VALUES (" . $survey_eval_id . ", '" . $qtext . "', 'multiple_radio', " . $rank . ", 1, 0, 40) returning *), tbl_evalans_ins as (" . $tbl_evalans . " returning *) SELECT * FROM tbl_evalans_ins ;\n\n\n";

                $actual_pre_q = "WITH actual_pretest as (SELECT tbl_pretests.jobnum, tbl_pretests.pretestid, tbl_pretestqs.qid, max(tbl_pretestans.ansrank) as ansrank, tbl_pretestqs.qtext FROM tbl_pretests JOIN tbl_pretestqs on tbl_pretests.pretestid = tbl_pretestqs.pretestid JOIN tbl_pretestans on tbl_pretestqs.qid = tbl_pretestans.qid WHERE tbl_pretests.jobnum = $1 and tbl_pretestqs.rank = $2 and tbl_pretestqs.qtypenum = 40 GROUP by tbl_pretests.jobnum, tbl_pretests.pretestid, tbl_pretestqs.qid, tbl_pretestqs.qtext), last_answer_pretest as (SELECT qid, ansid, anstext from tbl_pretestans WHERE (qid, ansrank) IN (SELECT qid, ansrank from actual_pretest)) SELECT actual_pretest.*, last_answer_pretest.ansid, last_answer_pretest.anstext from actual_pretest join last_answer_pretest on actual_pretest.qid = last_answer_pretest.qid";

                $actual_pre_res = pg_query_params($pik_db, $actual_pre_q, array($record_id, $rank));

                if ($actual_pre_res) {
                    while ($actual_pre = pg_fetch_assoc($actual_pre_res)) {
                        if (strpos($actual_pre["anstext"], "I do NOT know") === false) {

                            // var_dump($actual_pre,strpos($actual_pre["anstext"], "I do NOT know"));

                            $output .= "-- add 'I do NOT know'\nINSERT INTO tbl_pretestans (qid,anstext,ansvalue,ansrank,iscorrect) VALUES (" . $actual_pre["qid"] . ", 'I do NOT know', NULL, " . (intval($actual_pre["ansrank"]) + 1) . ", '0');\nSELECT pg_sleep(.25);\nINSERT INTO tbl_pretestreport (pretestid,qid,qtext,ansid,anstext) VALUES (" . $actual_pre["pretestid"] . ", " . $actual_pre["qid"] . ", '" . $actual_pre["qtext"] . "', (select ansid from tbl_pretestans where qid = " . $actual_pre["qid"] . " order by ansid desc limit 1), 'I do NOT know');\n\n\n\n\n";

                            $hotfix .= "-- add 'I do NOT know'\nINSERT INTO tbl_pretestans (qid,anstext,ansvalue,ansrank,iscorrect) VALUES (" . $actual_pre["qid"] . ", 'I do NOT know', NULL, " . (intval($actual_pre["ansrank"]) + 1) . ", '0');\n";

                        } else {

                            $output .= "-- 'I do NOT know' ALREADY EXISTS for qid " . $actual_pre["qid"] . "\nSELECT * from tbl_pretestans where qid = " . $actual_pre["qid"] . "\n\n\n\n\n";

                        }
                    }

                }

            }
        } else {
            $output .= "\n\n\nerror!";
        }

        $output .= "\nS) How often do you currently apply the following clinical practice strategies?\n\n";

        $strategies_q   = "SELECT * FROM tbl_breakingmed_strategies WHERE jobnum = $1 and isactive = 1 order by sortorder";
        $strategies_res = pg_query_params($atpoc_db, $strategies_q, array($record_id));

        if ($strategies_res) {
            while ($strategies = pg_fetch_assoc($strategies_res)) {
                $strategy = $strategies['strategy'];
                $strategy = str_replace("<p>", "", $strategy);
                $strategy = str_replace("</p>", "", $strategy);
                $output .= "\t∙ " . $strategy . "\n";
            }
        }

        $hotfix .= "\n\n\n\n";

        $jobstrategies_q   = "SELECT * FROM jobstrategies WHERE jobnum = $1 order by id";
        $jobstrategies_res = pg_query_params($pik_db, $jobstrategies_q, array($record_id));
        while ($jobstrategies = pg_fetch_assoc($jobstrategies_res)) {
            $id        = $jobstrategies["id"];
            $strattext = $jobstrategies["strattext"];
            $hotfix .= "UPDATE tbl_pretestsubqs set subqtext = (SELECT strattext from jobstrategies where id = " . $id . "), dm = now() where externalid = " . $id . ";\n";
            $hotfix .= "UPDATE tbl_evalsubqs set subqtext = (SELECT strattext from jobstrategies where id = " . $id . "), dm = now() where externalid = " . $id . ";\n\n\n";
        }

    } elseif ($method == "INSERT") {
        // print_r($POST);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        header("HTTP/1.1 201 Created");
        $a2  = $debug;
        $res = array_merge_recursive($output, $a2);
        var_dump($res);
        // $resJson = json_encode($res);
        // echo $resJson;
    } else {
        // header("HTTP/1.1 201 Created");

        echo ($hotfix);
        echo ("\n\n\n\n\n\n");
        echo ($output);
        // $resJson = json_encode($output);
        // echo $resJson;
        // echo "https://secureapi.atpoc.com/s1/moonlight/content/".$record_id;

    }

    apilog($debug, $profile);

}
