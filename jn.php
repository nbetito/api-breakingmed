<?php

function api($service_id,$method,$listmode,$record_id,$token,$GET,$POST,$PUT,$debug,$debugtoggle)
{


//db connections
$pik_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=pik user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
$atpoc_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=atpoc user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);

//GET
if ( $method == "SELECT")
{	//print_r($GET);

	if (isset($GET['articleid'])) {
		
		$articleid = $GET['articleid'] ;

		if (!$atpoc_db) {
			die('Could not connect to the database' . pg_last_Error($atpoc_db));
		}


		$content_url = 'https://api.atpoc.com/feed-breakingmed/beta/articles/'. $articleid ;
		$content_json = file_get_contents($content_url);
		$content = json_decode($content_json, true);

		$hed = $content["hed"];
		$dek = $content["dek"];
		$authorbyline = $content["authorbyline"];
		$reviewerbyline = $content["reviewerbyline"];
		$jobnum = $content["jobnum"];

		echo "<h1>".$hed."</h1>\r\n<h2>".$dek."</h2>\r\n\r\n<p class=\"byline\">\r\nBy ".$authorbyline."<br />\r\nReviewed by ".$reviewerbyline."</p>\r\n\r\n<!--CME/CE INFORMATION --><div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter('".$jobnum."');</script>";
		echo "\r\n\r\n\r\n----------------\r\n\r\n\r\n";


		$takeaways = $content["takeaways"];
		$takeaways = str_replace("<ol>", "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Take Away:</div><p>", $takeaways);
		$takeaways = str_replace("<li><p>", "<span class=\"li\">", $takeaways);
		$takeaways = str_replace("</p></li>", "</span>", $takeaways);
		$takeaways = str_replace("</ol>", "</p></div></div>", $takeaways);

		echo $takeaways;
		echo "\r\n\r\n\r\n----------------\r\n\r\n\r\n";

		$body = $content["body"];
		$body = str_replace("</p>", "</p>\r\n\r\n", $body);
		// &#39;
		$body = str_replace("&#8217;", "&#39;", $body);

		echo $body;

		$sourcedisclosures = $content["sourcedisclosures"];
		echo "\r\n\r\n<h4>Disclosure:</h4>".$sourcedisclosures;

		$sources = $content["sources"];
		echo "\r\n\r\n<h4>Sources:</h4>".$sources;


	}
	else {
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Please supply jn=" ;	
	}	
}


elseif ( $method == "INSERT")
{
	// print_r($POST);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
       
}
elseif ( $method == "UPDATE")
{
	// print_r($PUT);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";	
       
}

elseif ($method == "DELETE")
{
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
}



}
?>
