<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle, $premethod, $requestjson)
{

    // Make database connection
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);


    // 
    // LIMIT of results
    // 
    if (isset($GET['limit'])){
        $limit = $GET['limit'];
    } else {
        $limit = 20;
    }


    if ($premethod == "GET") {

        //
        // get categories by article
        //

        $articles_q     = "SELECT cattopicid from tbl_breakingmed_articles where articleid = ".$record_id ;
        $articles_res = pg_query($atpoc_db, $articles_q);
        $articles = pg_fetch_assoc($articles_res);
    
        $cattopicid = $articles["cattopicid"] ;

        // var_dump($cattopicid);

        // 
        // get verticals by categories
        // 

        $target_num_q    =  "SELECT DISTINCT(target_num) from tbl_breakingmed_categories_ww_target where categoryid in (".$cattopicid.") ;";
        $target_num_res = pg_query($atpoc_db, $target_num_q);

        $target_num = "";
        $cntt = 0;

        while ($row_jt = pg_fetch_assoc($target_num_res)){
            $cntt++;
            $target_num = $target_num . "'" . $row_jt["target_num"] . "'" ;
            if($cntt < pg_num_rows($target_num_res)) {$target_num = $target_num . ",";}
        }

        // var_dump($target_num);

        // 
        // get tas by verticals
        // 

        $ta_q    =  "SELECT DISTINCT(ta_num) from ww_target_ta where target_num in (".$target_num.") and archived = false ORDER by ta_num";
        $ta_res = pg_query($atpoc_db, $ta_q);

        $ta = "";
        $cntp = 0;

        while ($row_jp = pg_fetch_assoc($ta_res)){
            $cntp++;
            $ta = $ta . "'" . $row_jp["ta_num"] . "'" ;
            if($cntp < pg_num_rows($ta_res)) {$ta = $ta . ",";}
        }


        //
        // get jobnum by bookid
        //

        $lmt_toc     = "SELECT jobnum FROM lmt_toc WHERE status = 'active' and toc_type = 'subchapter' and bookid in (".$ta.")";
        $lmt_toc_res = pg_query($atpoc_db, $lmt_toc);

        $job_list = "";
        $cntj     = 0;

        while ($row_j = pg_fetch_assoc($lmt_toc_res)) {
            $cntj++;
            $job_list = $job_list . "'" . $row_j["jobnum"] . "'";
            if ($cntj < pg_num_rows($lmt_toc_res)) {$job_list = $job_list . ",";}
        }

        


        // 
        // get active CME only
        // 

        $termdate    =  "SELECT termdate, jobnum from joblist where jobnum in (".$job_list.") and termdate > now()";
        $termdate_res = pg_query($pik_db, $termdate);

        $job_list_valid = "";
        $cntv = 0;

        while ($row_jv = pg_fetch_assoc($termdate_res)){
            $cntv++;
            $job_list_valid = $job_list_valid . "'" . $row_jv["jobnum"] . "'" ;
            if($cntv < pg_num_rows($termdate_res)) {$job_list_valid = $job_list_valid . ",";}
        }




        // 
        // get !DNP only
        // 

        $dnp    =  "SELECT jobnum from cme_goals_subjob where jobnum in (".$job_list_valid.") and dnp = false";
        $dnp_res = pg_query($atpoc_db, $dnp);

        $job_list_promo = "";
        $cntp = 0;

        while ($row_jp = pg_fetch_assoc($dnp_res)){
            $cntp++;
            $job_list_promo = $job_list_promo . "'" . $row_jp["jobnum"] . "'" ;
            if($cntp < pg_num_rows($dnp_res)) {$job_list_promo = $job_list_promo . ",";}
        }


        // die(var_dump($job_list,$job_list_valid,$job_list_promo));


        // 
        // get metadata
        // 
        // move dnr to where
        // 

        $related_interest = array();

        if (!empty(job_list_promo)){

            $pacing_q = "SELECT t1.jobnum
                        , t1.subdomain as subdomain
                        , t1.activity_title, t1.reldate 
                        , date_part('day', now() - t1.reldate::date) as days_released
                        , t1.actual_participation::float / t1.subjob_goal as percent_of_goal
                        , t1.type
                        FROM cme_goals_subjob t1
                        WHERE jobnum in (". $job_list_promo .") and dnr = FALSE
                        ORDER by rescue desc, percent_of_goal
                        LIMIT ".$limit." ;" ;

            $pacing_q_res = pg_query($atpoc_db,$pacing_q);

            while ($metadata = pg_fetch_assoc($pacing_q_res)) {
                            $related_interest[] = $metadata;
                        }            

            $output['related_jobnum'] = $related_interest;
        } else {
            $output['error'] = "no jobnum to promote";
        }
        





        // 
        // 
        // DEBUG
        // 
        // 



        if ($GET[debug] == 1) {
                    $cats = array();

        if (!empty($cattopicid)){
            $categories = "SELECT categoryid, categorytitle, parentcategory FROM tbl_breakingmed_categories WHERE categoryid in (". $cattopicid .") ;" ;
            $categories_res = pg_query($atpoc_db,$categories);
            while ($metadata_cats = pg_fetch_assoc($categories_res)) {
                            $cats[] = $metadata_cats;
                        }            
            $output['related_categories'] = $cats;
        } else {
            $output['error'] = "no jobnum to promote";
        }
        

        $verts = array();

        if (!empty($target_num)){
            $verts_q = "SELECT target_num, target_name FROM ww_target_collection WHERE target_num in (". $target_num .") ;" ;
            $verts_res = pg_query($atpoc_db,$verts_q);
            while ($metadata_verts = pg_fetch_assoc($verts_res)) {
                            $verts[] = $metadata_verts;
                        }            
            $output['related_verticals'] = $verts;
        } else {
            $output['error'] = "no jobnum to promote";
        }



        $related_ta = array();

        if (!empty($ta)){
            $therapeutic_area = "SELECT ta_num, ta_name, subdomain FROM ww_therapeutic_area WHERE ta_num in (". $ta .") ;" ;
            $therapeutic_area_res = pg_query($atpoc_db,$therapeutic_area);
            while ($metadata = pg_fetch_assoc($therapeutic_area_res)) {
                            $related_ta[] = $metadata;
                        }            
            $output['related_therapeutic_areas'] = $related_ta;
        } else {
            $output['error'] = "no jobnum to promote";
        }

        $article_metadata = array();

        if (!empty($ta)){
            $meta_q = "SELECT hed, dek, articletype, releasedate, updated FROM tbl_breakingmed_articles WHERE articleid = ". $record_id ." ;" ;
            $meta_q_res = pg_query($atpoc_db,$meta_q);
            while ($metadata_bm = pg_fetch_assoc($meta_q_res)) {
                            $article_metadata[] = $metadata_bm;
                        }            
            $output['article_metadata'] = $article_metadata;
        } else {
            $output['error'] = "no jobnum to promote";
        }
                
        }






    } elseif ($premethod == "DELETE") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "PUT") {
        $output["error"] = "method not supported" ;
    } elseif ($premethod == "POST") {
        $output["error"] = "method not supported" ;
    }

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;
    }

}
