<?php

function api($service_id,$method,$listmode,$record_id,$token,$GET,$POST,$PUT,$debug,$debugtoggle)
{

#$content_location = '/var/www/s1/moonlight/content-blueline/';
$content_location = '/var/www/s1/moonlight/content-stage/';

$dbhost_webcms="169.54.235.132";
$dbuser_webcms="postgres";
$dbpasswd_webcms="";
$dbdb_webcms = "atpoc";

$dbhost_legacycms="169.54.235.132";
$dbuser_legacycms="postgres";
$dbpasswd_legacycms="";
$dbdb_legacycms = "atpoc";


$chapter_order = 0;
$subchapter_order = 0;
$page_order = 1;
$jn = '0000';
$default_cme_instructions = "\n<p class=\"body12arial\"><br>To obtain CME/CE credit:</p>\n<ol id=\"to_obtain_credit_list\">\n<li class=\"learning_obj\">Read or listen to each activity carefully.</li>\n<li class=\"learning_obj\">Complete/submit each posttest and evaluation.</li>\n<li class=\"learning_obj\">A record of your participation will be available in the CME Tracker area of this application and the certificate will be available on our website.</li>\n\n</ol>\n";
$default_no_fee_disclaimer = "There is no fee for this activity.";
$default_trademark = "\n<p class=\"body11arial\">Projects In Knowledge<sup>&reg;</sup> is a registered trademark of Projects In Knowledge, Inc.</p>\n";

$content = '';


$subjob = array();

function get_data($url) {
	        $ch = curl_init();
	        $timeout = 10;
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	        $data = curl_exec($ch);
            if(!$data) 
            { 
                trigger_error(curl_error($ch)); 
            } 

	        curl_close($ch);
	        return $data;
}



//GET
if ( $method == "SELECT")
{

	#var_dump($record_id);

	
	$jn = (strlen($record_id) > 0 ? $record_id : $jn);

	$dbconn_webcms = pg_connect("host=".$dbhost_webcms." port=5432 dbname=".$dbdb_webcms." user=".$dbuser_webcms." password=".$dbpasswd_webcms);

	$dbconn_legacycms = pg_connect("host=".$dbhost_legacycms." port=5432 dbname=".$dbdb_legacycms." user=".$dbuser_legacycms." password=".$dbpasswd_legacycms);

	$articleid = str_replace("BM", "", $jn);
	$cola_endpoint = 'https://api.atpoc.com/sandboxapi-nb/api-breakingmed/bridgefeed_cola/'.$articleid;
	$colb_endpoint = 'https://api.atpoc.com/sandboxapi-nb/api-breakingmed/bridgefeed_colb/'.$articleid;


	if (!$dbconn_webcms) {
		die('Could not connect to the WebCMS database' . pg_last_error($dbconn));
	}

	if (!$dbconn_legacycms) {
		die('Could not connect to the LegacyCMS database' . pg_last_error($dbconn));
	}


	$cme_webcms_q = "SELECT * FROM tbl_breakingmed_articles  

					WHERE articleid = $1";


	$faculty_webcms_q = "SELECT name
								,role
								,organization
								,photo

						FROM tbl_newsengin_atpoc_faculty
						WHERE jobnum = $1
								AND role IS NOT NULL
								AND isactive = 1";

	$lo_webcms_q = 	"SELECT lo
					FROM tbl_newsengin_atpoc_learnobjs
 					WHERE jobnum = $1
							AND isactive = 1
					ORDER BY sortorder";			



	/* $pages_legacycms_q = "SELECT t2.pageid
								,t2.page_title
								,t2.show_pretest
				      			,t1.parentid
								,t10.jobnum
				      			,t1.sortorder-1 as page_num
				      			,t1.tocid
								,t1.bookid
								,t2.cola
								,t2.colb 
								,t3.bookid
						  FROM lmt_toc t1 
									join lmt_pages t2 on t1.tocid=t2.tocid 
									  join lmt_toc t10  on t10.tocid=t1.parentid
									  join lmt_toc t100 on t100.tocid = t10.parentid 
									  join lmt_book t3 on t3.bookid = t1.bookid
				          WHERE t10.jobnum = $1
				                            and t1.toc_type = 'page'
											and t1.status = 'active'
											and t10.status = 'active'
											and t100.publish2ios = 1 


				          ORDER by t1.parentid
				                  ,page_num
				                  ,t2.pageid
				                  ,t2.tocid";

	*/

    $cola = get_data($cola_endpoint);
    $colb = get_data($colb_endpoint);

    // echo($cola);exit();

    $pages_webcms_q = "SELECT 0 as pageid
    						 ,'' as page_title
    						 ,'yes' as show_pretest
    						 ,1 as parentid
    						 ,0 as jobnum
    						 ,0 as page_num
    						 ,0 as tocid
    						 ,999 as bookid 
    						 ,'content here' as cola
    						 ,'' as colb
    					FROM lmt_toc 
    					WHERE jobnum = $1";


    				
	$cme_webcms_res = pg_query_params($dbconn_webcms,$cme_webcms_q,array($articleid));
	$faculty_webcms_res = pg_query_params($dbconn_webcms,$faculty_webcms_q,array($jn));
	$lo_webcms_res = pg_query_params($dbconn_webcms,$lo_webcms_q,array($jn));

	#$pages_legacycms_res = pg_query_params($dbconn_legacycms,$pages_legacycms_q,array($jn));
	$pages_webcms_res = pg_query_params($dbconn_webcms,$pages_webcms_q,array($jn));
	#echo pg_num_rows($pages_webcms_res); exit();

	if (!pg_num_rows($cme_webcms_res)) {

		header("HTTP/1.1 400 Bad Request");		
		//$output ['Error'] = "Jobnum does not exist" ;	
		die('Jobnum >> ' . $jn . ' << CME does not exist');
	
		
	}
	else {

		while ($row_cme = pg_fetch_assoc($cme_webcms_res)) {


			$subjob["surNum"] = "<deprecated>";
			$subjob["subNum"] = "<deprecated>";
			$subjob["testjobnum"] = $jn;
			$subjob["title"] = $row_cme["activity_title"];
			$subjob["cme"]["funderLogos"]["peerReviewed"] = array();

			$subjob["cme"]["funderLogos"]["peerReviewed"][0]["link"] = "http://www.projectsinknowledge.com";
			$subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgsrc"] = "http://api.atpoc.com/cdn/DESIGN/breakingmed/logos/PIK.gif";
			$subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgAlt"] = "Projects In Knowledge&reg;";

			$subjob["cme"]["funderLogos"]["peerReviewed"][1]["link"] = "http://www.projectsinknowledge.com/Trust.cfm";
			$subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgsrc"] = "http://api.atpoc.com/cdn/DESIGN/breakingmed/logos/TIK.gif";
			$subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgAlt"] = "Peer Reviewed";

			$subjob["cme"]["funderLogos"]["tieredFunders"] = $row_cme["funder_line"];

			$subjob["cme"]["faculty"] = array();

			$faculty_cnt = 0;
			while ($row_fcl = pg_fetch_assoc($faculty_webcms_res)) {

				$subjob["cme"]["faculty"][$faculty_cnt]["role"] = $row_fcl["role"];
				$subjob["cme"]["faculty"][$faculty_cnt]["facultyImage"] = $row_fcl["photo"];
				$subjob["cme"]["faculty"][$faculty_cnt]["fullName"] = $row_fcl["name"];
				$subjob["cme"]["faculty"][$faculty_cnt]["credentials"] = null;
				$subjob["cme"]["faculty"][$faculty_cnt]["jobAffiliations"] = $row_fcl["organization"];

				$faculty_cnt++;		
			}
			#$row_cme["is_moc"] = '1';
			$subjob["cme"]["estimatedTimeToComplete"] = array();
			$subjob["cme"]["estimatedTimeToComplete"][0]["creditType"] = "<deprecated>";
			$subjob["cme"]["estimatedTimeToComplete"][0]["availFor"] = ($row_cme["is_moc"]) == '1' ? "CME / CE / ABIM MOC":"CME / CE";
			$subjob["cme"]["estimatedTimeToComplete"][0]["cme_publishDate"] = date('M d, Y',strtotime($row_cme["releasedate"]));
			$subjob["cme"]["estimatedTimeToComplete"][0]["cme_terminationDate"] = date('M d, Y',strtotime($row_cme["termdate"]));
			$subjob["cme"]["estimatedTimeToComplete"][0]["ce_publishDate"] = date('M d, Y',strtotime($row_cme["releasedate"]));
			$subjob["cme"]["estimatedTimeToComplete"][0]["ce_terminationDate"] = date('M d, Y',strtotime($row_cme["termdate"]));
			$subjob["cme"]["estimatedTimeToComplete"][0]["cpe_publishDate"] = date('M d, Y',strtotime($row_cme["releasedate"]));
			$subjob["cme"]["estimatedTimeToComplete"][0]["cpe_terminationDate"] = date('M d, Y',strtotime($row_cme["termdate"]));

			
			$subjob["cme"]["credits"] = array(); //correlates to  'estimated time for completion'
			$subjob["cme"]["credits"][0]["creditType"] = "CME";
			$subjob["cme"]["credits"][0]["actualCredit"] = $row_cme["cme_credit"] . " hours";
			$subjob["cme"]["credits"][1]["creditType"] = "CNE";
			$subjob["cme"]["credits"][1]["actualCredit"] = $row_cme["ce_credit"] . " hours";
			$subjob["cme"]["credits"][2]["creditType"] = "CPE";
			$subjob["cme"]["credits"][2]["actualCredit"] = $row_cme["cpe_credit"] . " hours";

			$subjob["cme"]["abim_credit"] = ($row_cme["is_moc"] == '1') ? "You are Eligible for AMA PRA Category 1 Credit(s)<sup>TM</sup> ABIM MOC points" :null;


			$subjob["cme"]["cmeInstructions"] = array();
			$subjob["cme"]["cmeInstructions"][0]["title"] = null;
			$subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = $default_cme_instructions;
			$subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = $default_no_fee_disclaimer;


			$subjob["cme"]["contactInfo"] = $row_cme["contact_information"];
			$subjob["cme"]["targetAudience"] = $row_cme["target_audience"];
			$subjob["cme"]["activityGoal"] = $row_cme["activity_goal"];


			$subjob["cme"]["learningObjectives"] = array();

			$lo_cnt = 0;
			while ($row_lo = pg_fetch_assoc($lo_webcms_res)) {

				$subjob["cme"]["learningObjectives"][$lo_cnt]["objective"] = $row_lo["lo"];

				$lo_cnt++;
			}

			$subjob["cme"]["cmeInfo"] = array();
			$subjob["cme"]["cmeInfo"][0]["title"] = null;
			$subjob["cme"]["cmeInfo"][0]["statementOfAccreditation"] = null;
			$subjob["cme"]["cmeInfo"][0]["creditDesignation"] = $row_cme["cme_statement"];

			$subjob["cme"]["cmeInfo"][1]["title"] = null;
			$subjob["cme"]["cmeInfo"][1]["statementOfAccreditation"] = null;
			$subjob["cme"]["cmeInfo"][1]["creditDesignation"] = $row_cme["ce_statement"];

			$subjob["cme"]["cmeInfo"][2]["title"] = null;
			$subjob["cme"]["cmeInfo"][2]["statementOfAccreditation"] = null;
			$subjob["cme"]["cmeInfo"][2]["creditDesignation"] = $row_cme["cpe_statement"];



			$subjob["cme"]["disclosureInformation"] = $row_cme["cmedisclosures"];

			$subjob["cme"]["facultyDisclosures"] = array();
			$subjob["cme"]["facultyDisclosures"][0]["fullName"] = "<deprecated>";
			$subjob["cme"]["facultyDisclosures"][0]["disclosure"] = "<deprecated>";
			$subjob["cme"]["facultyDisclosures"][0]["role"] = "<deprecated>";


			$subjob["cme"]["medicalWriters"] = array();
			$subjob["cme"]["medicalWriters"][0]["fullName"] = "<deprecated>";
			$subjob["cme"]["medicalWriters"][0]["disclosure"] = "<deprecated>";


			$subjob["cme"]["noPeerReviewerDisclosures"] = "<deprecated>";

			$subjob["cme"]["ceDisclosures"] = array();
			$subjob["cme"]["ceDisclosures"][0]["fullName"] = "<deprecated>";
			$subjob["cme"]["ceDisclosures"][0]["disclosure"] = "<deprecated>";

			$subjob["cme"]["cmeAccreditorProvider"] = "<deprecated>";
			$subjob["cme"]["PIKRelationship"] = "<deprecated>";
			$subjob["cme"]["conflictsOfInterest"] = "<deprecated>";

			$subjob["cme"]["finalBlurb"] = "<deprecated>";

			$subjob["cme"]["funderInfo"] = array();
			$subjob["cme"]["funderInfo"][0]["funderJN"] = $jn;
			$subjob["cme"]["funderInfo"][0]["fundertext"] = $row_cme["funder_line"];
			$subjob["cme"]["funderInfo"][0]["fundertext2"] = $row_cme["funder_line"];
			$subjob["cme"]["funderInfo"][0]["funderHTML"] = $row_cme["funder_line"];

			$funder_line = $row_cme["funder_line"]; //need in pages

			$subjob["cme"]["mutualResponsibility"] = $row_cme["mutual_responsibility_statement"];
			$subjob["cme"]["trademark"] = $default_trademark;
			$subjob["cme"]["is_moc"] = $row_cme["is_moc"];
			$subjob["cme"]["moc_credit_type"] = $row_cme["moc_credit_type"];


			$subjob["cmeNum"] = $row_cme["jobnum"];

			

		}

		
		$subjob["pages"] = array();

		$page_cnt = 0;
		#while ($row_p = pg_fetch_assoc($pages_legacycms_res)) {
		while ($row_p = pg_fetch_assoc($pages_webcms_res)) {
			$subjob["pages"][$page_cnt]["pageNum"] = $row_p["page_num"];
			$subjob["pages"][$page_cnt]["tocTitle"] = $row_p["page_title"];
			$subjob["pages"][$page_cnt]["tocType"] = "page";
			$subjob["pages"][$page_cnt]["tocID"] = $row_p["tocid"];
			$subjob["pages"][$page_cnt]["parentID"] = $row_p["parentid"];
			$subjob["pages"][$page_cnt]["jobNum"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["jobNumDot"] = "<deprecated>";
			#$subjob["pages"][$page_cnt]["cola"] = $row_p["cola"];
			$subjob["pages"][$page_cnt]["cola"] = $cola;
			$subjob["pages"][$page_cnt]["colb"] = $colb;
			$subjob["pages"][$page_cnt]["colc"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["cold"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["cole"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["page_type"] = "page";
			$subjob["pages"][$page_cnt]["chapternum"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["subchapternum"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["page_title"] = $row_p["page_title"];
			$subjob["pages"][$page_cnt]["pageid"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["swipeleft"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["swiperight"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["swipedown"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["swipeup"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["sortorder"] = $page_cnt+1;
			$subjob["pages"][$page_cnt]["dlu"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["datepublished"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["publishedby"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["publish2ios"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["showPosttest"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["showPretest"] = $row_p["show_pretest"];
			$subjob["pages"][$page_cnt]["showClaim"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["bookid"] = $row_p["bookid"];
			$subjob["pages"][$page_cnt]["portBack"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["landBack"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["testjobnum"] = $jn;
			$subjob["pages"][$page_cnt]["funderText"] = $funder_line;
			$subjob["pages"][$page_cnt]["layout"] = "<deprecated>";
			$subjob["pages"][$page_cnt]["template"] = "<deprecated>";

			$page_cnt++;

		}


		$subjob["pretest"] = null;
		$subjob["posttest"]["questions"] = array();
		$subjob["survey"] = null;
		
		
		pg_close($dbconn_webcms);
		// $profile['details'] = json_decode( $profilejson, true );
				
	}
			


	$output = $subjob;
	
	
	$subjob_json_file_ext = $content_location . $jn . ".json";
	$subjob_json_file = $content_location . $jn;

	$subjob_json_ext = fopen($subjob_json_file_ext, "w") or die("Unable to open file!");
	fwrite($subjob_json_ext, json_encode($output));

	$subjob_json = fopen($subjob_json_file, "w") or die("Unable to open file!");
	fwrite($subjob_json, json_encode($output));

	fclose($subjob_json_ext);
	fclose($subjob_json);
	
	#var_dump($subjob_json_file);


}



elseif ( $method == "INSERT")
{
	// print_r($POST);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
       
}
elseif ( $method == "UPDATE")
{
	// print_r($PUT);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";	
       
}

elseif ($method == "DELETE")
{
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
}

if ( $debugtoggle == 1 )
{
	$a2 = $debug;
        $res = array_merge_recursive( $output, $a2 );
        $resJson = json_encode( $res );
	echo $resJson;
}
else
{
	$resJson = json_encode( $output );
	echo $resJson;
}



apilog($debug,$profile);


}
?>
