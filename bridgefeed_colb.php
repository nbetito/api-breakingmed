<?php

function api($service_id,$method,$listmode,$record_id,$token,$GET,$POST,$PUT,$debug,$debugtoggle)
{


//db connections
$pik_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=pik user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);
$atpoc_db = pg_connect("host=".$GLOBALS['m_server']." port=5432 dbname=atpoc user=".$GLOBALS['m_user']." password=".$GLOBALS['m_password']);

//GET
if ( $method == "SELECT")
{	//print_r($GET);

	if (isset($record_id)) {
		
		$articleid = $record_id ;

		if (!$atpoc_db) {
			die('Could not connect to the database' . pg_last_Error($atpoc_db));
		}


		$content_url = 'https://api.atpoc.com/feed-breakingmed/beta/articles/'. $articleid ;
		$content_json = file_get_contents($content_url);
		$content = json_decode($content_json, true);

		$hed = $content["hed"];
		$dek = $content["dek"];
		$authorbyline = $content["authorbyline"];
		$reviewerbyline = $content["reviewerbyline"];
		$jobnum = $content["jobnum"];


		$body = $content["body"];
		$body = str_replace("</p>", "</p>\r\n\r\n", $body);
		$body = str_replace("&#8217;", "&#39;", $body);
		$body = str_replace("<ul>", "<ul class=\"list\">", $body);

		$p_count = substr_count($body, '<p>'); 
		$p_count_each = $p_count / 4 ;

		echo "\r\n\r\n\r\n<!-- page 2 col b for first 4-5 tags, then 4-5 tags per col until end || ".$p_count." <p> tags counted, approx ".$p_count_each." per column -->\r\n\r\n\r\n";
		echo $body;

		echo "\r\n<!-- final page col b -->\r\n";


		$sourcedisclosures = $content["sourcedisclosures"];
		echo "\r\n\r\n<h4>Disclosure:</h4>".$sourcedisclosures;

		$sources = $content["sources"];
		echo "\r\n\r\n<h4>Sources:</h4>".$sources."\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";


	}
	else {
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Please supply articleid" ;	
	}	
}


elseif ( $method == "INSERT")
{
	// print_r($POST);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
       
}
elseif ( $method == "UPDATE")
{
	// print_r($PUT);
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";	
       
}

elseif ($method == "DELETE")
{
	header("HTTP/1.1 400 Bad Request");		
	$output ['Error'] = "Method not supported";		
}



}
?>
