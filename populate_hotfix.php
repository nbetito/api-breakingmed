<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    if ($record_id) {

        //db connections
        $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=pik user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
        $atpoc_db = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=atpoc user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);

        // Get subchapter tocid by jobnum
        $lmt_toc_q     = "select * from lmt_toc where jobnum = '" . $record_id . "' and toc_type = 'subchapter' and status = 'active' order by tocid asc limit 1";
        $lmt_toc_q_res = pg_query($atpoc_db, $lmt_toc_q);
        $lmt_toc       = pg_fetch_assoc($lmt_toc_q_res);

        $tocid = $lmt_toc["tocid"];

        // Get articleid by jobnum
        $tbl_breakingmed_articles_q     = "select * from tbl_breakingmed_articles where jobnum = '" . $record_id . "' limit 1";
        $tbl_breakingmed_articles_q_res = pg_query($atpoc_db, $tbl_breakingmed_articles_q);
        $tbl_breakingmed_articles       = pg_fetch_assoc($tbl_breakingmed_articles_q_res);

        $articleid = $tbl_breakingmed_articles["articleid"];

        // GPS activity content from tbl_newsengin_atpoc
        $gps_url  = 'http://api.atpoc.com/api-newsengin/1.1/articles/' . $articleid;
        $gps_json = file_get_contents($gps_url);
        $gps      = json_decode($gps_json, true);

        $jobnum         = $gps['jobnum'];
        $activity_title = $gps['hed'];
        $releasedate    = $gps['releasedate'];
        $termdate       = date('Y-m-d H:i:s', strtotime('+2 years -1 day', strtotime($releasedate)));
        $byline         = "By " . $gps['authorbyline'] . "
Reviewed by " . $gps['reviewerbyline'];

        // $jobnum         = $gps['jobnum'];
        // $jobnum         = $gps['jobnum'];

        // update lmt_toc
        $query                      = "update lmt_toc set toc_title = '" . trim($activity_title) . "' , publish2ios = 1 where jobnum = '" . $jobnum . "'; ";
        $output['lmt_toc']['query'] = $query;
        $result                     = pg_query($atpoc_db, $query);
        $status                     = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_toc']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_toc']['status'] = pg_last_error($atpoc_db);
        }

        // update lmt_pages
        $query                        = "update lmt_pages set page_title = '" . trim($activity_title) . "' where tocid = '" . $tocid . "'; ";
        $output['lmt_pages']['query'] = $query;
        $result                       = pg_query($atpoc_db, $query);
        $status                       = pg_result_status($result);
        if ($status == 1) {
            $output['lmt_pages']['status'] = "tocid = " . $tocid . " updated";
        } else {
            $output['lmt_pages']['status'] = pg_last_error($atpoc_db);
        }

        // update joblist

        // rewrite string
        $activity_url  = 'http://api.atpoc.com/sandboxapi-nb/zapier/jn?jn=' . $record_id;
        $activity_json = file_get_contents($activity_url);
        $activity      = json_decode($activity_json, true);
        $subdomain     = $activity["subdomain"];
        $parentjobnum  = substr($record_id, 0, 4);
        $rewritestring = '//suiteweb.atpointofcare.com/#library/' . $subdomain . '/' . $record_id . '/page/0';

        $query = "UPDATE joblist set rewritestring ='" . $rewritestring . "', parentjobnum = " . $parentjobnum . ", webemailname = 'breakingmed', byline = '" . $byline . "', hascme = TRUE, hasce = TRUE, hascpe = TRUE where jobnum = '" . $record_id . "';";

        $output['joblist']['query'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['status'] = "jobnum = " . $record_id . " updated";
        } else {
            $output['joblist']['status'] = pg_last_error($pik_db);
        }

        // update jobcredit
        $query = "UPDATE jobcredit set reldate ='" . $releasedate . "' , termdate ='" . $termdate . "' where jobnum = '" . $record_id . "';";

        $output['jobcredit']['query'] = $query;

        $result = pg_query($pik_db, $query);
        $status = pg_result_status($result);
        if ($status == 1) {
            $output['joblist']['status'] = "jobnum = " . $record_id . " updated";
        } else {
            $output['joblist']['status'] = pg_last_error($pik_db);
        }

    } else {

        // $output["error"] = "please provide articleid";
        $output = "please provide articleid after the slash";

    }

// debug stuff

    if ($debugtoggle == 1) {
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        $resJson = json_encode($output);
        echo $resJson;
        // echo $output;

    }
}
