<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{


    if ($method == "INSERT") {
        // print_r($POST);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "SELECT") {
        // print_r($GET);

        if ($record_id) {

            $jobnum = $record_id;
            $gps_location = '/vol3/log/funded_breakingmed/'.$jobnum.'.json'  ;
            $gps_json = file_get_contents($gps_location);
            $gps = json_decode($gps_json, true);


            $autolayout_json = post_data('https://secureapi.atpoc.com/api-webcms/beta/bm_funded_autolayout?autolayout=1', $gps_json);
            $autolayout = json_decode($autolayout_json, true);


            $blue_line_json = file_get_contents('https://secureapi.atpoc.com/api-webcms/beta/blue_line_subfeed_pik/'.$record_id.'?prod=1');
            $blue_line = json_decode($blue_line_json, true);


            // $output["json"] = $blue_line;
            $output["json"] = $blue_line["json"];
            $output["suiteweb"] = $blue_line["suiteweb"];
            // $output["autolayout"] = $autolayout;
            // $output["gps"] = $gps;




        } else {
            header("HTTP/1.1 400 Bad Request");
            $output['Error'] = "record_id missing";

        }

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        header("HTTP/1.1 200 OK");
        $a2      = $debug;
        $output["autolayout"] = $autolayout;
        $output["gps"] = $gps;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        header("HTTP/1.1 200 OK");
        $resJson = json_encode($output);
        echo $resJson;

    }

}

?>