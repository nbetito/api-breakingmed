<?php
/*
    * @package DragonAPI
    * @subpackage api.php v2.8
    * @copyright (C) Unika Boutique LLC
    * @license GNU/GPL, see license.txt
    * DragonAPI is free software; you can redistribute it and/or
    * modify it under the terms of the GNU General Public License 2
    * as published by the Free Software Foundation.
    * 
    * DragonAPI is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    * THE SOFTWARE. 
    * 
    * You should have received a copy of the GNU General Public License
    * along with DragonAPI; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
    * or see http://www.gnu.org/licenses/.
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
*/
?>
<?php
if (!function_exists('apache_request_headers')) { 


       function apache_request_headers() { 
            foreach($_SERVER as $key=>$value) { 
                if (substr($key,0,5)=="HTTP_") { 
                    $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5))))); 
                    $out[$key]=$value; 
                }else{ 
                    $out[$key]=$value; 
        } 
            } 
            return $out; 
        } 
} 
?>
<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT, DELETE");
header("Access-Control-Allow-Headers: Authorization, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
#header("Access-Control-Request-Headers:accept, authorization");
#header("Access-Control-Allow-Headers: *");
header('Content-Type: application/json');
header('Cache-Control:no-cache');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
session_start();
if ( isset($_GET['debug']))
{
        if ($_GET['debug']==1)
        {
        $debug=1;
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
        }
        else
        {
        $debug=0;
        }
}
else
{
$debug=0;
}
 ?>
<?php 
$token   = null;
$headers = apache_request_headers();
  //print_r($headers);
  if(isset($headers['Authorization'])){
    $matches = explode(" ",($headers['Authorization']));
    // echo 'print_r($matches)';
    // print_r($matches);
    if(isset($matches[1])){
      $token = $matches[1];
    }
    else
    {
	$token = $matches[0];
    }
  }
  elseif(isset($_COOKIE["auth_token"]))
  {
	$token = $_COOKIE["auth_token"];
  }
  elseif(isset($_SESSION["auth_token"]))
  {
        $token = $_SESSION["auth_token"];
  }
  elseif(isset($_GET["auth_token"]))
  {
        $token = $_GET["auth_token"];
  }
  elseif(isset($_GET["token"]))
  {
        $token = $_GET["token"];
  }
?>
<?php
$request       = $_SERVER['REQUEST_URI'];
$request_parts = explode('/',$request);
$requestCount  = count($request_parts);
$service_parts = explode('?',$request_parts[3]);
$service_parts = explode('?',$request_parts[$requestCount -1],2);
$serviceCount  = count($service_parts);

if ($requestCount < 5)
{
  $serviceIdNdx = $requestCount - 1;         // even
  $service_id   = $service_parts[0];
  $class_id =  $request_parts[$serviceIdNdx-1];
  $record_id    = '';
}
else
{
  $serviceIdNdx = $requestCount - 2;         // odd
  $class_id =  $request_parts[$serviceIdNdx-1];
  $service_id   = $request_parts[$serviceIdNdx];
  $record_id    = $service_parts[0];
}
// echo ' class_id='.$class_id;
// echo ' service_id='.$service_id;
// echo ' record_id='.$record_id;

if ($record_id == "")
   {
	$listmode=1;
	//print_r($service_parts);
         parse_str($service_parts[1],$_GET);         // list all records
   }
else
   {
	$listmode=0;
     	parse_str($service_parts[1],$_GET);        // select 1 record
   }

// echo ' print_r($_GET)';
// print_r($_GET);

if ($_SERVER['REQUEST_METHOD'] === 'POST')
   {
	// if(isset($HTTP_RAW_POST_DATA))
	// {
	$_POST=json_decode(file_get_contents("php://input"), true);
	// $_POST=json_decode($HTTP_RAW_POST_DATA);
	// }
	// else
	// {
    // parse_str(file_get_contents("php://input"),$_POST);	
    // echo 'print_r($_POST) 2 ';
	//}
    //print_r($HTTP_RAW_POST_DATA);
    $method="INSERT";
    $premethod="POST";
    $_PUT="";
   }
elseif ($_SERVER['REQUEST_METHOD'] === 'PUT')
   {
    $method="UPDATE";
    $premethod="PUT";
    $_PUT = json_decode(file_get_contents("php://input"), true);
    // parse_str(file_get_contents("php://input"),$_PUT);
   }
elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE')
   {
    $method="DELETE";
    $premethod="DELETE";
    $_PUT="";
   }
else
   {
    $method="SELECT";                             // default method 'GET'
    $premethod="GET";
    $_PUT="";
   }

// echo 'service id='.$service_id_php;
$debugarray['method']=$method;
$debugarray['premethod']=$premethod;
$debugarray['token']=$token;
$debugarray['class_id']=$class_id;
$debugarray['service_id']=$service_id;
$debugarray['record_id']=$record_id;
$debugarray['_SERVER']=$_SERVER;
$debugarray["_COOKIE"]=$_COOKIE;
$debugarray["_SESSION"]=$_SESSION;
$debugarray['_GET']=$_GET;
$debugarray["_POST"]=$_POST;
$debugarray["_PUT"]=$_PUT;
$debugarray['request_parts']=$request_parts;
$requestarray['serviceID']=$service_id;
$requestarray['method']=$method;
$requestarray['listmode']=$listmode;
$requestarray['record_id']=$record_id;
$requestarray['token']=$token;
$requestarray['GET']=$_GET;
$requestarray['POST']=$_POST;
$requestarray['PUT']=$_PUT;
$requestarray['debugarray']=$debugarray;
$requestarray['debug']=$debug;
$requestarray['premethod']=$premethod;
$requestjson=json_encode($requestarray);

$log  = "IP: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a")."|".
        "request: ".$request.PHP_EOL.
        "Pass: ".$requestjson.PHP_EOL.
        "-------------------------".PHP_EOL;
//Save string to log, use FILE_APPEND to append.
// file_put_contents('/tmp/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);

// file_put_contents('/var/log/api/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);

// error_log($request."|".$requestjson, 0);
// echo ' class_id='.$class_id;
// echo ' service_id='.$service_id;
// echo ' record_id='.$record_id;
// $debugarray['record_parts']=$record_parts;
$service_id_php=$service_id.".php";
switch ($service_id) {
    case "test123":
        // include "test.php";
        include $class_id."/".$service_id_php;
        api($service_id,$method,$listmode,$record_id,$token,$_GET,$_POST,$_PUT);
        break;
    default:
        include $service_id_php;
	if(file_exists("apicustom.php"))
	{
	include "apicustom.php";
	}
        api($service_id,$method,$listmode,$record_id,$token,$_GET,$_POST,$_PUT,$debugarray,$debug,$premethod,$requestjson);
}

function json_response($message = null, $code = 200)
{
    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    // treat this as json
    header('Content-Type: application/json');
    $status = array(
        200 => '200 OK',
	300 => '300 Bad',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
        );
    // ok, validation error, or failure
   // header('Status: '.$status[$code]);
    header("HTTP/1.1 ".$code." ".$message);
    // return the encoded json
    return json_encode(array(
        'status' => $code < 300, // success or not?
        'message' => $message
        ));
}

?>

