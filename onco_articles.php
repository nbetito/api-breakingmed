<?php

function api($service_id,$method,$listmode,$record_id,$token,$GET,$POST,$PUT,$debug,$debugtoggle,$premethod,$requestjson)
{
/* Optional credentials (token) check
$creds=checkcreds($token);
if ($creds[0] <> "1")
{
$code=401;
$message="Credentials failure";
header("HTTP/1.1 ".$code." ".$message);
// echo json_response(401, 'Credentials Fail');
die();
}
*/

         $dbhost="169.54.235.132";
        $dbuser="postgres";
         $dbpasswd="";
        // $dbdb="pik";
	$dbdb="atpoc";
        // $dbtable="breakingmed_phn";
	$dbtable="tbl_breakingmed_articles";
	// $dbhost=$GLOBALS['wawa_server'];
	// $dbdb=$GLOBALS['wawa_database'];
	// $dbuser=$GLOBALS['wawa_user'];
	// $dbpasswd=$GLOBALS['wawa_password'];

        $unique_key="articleid";
        $dir=null;
        $filename_key=null;
        $filetype_key=null;
	$db_type="postgres";

// map new fields to old
    $db_map = ", t1.primarycategoryid as categoryid
                , t1.primarytopcategoryid as parentid
                , t1.authorbyline as author
                , t1.reviewerbyline as reviewer
                , t1.takeaways as actionpoints
                , t1.updated as datestamp
                , t1.ce_credit as nurse_credit_amt
                , t1.sources as primarysource
                , t1.releasedate as reldate
                , concat(t1.body, '<br /><p>Disclosure:</p>', t1.sourcedisclosures) as articlecontent
                , t2.categorytitle
                , t2.parentcategory
                , t2.rewrite as rewritecategory
                , t2.twins as twinsubcat
                , t2.categoryterm as category
                , FALSE as isembargoed
                , 'categories' as categories
                , 'Live' as published
                , 'general' as tbtype
                , NULL as titledescription
                , NULL as rssection
                , NULL as topic
                , NULL as link
                , NULL as summary
                , NULL as artreferences";
    $db_join = " t1 join tbl_breakingmed_categories t2 on t1.primarycategoryid = t2.categoryid ";


	// Make database connection
	switch ($db_type) {
    		case "mysql":
			$mysqli = new mysqli($dbhost, $dbuser, $dbpasswd, $dbdb);
			// check connection
        		if ($mysqli->connect_errno) {
        			printf("Connect failed: %s\n", $mysqli->connect_error);
        		exit();
			}
        	break;
    		case "postgres":
			$dbconn = pg_connect("host=".$dbhost." port=5432 dbname=".$dbdb." user=".$dbuser." password=".$dbpasswd) or die("Could not connect");
        	break;
	}

if ( $premethod == "GET" )
{
	$query="SELECT * FROM ".$dbtable;
	// $querydate="2019-03-06";
	$querydate=date("Y-m-d", time() + 86400 + 15000);

	if ($record_id)
    {
	       $query="select * ".$db_map." from ".$dbtable.$db_join." where articleid='".$record_id."'";
	}
	else
	{
	       $query="select * ".$db_map." from ".$dbtable.$db_join." where releasedate <= '".$querydate."' and primarycategoryid in (11,16,22,23,24,25,26,65,73,78,115,116,117,118,119,120,121,122,123,129,148,165,172,175,218,219,237,238,277,323,324,325,331,332,335,336,337,350,351,352,356,357,384,385,389,390,391,445,450,451,466,467,468,517,538,539,546,547,550,552,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,646,647,655,656,657,668,675,678,679,687,688,689,691,692,693,694,695,696,697,698,699,700,703,710,711,715,716,717,718,749,750,753,776,777,778,780,781,782,788,789,801,802,803,804,805,806,810,826,827,828,829,832,833,834,835) order by releasedate DESC limit 40"; 
	}
	// echo $query;
	switch ($db_type) {
                case "mysql":
			if ($result = $mysqli->query($query, MYSQLI_USE_RESULT)) {
				while ($row = pg_fetch_assoc($result)){
                			$index=$row[$unique_key];
                			$data[$index]=$row;
        			}
			}
			$mysqli->close();
                break;
                case "postgres":
			if (!pg_connection_busy($dbconn)) {
                		pg_send_query($dbconn, $query);
			}
			if ($result = pg_get_result($dbconn)) {
				while ($row = pg_fetch_assoc($result)){
                			$index=$row[$unique_key];
                			$data[$index]=$row;
        			}	
			}
			pg_close($dbconn);
		break;
        	}
	if ($record_id)
	{
	$output = $data[$record_id];
    if ($dir !== null) {
        $file = base64_encode(file_get_contents($dir.$data[$record_id][$filename_key]));
        $output = array_merge($data[$record_id], array('raw_data'=>$data[$record_id][$filetype_key].$file));
    }
	}
	else
	{
	$output = $data;
	}
}
elseif ($premethod == "DELETE")
{
    echo "method not supported";
}
elseif ($premethod == "PUT")
{
    echo "method not supported";
}
elseif ($premethod == "POST")
{
    echo "method not supported";
}

if ( $debugtoggle == 1 )
{
        $a2 = $debug;
        $res = array_merge_recursive( $output, $a2 );
        $resJson = json_encode( $res );
        echo $resJson;
}
else
{
        $resJson = json_encode( $output );
        echo $resJson;
}

// apilog($debug,$profile);

}

function fetch_all_assoc(& $result,$index_keys) {

  // Args :    $result = mysqli result variable (passed as reference to allow a free() at the end
  //           $indexkeys = array of columns to index on
  // Returns : associative array indexed by the keys array

  $assoc = array();             // The array we're going to be returning

  while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $pointer = & $assoc;            // Start the pointer off at the base of the array

        for ($i=0; $i<count($index_keys); $i++) {

                $key_name = $index_keys[$i];
                if (!isset($row[$key_name])) {
                        print "Error: Key $key_name is not present in the results output.\n";
                        return(false);
                }

                $key_val= isset($row[$key_name]) ? $row[$key_name]  : "";

                if (!isset($pointer[$key_val])) {

                        $pointer[$key_val] = "";                // Start a new node
                        $pointer = & $pointer[$key_val];                // Move the pointer on to the new node
                }
                else {
                        $pointer = & $pointer[$key_val];            // Already exists, move the pointer on to the new node
                }

        } // for $i

        // At this point, $pointer should be at the furthest point on the tree of keys
        // Now we can go through all the columns and place their values on the tree
        // For ease of use, include the index keys and their values at this point too

        foreach ($row as $key => $val) {
                        $pointer[$key] = $val;
        }

  } // $row

  /* free result set */
  $result->close();

  return($assoc);
}
?>

