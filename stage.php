<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    $content_location = '/vol2/s1/moonlight/content-stage/';

    $subjob = array();

    if ($method == "INSERT") {

        //
        // CME Info
        //

        $subjob["surNum"]                                          = ""; // deprecated
        $subjob["subNum"]                                          = ""; // deprecated
        $subjob["testjobnum"]                                      = $POST["job_number"];
        $subjob["title"]                                           = $POST["activity_title"];
        $subjob["ad_widget"]                                       = "";
        $subjob["cme"]["funderLogos"]["peerReviewed"]              = array();
        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["link"]   = "https://www.projectsinknowledge.com";
        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgsrc"] = "https://secureapi.atpoc.com/cdn/DESIGN/breakingmed/logos/PIK.gif";
        $subjob["cme"]["funderLogos"]["peerReviewed"][0]["imgAlt"] = "Projects In Knowledge&reg;";
        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["link"]   = "https://www.projectsinknowledge.com/Trust.cfm";
        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgsrc"] = "https://secureapi.atpoc.com/cdn/DESIGN/breakingmed/logos/TIK.gif";
        $subjob["cme"]["funderLogos"]["peerReviewed"][1]["imgAlt"] = "Peer Reviewed";

        $subjob["cme"]["funderLogos"]["tieredFunders"] = $POST["funder_line"];

        $subjob["cme"]["faculty"] = array();

        $faculty     = $POST[faculty];
        $faculty_cnt = 0;

        foreach ($faculty as $fcl) {

            $jobAffiliations = preg_replace("/\n/", "<br>", $fcl["title"]) . "<br>" . preg_replace("/\n/", "<br>", $fcl["organization"]);
            $jobAffiliations = str_replace("<br><br>", "<br />", $jobAffiliations);
            $jobAffiliations = str_replace("<br /><br>", "<br />", $jobAffiliations);
            $jobAffiliations = str_replace("<br><br />", "<br />", $jobAffiliations);
            $jobAffiliations = str_replace("<br /><br />", "<br />", $jobAffiliations);

            // HOTFIX remove leading <br />
            $leading_break   = '<br />';
            $jobAffiliations = preg_replace('/^' . preg_quote($leading_break, '/') . '/', '', $jobAffiliations);
            $leading_break   = '<br>';
            $jobAffiliations = preg_replace('/^' . preg_quote($leading_break, '/') . '/', '', $jobAffiliations);

            // PATH REPLACE (sometimes copied over so multiple ids)
            $fcl_id            = $fcl["id"];
            $path_id           = explode("_", $fcl_id);
            $gps_baseurl       = "https://atpoc.newsengin.com/gps2/uploads/" . $path_id[0];
            $secureapi_baseurl = "https://secureapi.atpoc.com/cdn/newsengin/" . $POST["job_number"];
            $fcl_photo         = str_replace($gps_baseurl, $secureapi_baseurl, $fcl["photo"]);

            $subjob["cme"]["faculty"][$faculty_cnt]["role"]            = strip_tags($fcl["role"]);
            $subjob["cme"]["faculty"][$faculty_cnt]["facultyImage"]    = $fcl_photo;
            $subjob["cme"]["faculty"][$faculty_cnt]["fullName"]        = strip_tags($fcl["name"]);
            $subjob["cme"]["faculty"][$faculty_cnt]["credentials"]     = null;
            $subjob["cme"]["faculty"][$faculty_cnt]["jobAffiliations"] = $jobAffiliations;

            $faculty_cnt++;
        }

        #$POST["is_moc"] = '1';
        $subjob["cme"]["estimatedTimeToComplete"]                           = array();
        $subjob["cme"]["estimatedTimeToComplete"][0]["creditType"]          = ""; // deprecated
        $subjob["cme"]["estimatedTimeToComplete"][0]["availFor"]            = ($POST["moc"]) == '1' ? "CME / CE / ABIM MOC" : "CME / CE";
        $subjob["cme"]["estimatedTimeToComplete"][0]["cme_publishDate"]     = date('M d, Y', strtotime($POST["release_date"]));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cme_terminationDate"] = date('M d, Y', strtotime($POST["termination_date"]));
        $subjob["cme"]["estimatedTimeToComplete"][0]["ce_publishDate"]      = date('M d, Y', strtotime($POST["release_date"]));
        $subjob["cme"]["estimatedTimeToComplete"][0]["ce_terminationDate"]  = date('M d, Y', strtotime($POST["termination_date"]));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_publishDate"]     = date('M d, Y', strtotime($POST["release_date"]));
        $subjob["cme"]["estimatedTimeToComplete"][0]["cpe_terminationDate"] = "Nov 24, 2021"; // cpe termdate not show up in CME Info, this is to disable the Claim Credit button

        $subjob["cme"]["credits"] = array(); //correlates to  'estimated time for completion'
        $cme_minutes              = floatval($POST["physician_credit"]) * 60;
        $aapa_minutes             = floatval($POST["aapa_credit"]) * 60;
        $ce_minutes               = floatval($POST["nursing_credit"]) * 60;
        $cpe_minutes              = floatval($POST["pharmacist_credit"]) * 60;

        // $row_aapa     = pg_fetch_assoc($aapa_pik_res);
        // $is_aapa      = $row_aapa["maxcredit"];
        // $aapa_minutes = floatval($row_aapa["maxcredit"]) * 60;

        $subjob["cme"]["credits"][0]["creditType"]   = "CME for Physicians";
        $subjob["cme"]["credits"][0]["actualCredit"] = $cme_minutes . " minutes";
        $subjob["cme"]["credits"][1]["creditType"]   = "CME for Physician Assistants";
        $subjob["cme"]["credits"][1]["actualCredit"] = $aapa_minutes . " minutes";
        $subjob["cme"]["credits"][2]["creditType"]   = "CNE for Nurses";
        $subjob["cme"]["credits"][2]["actualCredit"] = $ce_minutes . " minutes";
        $subjob["cme"]["credits"][3]["creditType"]   = "CPE for Pharmacists";
        $subjob["cme"]["credits"][3]["actualCredit"] = $cpe_minutes . " minutes";

        $subjob["cme"]["abim_credit"] = ($POST["moc"] == '1') ? "<em>You are Eligible for AMA PRA Category 1 Credit(s)<sup>TM</sup> ABIM MOC points</em>" : null;

        $moc_credits_available = ($POST["moc"] == '1') ? strval(number_format(floatval($POST["physician_credit"]), 2)) : null;

        $subjob["cme"]["credits_available"] = array(); // new from AAPA spec

        $cme_credit  = strval(number_format(floatval($POST["physician_credit"]), 2));
        $ce_credit   = strval(number_format(floatval($POST["nursing_credit"]), 2));
        $cpe_credit  = strval(number_format(floatval($POST["pharmacist_credit"]), 2));
        $aapa_credit = strval(number_format(floatval($POST["aapa_credit"]), 2));

        $subjob["cme"]["credits_available"][0]["creditType"]   = "Physicians";
        $subjob["cme"]["credits_available"][0]["actualCredit"] = $cme_credit;
        $subjob["cme"]["credits_available"][1]["creditType"]   = "ABIM MOC";
        $subjob["cme"]["credits_available"][1]["actualCredit"] = ($POST["moc"] == '1') ? $cme_credit : null;
        $subjob["cme"]["credits_available"][2]["creditType"]   = "Physician Assistants";
        $subjob["cme"]["credits_available"][2]["actualCredit"] = $aapa_credit;
        $subjob["cme"]["credits_available"][3]["creditType"]   = "Nurses";
        $subjob["cme"]["credits_available"][3]["actualCredit"] = $ce_credit;
        $subjob["cme"]["credits_available"][4]["creditType"]   = "Pharmacists";
        $subjob["cme"]["credits_available"][4]["actualCredit"] = $cpe_credit;
        $subjob["cme"]["credits_available"][5]["creditType"]   = "IPCE";
        $subjob["cme"]["credits_available"][5]["actualCredit"] = $cme_credit;

        // $subjob["cme"]["cmeInstructions"] = array();
        // $subjob["cme"]["cmeInstructions"][0]["title"] = 'CME/CE Instructions';
        // $subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = "\n<p class=\"body12arial\"><br>To obtain CME/CE credit:</p>\n<ol id=\"to_obtain_credit_list\">\n<li class=\"learning_obj\">Read or listen to each activity carefully.</li>\n<li class=\"learning_obj\">Complete/submit each posttest and evaluation.</li>\n<li class=\"learning_obj\">A record of your participation will be available in the CME Tracker area of this application and the certificate will be available on our website.</li>\n\n</ol>\n";
        // $subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = "There is no fee for this activity.";

        $subjob["cme"]["cmeInstructions"]                       = array();
        $subjob["cme"]["cmeInstructions"][0]["title"]           = "";
        $subjob["cme"]["cmeInstructions"][0]["cmeInstructions"] = "<h3>Interprofessional Education Committee</h3><p>Interprofessional Education (IPE) requires collaboration among and between professions in order to enhance skills and strategies for effective healthcare team practices including communication and coordination of care. In guiding the overall CE Program, the role of the IPE Committee is to improve interprofessional collaborative practice by helping to recognize the different roles, responsibilities, and expertise of other healthcare professionals to develop team strategies that improve outcomes.</p><p>The IPE Committee members include:</p><p><strong>Vandana G. Abramson, MD</strong><br />Associate Professor of Medicine<br />Vanderbilt University Medical Center<br />Nashville, Tennessee</p><p><strong>James D. Bowen, MD</strong><br />Medical Director, Multiple Sclerosis Center<br />Swedish Neuroscience Institute<br />Seattle, Washington</p><p><strong>Veronica Esquible, PharmD</strong><br />Clinical Lead Pharmacist<br />Department of Pharmacotherapy<br />Swedish Medical Center<br />Seattle, Washington</p><p><strong>Bradley J. Monk, MD, FACS, FACOG</strong><br />Arizona Oncology (US Oncology Network)<br />Professor, Gynecologic Oncology<br />University of Arizona and Creighton University<br />Medical Director of US Oncology Research Gynecology Program<br />Phoenix, Arizona</p><p><strong>Brant Oliver, PhD, MS, MPH, APRN-BC</strong><br />Healthcare Improvement Scientist<br />Associate Professor<br />The Dartmouth Institute &amp; Geisel School of Medicine at Dartmouth<br />Hanover, New Hampshire</p><p><strong>Kendall Shultes, PharmD</strong><br />Clinical Pharmacist/Assistant Professor<br />Vanderbilt/Belmont University<br />Nashville, Tennessee</p><p><strong>Katherine Sibler, MSN, WHNP-BC</strong><br />Nurse Practitioner<br />Vanderbilt Breast Center<br />Nashville, Tennessee</p>";
        $subjob["cme"]["cmeInstructions"][0]["noFeeDisclaimer"] = "";
        $subjob["cme"]["cmeInstructions"][1]["title"]           = 'CME/CE Instructions';
        $subjob["cme"]["cmeInstructions"][1]["cmeInstructions"] = "\n<p class=\"body12arial\"><br>To obtain CME/CE credit:</p>\n<ol id=\"to_obtain_credit_list\">\n<li class=\"learning_obj\">Read or listen to each activity carefully.</li>\n<li class=\"learning_obj\">Complete/submit each posttest and evaluation.</li>\n<li class=\"learning_obj\">A record of your participation will be available in the CME Tracker area of this application and the certificate will be available on our website.</li>\n\n</ol>\n";
        $subjob["cme"]["cmeInstructions"][1]["noFeeDisclaimer"] = "There is no fee for this activity.";

        $subjob["cme"]["contactInfo"]    = "<h4>Contact Information</h4><p>For questions about content or obtaining CME credit, please <a href=\"https://www.projectsinknowledge.com/contact_us/contact_us.cfm\">contact us</a>.</p>";
        $subjob["cme"]["targetAudience"] = $POST["target_audience"];
        $subjob["cme"]["activityGoal"]   = $POST["activity_goal"];

        $subjob["cme"]["learningObjectives"] = array();

        $learning_objectives = $POST["learning_objectives"];
        $lo_cnt              = 0;
        foreach ($learning_objectives as $lo) {
            $subjob["cme"]["learningObjectives"][$lo_cnt]["objective"] = strip_tags($lo);
            $lo_cnt++;
        }

        $subjob["cme"]["cmeInfo"] = array();

        $ja_statement = $POST["joint_accreditation_statement"] . "<br>";
        $ja_statement = str_replace("<p><h3>", "<h3>", $ja_statement);
        $ja_statement = str_replace("</h3></p>", "</h3>", $ja_statement);
        $ja_statement = $ja_statement . '<br><p style="clear:both"><img style="float:left;margin:2px;" src="https://cdn.atpoc.com/cdn/activity/ipce.png" width="120" border="0">This activity was planned by and for the healthcare team, and learners will receive ' . $cme_credit . ' Interprofessional Continuing Education (IPCE) credit for learning and change.</p><br><br>';

        $subjob["cme"]["cmeInfo"][0]["title"]                    = null;
        $subjob["cme"]["cmeInfo"][0]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][0]["creditDesignation"]        = $ja_statement;

        $subjob["cme"]["cmeInfo"][1]["title"]                    = null;
        $subjob["cme"]["cmeInfo"][1]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][1]["creditDesignation"]        = $POST["cme_statement"];

        $subjob["cme"]["cmeInfo"][2]["title"]                    = null;
        $subjob["cme"]["cmeInfo"][2]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][2]["creditDesignation"]        = $POST["aapa_statement"];

        $subjob["cme"]["cmeInfo"][3]["title"]                    = null;
        $subjob["cme"]["cmeInfo"][3]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][3]["creditDesignation"]        = $POST["ce_statement"];

        $subjob["cme"]["cmeInfo"][4]["title"]                    = null;
        $subjob["cme"]["cmeInfo"][4]["statementOfAccreditation"] = null;
        $subjob["cme"]["cmeInfo"][4]["creditDesignation"]        = $POST["cpe_statement"];

        $subjob["cme"]["disclosureInformation"] = $POST["cme_disclosure"];

        $subjob["cme"]["facultyDisclosures"]                  = array();
        $subjob["cme"]["facultyDisclosures"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["facultyDisclosures"][0]["disclosure"] = ""; // deprecated
        $subjob["cme"]["facultyDisclosures"][0]["role"]       = ""; // deprecated

        $subjob["cme"]["medicalWriters"]                  = array();
        $subjob["cme"]["medicalWriters"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["medicalWriters"][0]["disclosure"] = ""; // deprecated

        $subjob["cme"]["noPeerReviewerDisclosures"] = ""; // deprecated

        $subjob["cme"]["ceDisclosures"]                  = array();
        $subjob["cme"]["ceDisclosures"][0]["fullName"]   = ""; // deprecated
        $subjob["cme"]["ceDisclosures"][0]["disclosure"] = ""; // deprecated

        $subjob["cme"]["cmeAccreditorProvider"] = ""; // deprecated
        $subjob["cme"]["PIKRelationship"]       = ""; // deprecated
        $subjob["cme"]["conflictsOfInterest"]   = ""; // deprecated

        $subjob["cme"]["finalBlurb"] = ""; // deprecated

        $subjob["cme"]["funderInfo"]                   = array();
        $subjob["cme"]["funderInfo"][0]["funderJN"]    = (float) $POST["job_number"];
        $subjob["cme"]["funderInfo"][0]["fundertext"]  = $POST["funder_line"];
        $subjob["cme"]["funderInfo"][0]["fundertext2"] = $POST["funder_line"];
        $subjob["cme"]["funderInfo"][0]["funderHTML"]  = '<div style="font-weight: bold; font-size: 1.15em; color:#8B0000;padding-top:5px;">QA WEB ONLY. Please do not share this URL.</div><p id="fundertext">' . $POST["funder_line"] . '</p>';

        $subjob["cme"]["mutualResponsibility"] = "<h4>CONTRACT FOR MUTUAL RESPONSIBILITY IN CME/CE</h4><p>Projects In Knowledge has developed the contract to demonstrate our commitment to providing the highest quality professional education to clinicians, and to help clinicians set educational goals to challenge and enhance their learning experience.</p><p>For more information on the contract, <a href=\"https://www.projectsinknowledge.com/mutual_responsibility.cfm\" target=\"_blank\">click here</a></p>";

        $subjob["cme"]["trademark"]       = "<p class=\"body11arial\">Projects In Knowledge<sup>&reg;</sup> is a registered trademark of Projects In Knowledge, Inc.</p>";
        $subjob["cme"]["is_moc"]          = $POST["moc"];
        $subjob["cme"]["moc_credit_type"] = $POST["moc_credit_type"];
        $subjob["cme"]["abim_specialty"]  = $POST["abim_specialty"];
        $subjob["cme"]["termdate"]        = "2021-12-31";

        $subjob["cmeNum"] = (float) $POST["job_number"];

        $subjob["pages"] = array();

        //
        // Pre and Posttest
        //

        $test_block = testBlock($POST);

        //
        // Layout differs by articletype
        //

        switch ($POST["article_type"]) {
            case 'atPOC_text_cme':
                $subjob = atPOC_text_cme($POST, $subjob, $test_block);
                break;

            default:
                $subjob = atPOC_video_cme($POST, $subjob, $test_block);
                break;
        }

        // if ($POST["marketing_product_label"] == 'irAEs' && $POST["marketing_format"] == 'Review Article') {}

        //
        // <script> workaround GOES HERE
        //

        $output = $subjob;

        $subjob_json_file_ext = $content_location . $POST["job_number"] . ".json";
        $subjob_json_file     = $content_location . $POST["job_number"];

        $subjob_json_ext = fopen($subjob_json_file_ext, "w") or die("Unable to open file!");
        fwrite($subjob_json_ext, json_encode($output));

        $subjob_json = fopen($subjob_json_file, "w") or die("Unable to open file!");
        fwrite($subjob_json, json_encode($output));

        fclose($subjob_json_ext);
        fclose($subjob_json);

        // $result             = array();
        // $result['json']     = "https://secureapi.atpoc.com/s1/moonlight/content/" . $record_id;
        // $result['suiteweb'] = "https://suiteweb.atpointofcare.com/#library/" . $POST["therapeutic_area"] . "/" . $record_id . "/page/0";

        // var_dump($subjob_json_file);

    } elseif ($method == "SELECT") {
        // print_r($GET);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "UPDATE") {
        // print_r($PUT);
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";

    } elseif ($method == "DELETE") {
        header("HTTP/1.1 400 Bad Request");
        $output['Error'] = "Method not supported";
    }

    if ($debugtoggle == 1) {
        header("HTTP/1.1 201 Created");
        $a2      = $debug;
        $res     = array_merge_recursive($output, $a2);
        $resJson = json_encode($res);
        echo $resJson;
    } else {
        header("HTTP/1.1 201 Created");
        $resJson = json_encode($output);
        echo $resJson;
        // echo "https://secureapi.atpoc.com/s1/moonlight/content/".$record_id;

    }

    apilog($debug, $profile);

}

function testBlock($POST)
{
    //
    // TEST STUFF
    //
    $test_block = '<div style="border-left:2px solid black; border-right:2px solid black; border-bottom: 2px solid black; border-top: 2px solid black; padding: 10px; margin-top: 10px; margin-bottom: 10px"><h3>Test Questions</h3>';
    // Knowledge Questions
    $rank = 1;
    foreach ($POST["test_questions"] as $questions) {
        $test_block .= "<p>Q" . $rank . ") " . $questions['qtext'] . '</p><ol type="A">';
        // modified from NR
        $answerLetters = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        $answerKey     = trim($questions['answercorrect']);
        $answerKey     = trim(strip_tags($answerKey));
        // Get the first character using substr.
        $answerKey  = substr($answerKey, 0, 1);
        $answerKey  = strtoupper($answerKey);
        $ansCounter = 0;
        foreach ($questions["answers"] as $answers) {
            if ($answerLetters[$ansCounter] == strtoupper($answerKey)) {
                $test_block .= "<li style=\"font-weight: bold; color:#355E3B;\" >" . $answers['text'] . "</li>";
            } else {
                $test_block .= "<li>" . $answers['text'] . "</li>";
            }
            $ansCounter++;
        }
        $test_block .= "</ol><p>Correct Answer: " . $answerKey . ". " . $questions['qexplanation'] . "</p>";
        $rank++;
    }
    $test_block .= "<p>S) How often do you currently apply the following clinical practice strategies?</p><ul>";
    // Strategy Questions
    foreach ($POST["strategies"] as $strategy) {
        $strategy = strip_tags($strategy);
        $test_block .= "<li>" . $strategy . "</li>";
    }
    $test_block .= "</ul></div>";

    return $test_block;

}

function atPOC_text_cme($POST, $subjob, $test_block)
{

    //
    // Title Block
    //

    $bylines = $POST["bylines"][0];
    $bylines = str_replace("</p><p>", "\r\n<br>", $bylines);
    $bylines = str_replace("<p>", "<div class=\"byline\" style=\"margin-bottom: var(--baseline)\">\r\n", $bylines);
    $bylines = str_replace("</p>", "</div>", $bylines);

    $title_block = "<h1>" . $POST["activity_title"] . "</h1>" . $bylines . "<div id=\"cmeBlock\">&nbsp;</div><script>CMEBlockGetter('" . $POST["job_number"] . "');</script><div style=\"clear:both\">&nbsp;</div>" . $test_block;

    //
    // CLINICAL INSIGHTS
    //

    $clinical_insights = $POST["clinical_insights"][0];
    $clinical_insights = str_replace("<p>", "", $clinical_insights);
    $clinical_insights = str_replace("</p>", "", $clinical_insights);
    $clinical_insights = str_replace("<ul>", "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Clinical Insights:</div><p>", $clinical_insights);
    $clinical_insights = str_replace("<li>", "<span class=\"li\">", $clinical_insights);
    $clinical_insights = str_replace("</li>", "</span>", $clinical_insights);
    $clinical_insights = str_replace("</ul>", "</p></div></div>", $clinical_insights);

    $patient_education = "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Patient Education/Resources:</div><p>For patient education materials/resources see below:</p>" . $POST["patient_education"][0] . "</div></div>";

    //
    // REFERENCES
    //

    $references = "<h2>References</h2>" . $POST["references"][0];
    $references = str_replace(" class=\"ReferenceList\"", "", $references);
    $references = str_replace("<p", "\r\n<div style=\"margin-bottom: var(--baseline)\"", $references);
    $references = str_replace("</p", "</div", $references);
    $references = str_replace("<a href=", "<a target=\"_blank\" href=", $references);
    $references = str_replace("<ol>", "<ol start=\"1\">", $references);

    //
    // BODY
    //

    $body = $POST["body"];

    //
    // HOTFIX HTML/CSS
    //

    // h5 is reserved
    $body = str_replace("<h5>", "<h6>", $body);
    $body = str_replace("</h5>", "</h6>", $body);

    // iron out curly quotes
    $body = str_replace("&#8217;", "&#39;", $body);

    //
    // IMAGES
    //

    // path replace
    $gps_baseurl       = "https://atpoc.newsengin.com/gps2/uploads/" . $POST["id"];
    $secureapi_baseurl = "https://secureapi.atpoc.com/cdn/newsengin/" . $POST["job_number"];
    $body              = str_replace($gps_baseurl, $secureapi_baseurl, $body);

    //
    // Proper solution is to place before the paragraph preceding it
    // HOTFIX: style="clear:both"
    //

    $image_snippet = 'thumbnails/mg.gif"/></span></a></div>';
    $add_div       = 'thumbnails/mg.gif"/></span></a></div></div><div style="clear:both">&nbsp;</div>';
    $body          = str_replace($image_snippet, $add_div, $body);

    //
    // POLLING REPLACE
    // no functionality, just rich text
    //

    $poll_count = 1;

    foreach ($POST["poll_questions"] as $poll) {

        // CREATE SNIPPET
        $poll_snippet = '<div style="border-left:4px solid teal; border-right:4px solid teal; border-bottom: 4px solid teal; border-top: 4px solid teal; padding: 10px; margin-top: 10px; margin-bottom: 10px"><p style="font-size: 0.85em">Question:</p>';

        // add question
        $poll_snippet .= "<p>" . $poll["qtext"] . "</p><ul class=\"list\">";

        // loop thru answers
        foreach ($poll["answers"] as $answers) {
            $poll_snippet .= "<li>" . $answers['text'] . "</li>";
            $ansCounter++;
        }

        $poll_snippet .= '</ul><p style="font-size: 0.85em">Submit and see how your peers answered</p></div><div style="clear:both">&nbsp;</div>';

        // REPLACE
        $poll_replace = "<!-- atpoc_PollQuestion" . $poll_count . " goes here -->";
        $body         = str_replace($poll_replace, $poll_snippet, $body);
        $poll_count++;

        // REPLACE script workaround
        $poll_replace = "<ins>" . $poll["qid"] . " | Polling Question | " . $poll["qtext"] . "</ins>";
        $body         = str_replace($poll_replace, $poll_snippet, $body);

    }

    //
    // NEED TO ADD VIDEO HTML SNIPPETS TO GPS
    // Proper Expert Opinion Video snippet with Video title? At least video title
    // YouTube Video snippet? At least youtube URL, title, source

    //
    // Expert Opinion hotfix
    //

    // $eo_video_count = 1;

    foreach ($POST["media"] as $media) {
        // CHECK MP4
        if (strpos($media, "mp4")) {
            $media_path = explode($POST["id"] . "/", $media);
            // $gps_baseurl       = "https://atpoc.newsengin.com/gps2/uploads/" . $POST["id"];
            $secureapi_path = "https://secureapi.atpoc.com/cdn/newsengin/" . $POST["job_number"] . "/" . $media_path[1];

            // CREATE SNIPPET
            $eo_video_snippet = '<div style="border-left:4px solid teal; border-right:4px solid teal; border-bottom: 4px solid teal; border-top: 4px solid teal; padding: 10px; margin-top: 10px; margin-bottom: 10px"><h4>Expert Opinion</h4><video controls="" style="width: 100%; height: auto; display: block; padding: 0px; margin: 0px;"><source src="' . $secureapi_path . '" type="video/mp4"> </video></div>';

            // REPLACE
            $eo_video_replace = "<!-- " . $media_path[1] . " goes here -->";
            $body             = str_replace($eo_video_replace, $eo_video_snippet, $body);
            // $eo_video_count++;
        }
    }

    //
    // THIS IS WHERE PAGINATION WOULD GO
    //

    $subjob["pages"][0]["pageNum"]       = "0";
    $subjob["pages"][0]["tocTitle"]      = "stage";
    $subjob["pages"][0]["tocType"]       = "page";
    $subjob["pages"][0]["tocID"]         = "0";
    $subjob["pages"][0]["parentID"]      = "0";
    $subjob["pages"][0]["jobNum"]        = ""; // deprecated
    $subjob["pages"][0]["jobNumDot"]     = ""; // deprecated
    $subjob["pages"][0]["cola"]          = $title_block;
    $subjob["pages"][0]["colb"]          = $body;
    $subjob["pages"][0]["colc"]          = ""; // deprecated
    $subjob["pages"][0]["cold"]          = ""; // deprecated
    $subjob["pages"][0]["cole"]          = ""; // deprecated
    $subjob["pages"][0]["page_type"]     = "page";
    $subjob["pages"][0]["chapternum"]    = ""; // deprecated
    $subjob["pages"][0]["subchapternum"] = ""; // deprecated
    $subjob["pages"][0]["page_title"]    = "stage";
    $subjob["pages"][0]["pageid"]        = ""; // deprecated
    $subjob["pages"][0]["swipeleft"]     = ""; // deprecated
    $subjob["pages"][0]["swiperight"]    = ""; // deprecated
    $subjob["pages"][0]["swipedown"]     = ""; // deprecated
    $subjob["pages"][0]["swipeup"]       = ""; // deprecated
    $subjob["pages"][0]["sortorder"]     = 1;
    $subjob["pages"][0]["dlu"]           = ""; // deprecated
    $subjob["pages"][0]["datepublished"] = ""; // deprecated
    $subjob["pages"][0]["publishedby"]   = ""; // deprecated
    $subjob["pages"][0]["publish2ios"]   = ""; // deprecated
    $subjob["pages"][0]["showPosttest"]  = ""; // deprecated
    $subjob["pages"][0]["showPretest"]   = "no";
    $subjob["pages"][0]["showClaim"]     = ""; // deprecated
    $subjob["pages"][0]["bookid"]        = "0";
    $subjob["pages"][0]["portBack"]      = ""; // deprecated
    $subjob["pages"][0]["landBack"]      = ""; // deprecated
    $subjob["pages"][0]["testjobnum"]    = $POST["job_number"];
    $subjob["pages"][0]["funderText"]    = $POST["funder_line"];
    $subjob["pages"][0]["layout"]        = ""; // deprecated
    $subjob["pages"][0]["template"]      = ""; // deprecated

    $subjob["pages"][1]["pageNum"]       = "0";
    $subjob["pages"][1]["tocTitle"]      = "stage";
    $subjob["pages"][1]["tocType"]       = "page";
    $subjob["pages"][1]["tocID"]         = "0";
    $subjob["pages"][1]["parentID"]      = "0";
    $subjob["pages"][1]["jobNum"]        = ""; // deprecated
    $subjob["pages"][1]["jobNumDot"]     = ""; // deprecated
    $subjob["pages"][1]["cola"]          = $clinical_insights . $patient_education;
    $subjob["pages"][1]["colb"]          = $references;
    $subjob["pages"][1]["colc"]          = ""; // deprecated
    $subjob["pages"][1]["cold"]          = ""; // deprecated
    $subjob["pages"][1]["cole"]          = ""; // deprecated
    $subjob["pages"][1]["page_type"]     = "page";
    $subjob["pages"][1]["chapternum"]    = ""; // deprecated
    $subjob["pages"][1]["subchapternum"] = ""; // deprecated
    $subjob["pages"][1]["page_title"]    = "stage";
    $subjob["pages"][1]["pageid"]        = ""; // deprecated
    $subjob["pages"][1]["swipeleft"]     = ""; // deprecated
    $subjob["pages"][1]["swiperight"]    = ""; // deprecated
    $subjob["pages"][1]["swipedown"]     = ""; // deprecated
    $subjob["pages"][1]["swipeup"]       = ""; // deprecated
    $subjob["pages"][1]["sortorder"]     = 1;
    $subjob["pages"][1]["dlu"]           = ""; // deprecated
    $subjob["pages"][1]["datepublished"] = ""; // deprecated
    $subjob["pages"][1]["publishedby"]   = ""; // deprecated
    $subjob["pages"][1]["publish2ios"]   = ""; // deprecated
    $subjob["pages"][1]["showPosttest"]  = ""; // deprecated
    $subjob["pages"][1]["showPretest"]   = "no";
    $subjob["pages"][1]["showClaim"]     = ""; // deprecated
    $subjob["pages"][1]["bookid"]        = "0";
    $subjob["pages"][1]["portBack"]      = ""; // deprecated
    $subjob["pages"][1]["landBack"]      = ""; // deprecated
    $subjob["pages"][1]["testjobnum"]    = $POST["job_number"];
    $subjob["pages"][1]["funderText"]    = $POST["funder_line"];
    $subjob["pages"][1]["layout"]        = ""; // deprecated
    $subjob["pages"][1]["template"]      = ""; // deprecated

    $subjob["pretest"]               = null;
    $subjob["posttest"]["questions"] = array();
    $subjob["survey"]                = null;

    $subjob["qaweb"] = "https://qaweb.atpointofcare.com/#library/" . $POST["therapeutic_area"] . "/" . $POST["job_number"] . "/page/0";

    return $subjob;

}

function atPOC_video_cme($POST, $subjob, $test_block)
{

    //
    // Title Block
    //

    $bylines = $POST["bylines"][0];
    $bylines = str_replace("</p><p>", "\r\n<br>", $bylines);
    $bylines = str_replace("<p>", "<div class=\"byline\" style=\"margin-bottom: var(--baseline)\">\r\n", $bylines);
    $bylines = str_replace("</p>", "</div>", $bylines);

    $colb = "<p>" . $POST["funder_line"] . "</p>" . $bylines . $test_block;

    //
    // CLINICAL INSIGHTS
    //

    $clinical_insights = $POST["clinical_insights"][0];
    $clinical_insights = str_replace("<p>", "", $clinical_insights);
    $clinical_insights = str_replace("</p>", "", $clinical_insights);
    $clinical_insights = str_replace("<ul>", "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Clinical Insights:</div><p>", $clinical_insights);
    $clinical_insights = str_replace("<li>", "<span class=\"li\">", $clinical_insights);
    $clinical_insights = str_replace("</li>", "</span>", $clinical_insights);
    $clinical_insights = str_replace("</ul>", "</p></div></div>", $clinical_insights);

    $patient_education = "<div><div class=\"highlightBox\"><div class=\"imagetitle\">Patient Education/Resources:</div><p>For patient education materials/resources see below:</p>" . $POST["patient_education"][0] . "</div></div>";

    //
    // REFERENCES
    //

    $references = "<h2>References</h2>" . $POST["references"][0];
    $references = str_replace(" class=\"ReferenceList\"", "", $references);
    $references = str_replace("<p", "\r\n<div style=\"margin-bottom: var(--baseline)\"", $references);
    $references = str_replace("</p", "</div", $references);
    $references = str_replace("<a href=", "<a target=\"_blank\" href=", $references);
    $references = str_replace("<ol>", "<ol start=\"1\">", $references);

    //
    // BODY
    //

    $body = $POST["body"];

    //
    // HOTFIX HTML/CSS
    //

    // h5 is reserved
    $body = str_replace("<h5>", "<h6>", $body);
    $body = str_replace("</h5>", "</h6>", $body);

    // iron out curly quotes
    $body = str_replace("&#8217;", "&#39;", $body);

    //
    // POLLING REPLACE
    // no functionality, just rich text
    //

    $poll_count = 1;
    $poll_block = '<div style="border-left:4px solid teal; border-right:4px solid teal; border-bottom: 4px solid teal; border-top: 4px solid teal; padding: 10px; margin-top: 10px; margin-bottom: 10px">';

    foreach ($POST["poll_questions"] as $poll) {

        // CREATE SNIPPET
        $poll_block .= '<p style="font-size: 0.85em">Question:</p>';

        // add question
        $poll_block .= "<p>" . $poll["qtext"] . "</p><ul class=\"list\">";

        // loop thru answers
        foreach ($poll["answers"] as $answers) {
            $poll_block .= "<li>" . $answers['text'] . "</li>";
            $ansCounter++;
        }

        $poll_block .= '</ul><div style="clear:both">&nbsp;</div>';

    }
    $poll_block .= '</div>';

    $colb .= '<div style="clear:both">&nbsp;</div>' . $poll_block;

    //
    // NEED TO ADD VIDEO HTML SNIPPETS TO GPS
    // Proper Expert Opinion Video snippet with Video title? At least video title
    // YouTube Video snippet? At least youtube URL, title, source

    //
    // Expert Opinion hotfix
    //

    // $eo_video_count = 1;

    foreach ($POST["media"] as $media) {
        // CHECK MP4
        if (strpos($media, "mp4")) {
            $media_path = explode($POST["id"] . "/", $media);
            // $gps_baseurl       = "https://atpoc.newsengin.com/gps2/uploads/" . $POST["id"];
            $secureapi_path = "https://secureapi.atpoc.com/cdn/newsengin/" . $POST["job_number"] . "/" . $media_path[1];

            // CREATE SNIPPET
            $eo_video_snippet = '<video controls="" style="width: 100%; height: auto; display: block; padding: 0px; margin: 0px;"><source src="' . $secureapi_path . '" type="video/mp4"> </video>';

        }
    }

    $cola = "<h1>" . $POST["activity_title"] . "</h1>";
    if (strlen($POST["subtitle"]) > 1) {
        $cola .= "<h2>" . $POST["subtitle"] . "</h2>";
    }

    $cola .= $eo_video_snippet . $body;

    //
    // THIS IS WHERE PAGINATION WOULD GO
    //

    $subjob["pages"][0]["pageNum"]       = "0";
    $subjob["pages"][0]["tocTitle"]      = "stage";
    $subjob["pages"][0]["tocType"]       = "page";
    $subjob["pages"][0]["tocID"]         = "0";
    $subjob["pages"][0]["parentID"]      = "0";
    $subjob["pages"][0]["jobNum"]        = ""; // deprecated
    $subjob["pages"][0]["jobNumDot"]     = ""; // deprecated
    $subjob["pages"][0]["cola"]          = $cola;
    $subjob["pages"][0]["colb"]          = $colb;
    $subjob["pages"][0]["colc"]          = ""; // deprecated
    $subjob["pages"][0]["cold"]          = ""; // deprecated
    $subjob["pages"][0]["cole"]          = ""; // deprecated
    $subjob["pages"][0]["page_type"]     = "page";
    $subjob["pages"][0]["chapternum"]    = ""; // deprecated
    $subjob["pages"][0]["subchapternum"] = ""; // deprecated
    $subjob["pages"][0]["page_title"]    = "stage";
    $subjob["pages"][0]["pageid"]        = ""; // deprecated
    $subjob["pages"][0]["swipeleft"]     = ""; // deprecated
    $subjob["pages"][0]["swiperight"]    = ""; // deprecated
    $subjob["pages"][0]["swipedown"]     = ""; // deprecated
    $subjob["pages"][0]["swipeup"]       = ""; // deprecated
    $subjob["pages"][0]["sortorder"]     = 1;
    $subjob["pages"][0]["dlu"]           = ""; // deprecated
    $subjob["pages"][0]["datepublished"] = ""; // deprecated
    $subjob["pages"][0]["publishedby"]   = ""; // deprecated
    $subjob["pages"][0]["publish2ios"]   = ""; // deprecated
    $subjob["pages"][0]["showPosttest"]  = ""; // deprecated
    $subjob["pages"][0]["showPretest"]   = "no";
    $subjob["pages"][0]["showClaim"]     = ""; // deprecated
    $subjob["pages"][0]["bookid"]        = "0";
    $subjob["pages"][0]["portBack"]      = ""; // deprecated
    $subjob["pages"][0]["landBack"]      = ""; // deprecated
    $subjob["pages"][0]["testjobnum"]    = $POST["job_number"];
    $subjob["pages"][0]["funderText"]    = $POST["funder_line"];
    $subjob["pages"][0]["layout"]        = ""; // deprecated
    $subjob["pages"][0]["template"]      = ""; // deprecated

    $subjob["pages"][1]["pageNum"]       = "0";
    $subjob["pages"][1]["tocTitle"]      = "stage";
    $subjob["pages"][1]["tocType"]       = "page";
    $subjob["pages"][1]["tocID"]         = "0";
    $subjob["pages"][1]["parentID"]      = "0";
    $subjob["pages"][1]["jobNum"]        = ""; // deprecated
    $subjob["pages"][1]["jobNumDot"]     = ""; // deprecated
    $subjob["pages"][1]["cola"]          = $clinical_insights . $patient_education;
    $subjob["pages"][1]["colb"]          = $references;
    $subjob["pages"][1]["colc"]          = ""; // deprecated
    $subjob["pages"][1]["cold"]          = ""; // deprecated
    $subjob["pages"][1]["cole"]          = ""; // deprecated
    $subjob["pages"][1]["page_type"]     = "page";
    $subjob["pages"][1]["chapternum"]    = ""; // deprecated
    $subjob["pages"][1]["subchapternum"] = ""; // deprecated
    $subjob["pages"][1]["page_title"]    = "stage";
    $subjob["pages"][1]["pageid"]        = ""; // deprecated
    $subjob["pages"][1]["swipeleft"]     = ""; // deprecated
    $subjob["pages"][1]["swiperight"]    = ""; // deprecated
    $subjob["pages"][1]["swipedown"]     = ""; // deprecated
    $subjob["pages"][1]["swipeup"]       = ""; // deprecated
    $subjob["pages"][1]["sortorder"]     = 1;
    $subjob["pages"][1]["dlu"]           = ""; // deprecated
    $subjob["pages"][1]["datepublished"] = ""; // deprecated
    $subjob["pages"][1]["publishedby"]   = ""; // deprecated
    $subjob["pages"][1]["publish2ios"]   = ""; // deprecated
    $subjob["pages"][1]["showPosttest"]  = ""; // deprecated
    $subjob["pages"][1]["showPretest"]   = "no";
    $subjob["pages"][1]["showClaim"]     = ""; // deprecated
    $subjob["pages"][1]["bookid"]        = "0";
    $subjob["pages"][1]["portBack"]      = ""; // deprecated
    $subjob["pages"][1]["landBack"]      = ""; // deprecated
    $subjob["pages"][1]["testjobnum"]    = $POST["job_number"];
    $subjob["pages"][1]["funderText"]    = $POST["funder_line"];
    $subjob["pages"][1]["layout"]        = ""; // deprecated
    $subjob["pages"][1]["template"]      = ""; // deprecated

    $subjob["pretest"]               = null;
    $subjob["posttest"]["questions"] = array();
    $subjob["survey"]                = null;

    $subjob["qaweb"] = "https://qaweb.atpointofcare.com/#library/" . $POST["therapeutic_area"] . "/" . $POST["job_number"] . "/page/0";

    return $subjob;

}
