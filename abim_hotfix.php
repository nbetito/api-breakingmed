<?php

function api($service_id, $method, $listmode, $record_id, $token, $GET, $POST, $PUT, $debug, $debugtoggle)
{

    //db connections
    $pik_db   = pg_connect("host=" . $GLOBALS['m_server'] . " port=5432 dbname=" . $GLOBALS['m_database'] . " user=" . $GLOBALS['m_user'] . " password=" . $GLOBALS['m_password']);
    $atpoc_db = pg_connect("host=" . $GLOBALS['wawa_server'] . " port=5432 dbname=" . $GLOBALS['wawa_database'] . " user=" . $GLOBALS['wawa_user'] . " password=" . $GLOBALS['wawa_password']);

    if (!empty($POST)) {

        $output    = array();

        $include_in_updates = (strlen($POST["include_in_updates"])>0) ? 1 : 0 ;

        // add UAN and abim_specialty
        $tbl_breakingmed_articles = "update tbl_breakingmed_articles set include_in_updates = " . $include_in_updates . ", abim_specialty = '" . $POST["abim_specialty"] . "' where articleid = " . $POST["articleid"] . ";";

        $output['tbl_breakingmed_articles']['query'] = $tbl_breakingmed_articles;

        $result = pg_query($atpoc_db, $tbl_breakingmed_articles);
        $status = pg_result_status($result);

        if ($status == 1) {
            $output['tbl_breakingmed_articles']['status'] = "articleid = " . $POST["articleid"] . " updated";
        } else {
            $output['tbl_breakingmed_articles']['status'] = pg_last_error($atpoc_db);
            echo json_encode($output);
            die();
        }

    } else {
        $output["error"] = "must POST JSON";
    }

    echo json_encode($output);

}
